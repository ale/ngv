try:
    from setuptools import setup, find_packages
except ImportError:
    from ez_setup import use_setuptools
    use_setuptools()
    from setuptools import setup, find_packages

setup(
    name='ngv_frontend',
    version="",
    #description='',
    #author='',
    #author_email='',
    #url='',
    install_requires=["Pylons==0.9.6.2", "WebHelpers>=0.6.0", "SQLAlchemy>=0.4.0", "feedparser", "AuthKit>=0.4.0"],
    packages=find_packages(exclude=['ez_setup']),
    include_package_data=True,
    test_suite='nose.collector',
    package_data={'ngv_frontend': ['i18n/*/LC_MESSAGES/*.mo']},
    message_extractors = {'ngv_frontend': [
            ('**.py', 'python', None),
            ('templates/**.html', 'mako', None),
            ('public/**', 'ignore', None)]},
    entry_points="""
    [paste.app_factory]
    main = ngv_frontend.config.middleware:make_app

    [paste.app_install]
    main = pylons.util:PylonsInstaller

    [console_scripts]
    ngv-migrate = ngv_frontend.scripts.migrate:entry_point
    ngv-rss-agent = ngv_frontend.scripts.rss_fetcher:entry_point
    ngv-download-favicon = ngv_frontend.scripts.favicon:entry_point
    ngv-process-files = ngv_frontend.scripts.process_files:entry_point
    ngv-optimize-lucene = ngv_frontend.scripts.optimize_lucene:entry_point
    """,
)
