# $Id: archive.py 217 2007-01-17 18:14:01Z ale $

from meta import *
import os


### sql tables 

archives_table = Table('archives', metadata,
                       Column('archive_id', Integer, primary_key=True),
                       Column('name', Unicode(64)),
                       Column('homepage', String(255)),
                       Column('sync_mode', String(1), default='L'),
                       Column('feed_url', String(255)),
                       Column('icon', Binary()),
                       )


### classes 

class Archive(object):

    Sync = Constants(
        LOCAL=('L', 'Local'),
        RSS=('R', 'RSS Feed'),
        NGV_SYNC=('N', 'NGV Sync Protocol'),
        )

    def __init__(self, name, homepage):
        self.name = name
        self.homepage = homepage

    def icon_url(self):
        if not self.icon:
            return None
        local_file_name = 'archive_%d.gif' % (self.archive_id,)
        local_file_dir = '/srv/ngvision/ngv_frontend/ngv_frontend/public/images'
        local_file_path = os.path.join(local_file_dir, local_file_name)
        images_base = '/images'
        if not os.path.exists(local_file_path):
            icofd = open(local_file_path, 'w')
            icofd.write(self.icon)
            icofd.close()
        return images_base + '/' + local_file_name

    def is_local(self):
        return self.sync_mode == 'L'

    @classmethod
    def local(cls):
        return cls.query.order_by(asc(cls.archive_id)).limit(1).first()


### orm mapper

Session.mapper(Archive, archives_table)


