# $Id: $

from meta import *
from datetime import datetime


### sql table

groups_table = Table('groups', metadata,
                     Column('group_id', Integer, primary_key=True),
                     Column('name', String(64), index=True),
                     Column('description', Unicode),
                     Column('created_at', DateTime, default=datetime.now),
                     )


### classes

class Group(object):

    def __repr__(self):
        return '<Group: %s>' % self.name



### object-relational mappings

Session.mapper(Group, groups_table)

