# $Id: file.py 217 2007-01-17 18:14:01Z ale $


import os
import random
from datetime import datetime
#import storage
from meta import *
from video import Video

### sql tables

files_table = Table(
    'files', metadata,
    Column('file_id', Integer, primary_key=True),
    Column('video_id', Integer, ForeignKey('videos.video_id')),
    Column('role', String(20), index=True),
    Column('ftype', String(20), index=True),
    Column('name', String(200), index=True),
    Column('publication_date', DateTime, default=datetime.now, index=True),
    Column('mime_type', String(40)),
    Column('size', Integer),
    Column('notes', Unicode),
    Column('errors', Unicode),
    Column('created_at', DateTime, default=datetime.now),
    Column('modified_at', DateTime, default=datetime.now)
    )

files_video_table = Table(
    'files_video', metadata, 
    Column('file_id', Integer, ForeignKey('files.file_id'),
           primary_key=True),
    Column('frame_width', Integer),
    Column('frame_height', Integer),
    Column('duration', String(10)),
    Column('fps', String(6)),
    Column('video_codec', String(32)),
    Column('audio_codec', String(32)),
    Column('audio_lang', String(2)),
    Column('subtitles_lang', String(2)),
    Column('process_job_id', String(32)),
    )

files_subtitle_table = Table(
    'files_subtitle', metadata,
    Column('file_id', Integer, ForeignKey('files.file_id'), 
           primary_key=True),
    Column('lang', String(2)),
    )

files_screenshots_table = Table(
    'files_screenshots', metadata,
    Column('file_id', Integer, ForeignKey('files.file_id'),
           primary_key=True),
    Column('width', Integer(3), default=0),
    Column('height', Integer(3), default=0),
    )

files_p2p_table = Table(
    'files_p2p', metadata,
    Column('file_id', Integer, ForeignKey('files.file_id'), 
           primary_key=True),
    Column('link', String(512)),
    )

direct_links_table = Table(
    'direct_links', metadata,
    Column('link_id', Integer, primary_key=True),
    Column('file_id', Integer, ForeignKey('files.file_id'), index=True),
    Column('url', String(512)),
    )



### classes 

class File(object):

    def __init__(self, **kw):
        for k, v in kw.iteritems():
            setattr(self, k, v)
        if 'name' not in kw:
            self.name = self.guess_filename()

    def __repr__(self):
        return '<%s: %s>' % (self.__class__.__name__, self.name)

    def to_dict(self):
        file_info = {
            'id': self.file_id,
            'type': self.ftype,
            'role': self.role,
            'name': self.name,
            'publication_date': self.publication_date.isoformat(),
            'mime_type': self.mime_type,
            'size': self.size
            }
        if self.errors:
            file_info['errors'] = self.errors
        if self.direct_links:
            file_info['links'] = [x.url for x in self.direct_links]
        else:
            file_info['links'] = ['/download/%d' % self.file_id]
        return nonone(file_info)

    def update(self, attrs):
        for key, val in attrs.items():
            setattr(self, key, val)

    def key(self):
        if not self.file_id:
            Session.save_or_update(self)
            Session.commit()
            Session.refresh(self)
        return str(self.file_id)

    def upload(self, local_file_name, storage):
        return storage.put(self.key(), local_file_name)

    def download(self, storage):
        return storage.get(self.key())

    def open(self, storage):
        return storage.open(self.key())

    def get_url(self, storage):
        """Return an URL to download this file."""
        # first search for a direct link
        if self.direct_links:
            n = random.randint(0, len(self.direct_links) - 1)
            return str(self.direct_links[n].url)
        return storage.get_url(self.key())

    def guess_filename(self):
        if self.video:
            return '%s%s' % (self.video.file_base_name,
                             self.filename_suffix())

    def filename_suffix(self):
        # default implementation, does guess on mime_type
        sfx = '__%s' % self.role
        if self.mime_type:
            if self.mime_type == 'image/jpeg':
                sfx += '.jpg'
            elif self.mime_type == 'image/png':
                sfx += '.png'
        return sfx


class VideoFile(File):

    def to_dict(self):
        fi = File.to_dict(self)
        fi.update({
                'frame_width': self.frame_width,
                'frame_height': self.frame_height,
                'duration': self.duration,
                'fps': self.fps,
                'video_codec': self.video_codec,
                'audio_codec': self.audio_codec,
                'subtitles_lang': self.subtitles_lang
                })
        return nonone(fi)

    def filename_suffix(self):
        if self.role == 'original':
            return '.avi'
        elif self.role == 'streaming':
            return '__stream.flv'
        else:
            return '__%s.avi' % self.role

    def schedule_job(self, jobserver, action, group='processing', **args):
        args.update({'file_id': self.file_id,
                     'video_id': self.video_id,
                     'action': action})
        job_id = jobserver.put(args, group)
        self.process_job_id = job_id
        self.errors = None

    def job_status(self, jobserver):
        """Return the job status, if any. At the same time it
        checks for completion, and resets process_job_id if done."""
        if not self.process_job_id:
            return None
        try:
            info = jobserver.status(self.process_job_id)
        except:
            return None
        if 'exc' in info:
            return None
        info['job_id'] = self.process_job_id
        self.notes = info['worklog']
        if info['status'] == 'e':
            self.errors = info['errors']
            self.modified_at = datetime.now()
            self.process_job_id = None
        elif info['status'] == 'c':
            self.modified_at = datetime.now()
            self.process_job_id = None
        Session.update(self)
        return info


class SubtitleFile(File):

    def to_dict(self):
        fi = File.to_dict(self)
        fi['lang'] = self.lang
        return fi

    def filename_suffix(self):
        return '__%s.srt' % self.lang


class ScreenshotFile(File):

    def to_dict(self):
        fi = File.to_dict(self)
        fi['width'] = self.width
        fi['height'] = self.height
        return fi

    def filename_suffix(self):
        return '__%s.jpg' % self.role


class P2PFile(File):

    def filename_suffix(self):
        return '.torrent'


class FileType:

    video = VideoFile
    subtitle = SubtitleFile
    p2p = P2PFile
    screenshot = ScreenshotFile

    @classmethod
    def __getattr__(cls, attr):
        return cls.__dict__.get(attr, File)

    @classmethod
    def choose(cls, ftype):
        if hasattr(cls, ftype):
            return getattr(cls, ftype)
        else:
            return File


class DirectLink(object):

    def __repr__(self):
        return '<DirectLink: %s>' % repr(self.url)


### constants

class FileRoles:
    class subtitle:
        DEFAULT = 'main'
    class video:
        PRIMARY = 'primary'
        STREAMING = 'stream'
        USER = 'user'
    class p2p:
        pass


### object-relational mappings

files_join = polymorphic_union({
        'video': files_table.join(files_video_table),
        'subtitle': files_table.join(files_subtitle_table),
        'screenshot': files_table.join(files_screenshots_table),
        'p2p': files_table.join(files_p2p_table),
        'file': files_table.select(files_table.c.ftype=='file')
        }, None, 'fjoin')
 
files_mapper = Session.mapper(
    File, files_table, 
    select_table = files_join,
    polymorphic_on = files_table.c.ftype,
    polymorphic_identity = 'file',
    properties = dict(video = relation(Video, lazy=False,
                                       backref=backref('files', cascade="all, delete-orphan")),
                      )
    )
mapper(VideoFile, files_video_table,
       inherits = files_mapper,
       polymorphic_identity = 'video')
mapper(SubtitleFile, files_subtitle_table,
       inherits = files_mapper,
       polymorphic_identity = 'subtitle')
mapper(ScreenshotFile, files_screenshots_table,
       inherits = files_mapper,
       polymorphic_identity = 'screenshot')
mapper(P2PFile, files_p2p_table,
       inherits = files_mapper,
       polymorphic_identity = 'p2p')

Session.mapper(DirectLink, direct_links_table,
               properties = dict(file = relation(File, lazy=True, cascade="none",
                                                 backref=backref('direct_links', cascade="all, delete-orphan")))
               )



