# $Id: tag.py 217 2007-01-17 18:14:01Z ale $

from meta import *


### sql tables

tags_table = Table('tags', metadata,
                   Column('tag_id', Integer, primary_key=True),
                   Column('tag', Unicode(64), unique=True)
                   )


### classes 

class Tag(object):

    def __repr__(self):
        return '<Tag: %s>' % self.tag

    @classmethod
    def all(cls):
        return cls.query.order_by(asc(cls.tag))


## object-relational mappings

Session.mapper(Tag, tags_table)

