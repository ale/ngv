# $Id: category.py 217 2007-01-17 18:14:01Z ale $

from meta import *

from archive import Archive


### sql tables

categories_table = Table('categories', metadata,
                         Column('category_id', Integer, primary_key=True),
                         Column('archive_id', Integer, 
                                ForeignKey('archives.archive_id'),
                                index=True),
                         Column('name', Unicode(64)),
                         )


### classes 

class Category(object):
    def __repr__(self):
        return '<Category: %s>' % self.name

    @classmethod
    def by_archive(cls, archive):
        return cls.query.filter_by(archive=archive)\
            .order_by(asc(cls.name))


### object-relational mappings

Session.mapper(Category, categories_table, properties=dict(
        archive=relation(Archive, lazy=True, backref=backref('categories', lazy=True)),
        ))

