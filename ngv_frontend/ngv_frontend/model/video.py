# $Id: video.py 217 2007-01-17 18:14:01Z ale $

import base64
import struct
import random
import re
from time import strftime
from datetime import date, datetime
from meta import *

from pylons.i18n import _
from pylons import g

from audit import Audit
from archive import Archive
from category import Category
from author import Author
from tag import Tag
from synopsis import Synopsis
from group import Group 
from comment import Comment, comments_table

from ngv_frontend.lib import string_utils
from ngv_frontend.model.lucene.ngv_glue import lucene_api


## utility function: 
## format a date if it is not None
def date_or_none(d):
    if d is None:
        return d
    return d.isoformat()


### sql table

videos_table = Table(
    'videos', metadata,
    Column('video_id', Integer, primary_key=True),
    Column('public_id', String(10), index=True),
    Column('archive_id', Integer, ForeignKey('archives.archive_id')),
    Column('status', String(1), index=True),
    Column('title', Unicode(255), unique=True),
    Column('slug', String(255), index=True),
    Column('publication_date', Date, index=True),
    Column('coverage_date', Date, index=True),
    Column('coverage_location', Unicode(128)),
    Column('coverage_country', String(2)),
    Column('created_at', DateTime, default=datetime.now),
    Column('modified_at', DateTime, default=datetime.now),
    Column('file_base_name', String(255)),
    Column('license', String(32)),
    Column('assigned_to', Integer, ForeignKey('authors.author_id')),
    Column('approved_by', Integer, ForeignKey('authors.author_id')),
    Column('external_url', String(255)),
    )

category_videos_table = Table(
    'category_videos', metadata,
    Column('video_id', Integer, 
           ForeignKey('videos.video_id'), index=True),
    Column('category_id', Integer, 
           ForeignKey('categories.category_id'), index=True),
    )

authors_videos_table = Table(
    'authors_videos', metadata,
    Column('video_id', Integer, 
           ForeignKey('videos.video_id'), index=True),
    Column('author_id', Integer, 
           ForeignKey('authors.author_id'), index=True),
    )

tags_videos_table = Table(
    'tags_videos', metadata,
    Column('video_id', Integer,
           ForeignKey('videos.video_id'), index=True),
    Column('tag_id', Integer,
           ForeignKey('tags.tag_id'), index=True),
    )

groups_videos_table = Table(
    'groups_videos', metadata,
    Column('video_id', Integer, 
           ForeignKey('videos.video_id'), index=True),
    Column('group_id', Integer, 
           ForeignKey('groups.group_id'), index=True),
    Column('position', Integer),
    )


### classes 

class Video(object):

    Status = Constants(
        INCOMING=('I', 'Incoming'),
        CONFIRMED=('C', 'Confirmed'),
        REVIEW=('R', 'Review'),
        PUBLISHED=('P', 'Published'),
        ERROR=('E', 'Error'),
        )

    def __init__(self, **args):
        self.archive = Archive.local()
        if 'public_id' not in args:
            self.public_id = self.generate_id()
        for k, v in args.iteritems():
            setattr(self, k, v)

    def generate_id(self):
        r1 = random.randrange(0, 1<<32)
        r2 = random.randrange(0, 1<<32)
        return base64.b64encode(
            struct.pack('LL', r1, r2), 'zZ'
            ).rstrip('=')

    def __repr__(self):
        return '<Video: %s>' % self.title

    def to_dict(self):
        video_info = {
            'id': self.video_id,
            'title': self.title,
            'slug': self.slug,
            'status': self.Status.label(self.status),
            'publication_date': date_or_none(self.publication_date),
            'creation_date': date_or_none(self.created_at),
            'coverage_date': date_or_none(self.coverage_date),
            'coverage_location': self.coverage_location,
            'coverage_country': self.coverage_country,
            'archive': self.archive.name,
            'license': self.license,
            'synopsis': [x.to_dict() for x in self.synopsis],
            'authors': [{'id':x.author_id, 'name':x.name} 
                        for x in self.authors],
            'files': [x.to_dict() for x in self.files],
            'categories': [x.name for x in self.categories],
            }
        if self.tags:
            video_info['tags'] = [x.tag for x in self.tags]
        return nonone(video_info)

    def get_synopsis(self, lang=None, strict=False):
        """Returns a synopsis in the specified language, if exists, 
        otherwise simply the first one found."""
        if lang:
            for s in self.synopsis:
                if s.lang == lang:
                    return s
        if strict:
            return None
        elif len(self.synopsis) > 0:
            return self.synopsis[0]
        else:
            return None

    def get_files_by_role(self, *roles):
        """Returns just files with a certain role (like, 'screenshot')."""
        def _is_of_role(f):
            return (f.role in roles)
        return filter(_is_of_role, self.files)

    def get_files_by_type(self, *types):
        def _is_of_type(f):
            return (f.ftype in types)
        return filter(_is_of_type, self.files)

    def get_screenshot_thumbnail(self):
        s = self.get_files_by_role('screenshot_small')
        if s:
            return s[0]
        return None

    def get_torrent_for(self, file):
        """Tries to find the bittorrent version of the given file."""
        for f in self.files:
            if f.ftype == 'p2p' and f.name == file.name + '.torrent':
                return f
        return None

    def modify(self):
        self.modified_at = datetime.now()
        lucene_api().update(self)
        Session.update(self)

    def set_status(self, new_status, username=None):
        if self.status == new_status:
            return
        if new_status == self.Status.PUBLISHED:
            # pre-publication-checks
            self.pre_publish_checks()
        elif self.status == self.Status.PUBLISHED:
            # pre-removal checks
            self.pre_removal_checks()
        Audit.log('Changed status of video %s (%d) to %s' % (
                self.title, self.video_id, 
                self.Status.label(new_status)),
                  username=username,
                  video_id=self.video_id)
        self.status = new_status

    def set_title(self, s):
        self.title = s
        self.file_base_name = self.gen_file_base_name()
        self.slug = string_utils.sanitize(s, '-')

    def pre_removal_checks(self):
        pass

    def pre_publish_checks(self):
        if not self.publication_date:
            self.publication_date = datetime.now().date()

    def gen_file_base_name(self):
        # try to get a reasonable name
        mangled_name = string_utils.sanitize(self.title)
        # use either the coverage date or the current time
        date = self.coverage_date and self.coverage_date or datetime.now()
        location = self.coverage_location or 'xx'
        country = self.coverage_country or 'xx'
        return 'ngv_%s_%s_%04d%02d%02d_%s' % (
            location[:2].lower(),
            country[:2].lower(),
            date.year, date.month, date.day,
            mangled_name
            )

    # some common queries

    @classmethod
    def latest_videos(cls, limit=5):
        return cls.query.filter_by(status='P')\
            .order_by(desc(cls.publication_date))\
            .limit(limit)

    @classmethod
    def count_in_archive(cls, arc):
        return cls.query.filter_by(archive_id=arc.archive_id).count()

    @classmethod
    def query_modified(cls, since):
        return cls.query.filter(cls.modified_at >= since)

    def update(self, admin=False, **args):
        """To be called from controllers, resets video data to the
        given values."""
        self.coverage_date = args.get('coverage_date')
        self.coverage_location = args.get('coverage_location')
        self.coverage_country = args.get('coverage_country').upper()
        self.license = args.get('license')
        self.set_title(args.get('title'))

        sess = Session()
        if not self.video_id:
            self.set_status(self.Status.INCOMING)
            self.file_base_name = self.gen_file_base_name()
            # get the video primary ID!
            sess.save(self)
            sess.commit()
            sess.refresh(self)

        if admin:
            self.published_at = args.get('published_at')
            self.set_status(args.get('status'), args.get('username'))

        # keywords
        self.tags = []
        for kw in args.get('tags'):
            tag_objs = Tag.query.filter_by(tag=kw).all()
            if not tag_objs:
                tag_obj = Tag(tag=kw)
                self.tags.append(tag_obj)
                sess.save(tag_obj)
            else:
                self.tags.append(tag_objs[0])

        # categories
        self.categories = []
        for cat_id in args.get('categories'):
            cat = Category.query.get(int(cat_id))
            self.categories.append(cat)

        # authors
        self.authors = []
        for author_id in args.get('authors'):
            author = Author.query.get(int(author_id))
            if author.status == Author.Status.CONFIRMED:
                author.status = Author.Status.AUTHOR
                sess.update(author)
            self.authors.append(author)

        # synopsis
        for lang in g.languages.keys():
            descr = args.get('syn_' + lang)
            descr_s = args.get('syn_short_' + lang)
            if descr or descr_s:
                sy = self.get_synopsis(lang, strict=True)
                if sy:
                    sy.description = descr
                    sy.short_description = descr_s
                    sess.update(sy)
                else:
                    sy = Synopsis(
                        description=descr, lang=lang,
                        short_description=descr_s
                        )
                    self.synopsis.append(sy)
                    sess.save(sy)
        self.modify()
        sess.update(self)
        sess.commit()
        



### object-relational mappings

Session.mapper(Video, videos_table, properties = dict(
        archive = relation(Archive, cascade="none"),
        categories = relation(Category, secondary=category_videos_table, lazy=False,
                              #backref=backref('videos', cascade='none'),
                              order_by=[ asc(Category.name) ]),
        authors = relation(Author, secondary=authors_videos_table, lazy=False,
                           backref=backref('videos', cascade="all"),
                           order_by=[ asc(Author.name) ]),
        groups = relation(Group, secondary=groups_videos_table, lazy=True,
                          backref=backref('videos'), cascade='none'),
        tags = relation(Tag, secondary=tags_videos_table, lazy=False,
                        backref=backref('videos', lazy=True),
                        order_by=[ asc(Tag.tag) ]),
        synopsis = relation(Synopsis, cascade="all, delete-orphan", lazy=True),
        comments = relation(Comment, cascade="all, delete-orphan", lazy=True,
                            primaryjoin=
                            and_(videos_table.c.video_id==comments_table.c.video_id,
                                 comments_table.c.type==Comment.Type.COMMENT)),
        reviews = relation(Comment, cascade="all, delete-orphan", lazy=True,
                           primaryjoin=
                           and_(videos_table.c.video_id==comments_table.c.video_id,
                                comments_table.c.type==Comment.Type.REVIEW)),
        
        )
       )
