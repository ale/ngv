from meta import *
from author import Author


comments_table = Table('comments', metadata,
                       Column('id', Integer, primary_key=True),
                       Column('video_id', Integer, ForeignKey('videos.video_id'), index=True),
                       Column('author_id', Integer, ForeignKey('authors.author_id'), index=True),
                       Column('type', Integer(1), index=True),
                       Column('author_email', String(128)),
                       Column('text', UnicodeText()),
                       Column('stamp', DateTime(), default=func.current_timestamp()),
                       )


class Comment(object):

    Type = Constants(
        COMMENT = (0, 'Comment'),
        REVIEW = (1, 'Review'),
        )


Session.mapper(Comment, comments_table, properties=dict(
        author=relation(Author),
        ))
