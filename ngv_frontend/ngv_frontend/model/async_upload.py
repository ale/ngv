import os
from meta import *
from pylons import g


uploads_table = Table('async_uploads', metadata,
                      Column('token', String(48), primary_key=True),
                      Column('status', String(1)),
                      Column('size', Integer),
                      Column('cur_size', Integer, default=0),
                      Column('exp_size', Integer, default=0),
                      )


class AsyncUpload(object):
    def path(self):
        return os.path.join(g.async_upload_path, 'upload_%s' % self.token)
    def remove(self):
        try:
            os.remove(self.path())
        except IOError:
            pass
        Session.delete(self)


Session.mapper(AsyncUpload, uploads_table)
