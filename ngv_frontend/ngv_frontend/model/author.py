# $Id: author.py 217 2007-01-17 18:14:01Z ale $

import re
import webhelpers
from datetime import datetime
from random import randint
from meta import *
from md5 import md5

from archive import Archive


### sql tables

authors_table = Table('authors', metadata,
                      Column('author_id', Integer, primary_key=True),
                      Column('archive_id', Integer, ForeignKey('archives.archive_id')),
                      Column('name', Unicode(128)),
                      Column('username', String(64), unique=True),
                      Column('password', String(64)),
                      Column('description', Unicode),
                      Column('email', String(64)),
                      Column('website', String(255)),
                      Column('status', String(1), index=True),
                      Column('preferred_language', String(2), default='en'),
                      Column('last_login', DateTime),
                      Column('created_at', DateTime, default=func.current_timestamp()),
                      Column('modified_at', DateTime, default=func.current_timestamp())
                      )


authors_groups_membership_table = Table(
    'authors_groups_membership', metadata,
    Column('group_id', Integer, ForeignKey('authors_groups.group_id')),
    Column('author_id', Integer, ForeignKey('authors.author_id')),
    )
           
authors_groups_table = Table('authors_groups', metadata,
                             Column('group_id', Integer, primary_key=True),
                             Column('name', String(64), unique=True),
                             Column('description', Unicode),
                             )


### classes

class AuthorGroup(object):
    @classmethod
    def get(cls, name):
        return cls.query.filter_by(name=name).one()


class Author(object):

    Status = Constants(
        ADMIN = ('S', 'Administrator'),
        AUTHOR = ('A', 'Author'),
        PENDING = ('P', 'Pending registration'),
        CONFIRMED = ('C', 'Non-publishing user'),
        )

    def __init__(self, name, username=None, password=None, 
                 group=None, groups=None, **args):
        self.name = name
        if not username:
            username = self.create_username(name)
        self.username = username
        if not password:
            password = self.encrypt(self.generate_password())
        self.password = password
        if group:
            self.groups.append(AuthorGroup.get(group))
        elif groups:
            for g in groups:
                self.groups.append(AuthorGroup.get(g))
        for k, v in args.iteritems():
            setattr(self, k, v)

    def __repr__(self):
        return '<Author: %s (%s)>' % (self.name, self.username)

    def to_dict(self):
        return nonone({
            'name': self.name,
            'description': self.description,
            'email': self.email,
            'website': self.website
            })

    def modify(self):
        self.modified_at = datetime.now()
        Session.save_or_update(self)

    def is_admin(self):
        return (self.status == 'S')

    def resetpw_token(self):
        """Generates the token used to reset the password."""
        m = md5()
        m.update(str(self.last_login))
        m.update(self.password)
        m.update('SecretToken')
        return m.hexdigest()

    def check_password(self, pw):
        return self.password == self.encrypt(pw)

    @classmethod
    def generate_password(cls):
        chars = 'abcdefghijklmnopqrstuvwxyz0123456789'
        p = ''
        for i in range(8):
            p += chars[randint(0, len(chars)-1)]
        return p

    @classmethod
    def encrypt(cls, s):
        return md5(s).hexdigest()

    @classmethod
    def check_username(cls, s):
        if cls.query.filter_by(username=s).first():
            return True
        return False

    @classmethod
    def create_username(cls, s):
        parts = s.lower().split()
        if len(parts) > 3 or len(s) > 16:
            ubase = ''.join([ x[0] for x in parts ])
        else:
            ubase = re.compile(r'\W+').sub('', s.lower())
        seq = 1
        u = ubase[:32]
        while cls.check_username(u):
            seq += 1
            u = ubase + str(seq)
        return u

    # common queries

    @classmethod
    def select_list(cls, status=None, not_status=None, archive=None):
        q = cls.query
        if not archive is None:
            q = q.filter_by(archive=archive)
        if not status is None:
            q = q.filter_by(status=status)
        q = q.order_by(asc(cls.name))
        if not not_status is None:
            q = q.filter(cls.status != not_status)
        status_to_str = {
            cls.Status.ADMIN: '(A) ',
            cls.Status.AUTHOR: '',
            cls.Status.CONFIRMED: '(*) '
            }
        return ( (a.author_id, 
                  '%s%s' % (status_to_str[a.status], a.name))
                 for a in q )

    @classmethod
    def all_authors(cls, status=None):
        q = cls.query
        if status:
            q = q.filter_by(status=status)
        return q.order_by(asc(cls.name))

    @classmethod
    def search(cls, query):
        query_str = '%%%s%%' % query
        return cls.query.filter(
            cls.name.like(query_str) |
            cls.username.like(query_str) |
            cls.email.like(query_str)
            ).order_by(asc(cls.name))


### object-relational mappings

Session.mapper(AuthorGroup, authors_groups_table)

Session.mapper(Author, authors_table, properties=dict(
        archive=relation(Archive, lazy=True, backref=backref('authors')),
        groups=relation(AuthorGroup, lazy=True, 
                        secondary=authors_groups_membership_table,
                        backref=backref('authors')),
        ))

