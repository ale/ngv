from pylons import config
from sqlalchemy import Column, MetaData, Table, ForeignKey
from sqlalchemy import asc, desc, func, and_
from sqlalchemy.types import *
from sqlalchemy.orm import mapper, relation, backref, deferred, \
    polymorphic_union
from sqlalchemy.orm import scoped_session, sessionmaker

# Global session manager.  Session() returns the session object
# appropriate for the current web request.
Session = scoped_session(sessionmaker(autoflush=True, autocommit=False,
                                      bind=config['pylons.g'].sa_engine))

# Global metadata. If you have multiple databases with overlapping table
# names, you'll need a metadata for each database.
metadata = MetaData()


# This object stores constants, and provides useful methods
# to map between them.
class Constants(object):
    def __init__(self, **values):
        self.__value_to_key_map = {}
        self.__labels = {}
        for key, arg in values.iteritems():
            if isinstance(arg, tuple):
                value, label = arg
            else:
                value, label = arg, key
            setattr(self, key, value)
            self.__value_to_key_map[value] = key
            self.__labels[value] = label
    def label(self, value):
        return self.__labels[value]
    def to_key(self, value):
        return self.__value_to_key_map[value]
    def select_items(self):
        """Returns a list of key, vale pairs suitable for input
        to the form.select() template function."""
        for value, key in self.__value_to_key_map.iteritems():
            yield (value, self.__labels[value])


# Commodity function used to replace None values within dictionaries
def nonone(d):
    for k in d:
        if d[k] is None:
            d[k] = ''
    return d
