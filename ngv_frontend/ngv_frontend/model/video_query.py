from meta import *
from author import Author
from category import Category
from tag import Tag
from video import Video
from datetime import date


def subquery(attrs):
    def _subquery(arg):
        return attrs[arg]
    return _subquery


def filter_by_author(arg):
    author = Author.query.get(int(arg))
    return {'title': 'by "%s"' % author.name,
            'filter': Video.authors.contains(author),
            'sort': [asc(Video.title)]}


def filter_by_category(arg):
    category = Category.query.get(int(arg))
    return {'title': 'in category "%s"' % category.name,
            'filter': Video.categories.contains(category),
            'sort': [asc(Video.title)]}


def filter_by_location(arg):
    return {'title': 'location "%s"' % arg,
            'filter': Video.coverage_location==arg}


def filter_by_tag(arg):
    tags = [x.strip().lower() for x in arg.split(',')]
    tag_objs = []
    for tag in tags:
        tag_objs += Tag.query.filter_by(tag=tag).all()
    queries = []
    for tag in tag_objs:
        queries.append(Video.tags.contains(tag))
    return {'title': 'tagged "%s"' % ','.join(tag.tag),
            'filter': reduce(lambda x, y: x & y, queries)}


def filter_by_date(arg):
    p = arg.split('-')
    if len(p) == 1:
        beg_date = date(int(p[0]), 1, 1)
        end_date = date(int(p[0]), 12, 31)
    elif len(p) == 2:
        month_len = [ 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ]
        beg_date = date(int(p[0]), int(p[1]), 1)
        end_date = date(int(p[0]), int(p[1]), month_len[int(p[1])])
    elif len(p) == 3:
        beg_date = date(int(p[0]), int(p[1]), int(p[2]))
        end_date = beg_date
    else:
        #abort(400)
        raise Exception("Wrong date syntax")
    return {'title': 'date %s' % arg,
            'filter': ((Video.coverage_date >= beg_date)
                       & (Video.coverage_date <= end_date))}


def filter_usersort(arg):
    order_fn = (arg['sort_order'] == 'a') and asc or desc
    return {'sort':[order_fn(arg['sort_by'])]}


known_queries = {
    'all': {'title': 'all'},
    'queue': subquery({'incoming': {'title': 'incoming',
                                    'filter': Video.status==Video.Status.CONFIRMED},
                       'reviewed': {'title': 'reviewed',
                                    'filter': Video.status==Video.Status.REVIEW},
                       'errors': {'title': 'with errors',
                                  'filter': Video.status==Video.Status.ERROR},
                       }),
    'latest': {'title': 'latest',
               'sort': [desc(Video.publication_date)]},
    'author': filter_by_author,
    'latest_author': {'chain': ['author', 'latest']},
    'category': filter_by_category,
    'location': filter_by_location,
    'tag': filter_by_tag,
    'date': filter_by_date,
    'user_sort': filter_usersort,
}


class VideoQuery(object):

    def __init__(self, **args):
        for k, v in args.iteritems():
            setattr(self, k, v)

    @classmethod
    def build_query(cls, name, arg):
        if name not in known_queries:
            raise Exception("Unknown query %s" % name)
        attrs = known_queries[name]
        if callable(attrs):
            attrs = attrs(arg)
        return attrs

    @classmethod
    def query(cls, spec):
        # parse 'spec' and transform it into a list of query attributes
        queries = []
        for qname, arg in spec:
            queries.append(cls.build_query(qname, arg))

        # scan the list of query attributes and consecutively apply them
        fltr = (Video.status == Video.Status.PUBLISHED)
        titles = []
        sort_by = [asc(Video.title)]
        for attrs in queries:
            if 'filter' in attrs:
                fltr &= attrs['filter']
            if 'title' in attrs:
                titles.append(attrs['title'])
            if 'sort' in attrs:
                sort_by = attrs['sort']
        query = Video.query.filter(fltr).order_by(sort_by)

        # return an object with title, query and the result count
        return cls(title=', '.join(titles),
                   count=query.count(),
                   query=query)
