# $Id: $

import re
import formencode
from formencode import validators

from author import Author
from group import Group


### validators

class UniqueAuthor(validators.FancyValidator):
    def _to_python(self, value, state):
        value = value.strip()
        if Author.query.filter_by(name=value).all():
            raise formencode.Invalid(
                "Name already exists",
                value, state)
        return value

class CommaSeparatedList(validators.FancyValidator):
    def _to_python(self, value, state):
        value = [ x.strip() for x in value.split(',') ]
        return value

class UniqueGroup(validators.FancyValidator):
    def _to_python(self, value, state):
        value = value.strip().lower()
        if not re.match(r'[a-z0-9][-a-z0-9]+', value):
            raise formencode.Invalid(
                'Please use only alphanumeric characters for the group name',
                value, state)
        if Group.query.filter_by(name=value).all():
            raise formencode.Invalid(
                'This group name already exists',
                value, state)
        return value


### form schemas

class RegistrationForm(formencode.Schema):
    name = formencode.All(
        validators.UnicodeString(not_empty=True),
        UniqueAuthor()
        )
    email = validators.Email(not_empty=True)
    website = validators.URL(not_empty=False, add_http=True)
    description = validators.UnicodeString(not_empty=True)


class EditVideoForm(formencode.Schema):
    allow_extra_fields = True
    title = validators.UnicodeString(not_empty=True)
    keywords = CommaSeparatedList(not_empty=True)
    pub_date = validators.DateConverter(month_style='dd/mm/yyyy', not_empty=False)
    cov_date = validators.DateConverter(month_style='dd/mm/yyyy', not_empty=True)
    cov_location = validators.UnicodeString(not_empty=True)
    cov_country = validators.UnicodeString(not_empty=False)
    #status = validators.String(not_empty=False)


class EditAuthorForm(formencode.Schema):
    allow_extra_fields = True
    #name = formencode.All(
    #    validators.UnicodeString(not_empty=True),
    #    UniqueAuthor()
    #    )
    email = validators.Email(not_empty=True)
    website = validators.URL(not_empty=False, add_http=True)
    description = validators.UnicodeString(not_empty=True)


class ChangePasswordForm(formencode.Schema):
    password = validators.MinLength(6)
    password_confirm = validators.String()
    chained_validators = [validators.FieldsMatch(
            'password', 'password_confirm')]


class FullSearchForm(formencode.Schema):
    allow_extra_fields = True
    date_start = validators.DateConverter(month_style='dd/mm/yyyy', not_empty=False)
    date_end = validators.DateConverter(month_style='dd/mm/yyyy', not_empty=False)
    author = validators.String(not_empty=False)
    category = validators.String(not_empty=False)
    #status = validators.String(not_empty=False)
    location = validators.String(not_empty=False)

    
class NewGroupForm(formencode.Schema):
    name = UniqueGroup()
    description = validators.String()


class CommentForm(formencode.Schema):
    #email = validators.Email(not_empty=False)
    text = validators.UnicodeString(not_empty=True)


class ReviewForm(formencode.Schema):
    text = validators.UnicodeString(not_empty=True)


class ArchiveEditFormValidator(validators.FormValidator):
    def validate_python(self, values, state):
        if (values['sync_mode'] == 'R' 
            and ('feed_url' not in values 
                 or values['feed_url'] is None
                 or values['feed_url'] == '')):
            raise formencode.Invalid("You must specify the RSS feed URL for the RSS synchronization mode!",
                                     values, state, 
                                     error_dict={'feed_url': 'Specify a valid URL'})


class ArchiveEditForm(formencode.Schema):
    name = validators.UnicodeString(not_empty=True)
    homepage = validators.URL()
    sync_mode = validators.String()
    feed_url = validators.URL(not_empty=False)
    chained_validators = [ArchiveEditFormValidator()]
    allow_extra_fields = True

