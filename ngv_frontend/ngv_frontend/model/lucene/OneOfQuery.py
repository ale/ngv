from lucene import (BooleanQuery, Query, TermQuery, Term, 
                    NumberTools, BooleanClause)


def OneOfQuery(field, items):
    """Build an OR query for any of items in field."""
    q = BooleanQuery()
    q.setMaxClauseCount(len(items))
    for i in items:
        if Query.instance_(i):
            pass
        elif isinstance(i, basestring):
            i = TermQuery(Term(field, i))
        elif isinstance(i, (int, long)):
            i = TermQuery(Term(field, NumberTools.longToString(i)))
        else:
            raise TypeError, i
        q.add(i, BooleanClause.Occur.SHOULD)
    return q
