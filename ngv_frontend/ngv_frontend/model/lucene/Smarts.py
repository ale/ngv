"""a higher level L{lucene} wrapper with intelligence"""
import os.path
import logging
import datetime
from itertools import imap

from lucene import (Field, Document, Hit, Term, IndexWriter, IndexReader,
                      IndexSearcher, Query, FSDirectory, Document, StandardAnalyzer,
                      QueryParser)

from HitsWrapper import HitsWrapper
import TermIterator
from LuceneConstants import Constants
import conversion


__all__=['SmartDoc', 'SmartField', 'SmartHit', 'SmartStorage', 'Constants']

## dicts mapping python types to lucene-friendly strings
str2type=dict((t.__name__, t) for t in (int, long, float, datetime.date, datetime.datetime, datetime.time))
type2str=dict((t, n) for n, t in str2type.iteritems())


class SmartField(object):
    """a field name & value, with instructions on how to save it into the
    index
    
    XXX need to add support for value lists. Lucene supports multiple fields
    of the same name in a single doc, shouldn't be too hard.    
    
    See
    U{Lucene documentation<http://lucene.apache.org/java/docs/api/org/apache/lucene/document/Field.html>}    
    and nested classes for documentation on C{store, index & termvector}.
    These values may either be attributes of L{Constants} or corresonding
    string/bool values.   
    
    @ivar name: name of the L{Field}
    @type name: String
    
    @ivar value: value of the L{Field}. This is filled in by L{SmartDoc}
    @type value: basic python types
    
    @ivar alltext: should the value be included in a default field search
    @type alltext: Boolean
    
    @ivar store: how to store the document. One of C{yes, no, compress}
    @type store: string, bool or Lucene constant
    
    @ivar index: how to index the document.
    One of C{no, tokenized, untokenized, no_norms}
    @type index: string, bool or Lucene constant
    
    @ivar termvector: what sort of term vectors to generate for the document.
    One of C{yes, no, with_offsets, with_positions, with_positions_and_offsets}
    @type termvector: string, bool or Lucene constant
    """
    
    __slots__=['name', 'value', 'store', 'index', 'termvector', 'alltext']
    
    ## default storage instructions. These may not be used, depending on value
    ## type
    _default_store=Constants.STORE_NO
    _default_termvector=Constants.TERMVECTOR_NO
    _default_alltext=True

    def __init__(self, value,
                 store=None, index=None, termvector=None, alltext=None):
        
        if isinstance(value, str): raise TypeError("Lucene can't store str")
        
        self.value=value
        
        ## convert to Lucene constants for store, index & termvector. Use
        ## reasonable defaults based on value type if None specified.
        
        if store is None:
            store=self._default_store
        store=Constants.toConstant(store, "store")
        self.store=store
        
        if termvector is None:
            termvector=self._default_termvector
        termvector=Constants.toConstant(termvector, "termvector")
        self.termvector=termvector
        
        if index is None:
            if isinstance(value, unicode):
                # tokenize string like things by default
                index=Constants.INDEX_TOKENIZED
            else:
                index=Constants.INDEX_UNTOKENIZED
        index=Constants.toConstant(index, "index")
        self.index=index
        
        if alltext is None:
            alltext=self._default_alltext
        assert isinstance(alltext, bool)
        
        if isinstance(value, basestring):
            self.alltext=alltext
        else:
            ## it doesn't make sense to try to throw non-text fields into
            ## alltext
            self.alltext=False

    def toField(self):
        """
        @returns: a Lucene internal representation of this SmartField
        @rtype: L{Field}
        """
        return Field(self.name, conversion.toLucene(self.value),
                     self.store, self.index, self.termvector)

class SmartDoc(object):
    """a document built from L{SmartField}s

    @ivar fields: fields in this document
    @type fields: dict of L{SmartField}    
    """
    
    __slots__=['fields']
    
    ## how should we store termvectors for the __altext__ field
    _alltext_termvector=Constants.TERMVECTOR_NO
    
    def __init__(self, fields):
        assert isinstance(fields, dict)
        # populate field.name
        for name, field in fields.iteritems():
            assert isinstance(field, SmartField)
            field.name=name
        self.fields=fields

    def toDocument(self):
        """
        @returns: a Lucene internal representation of this SmartDoc.
        @rtype: L{Document}
        """
        doc=Document()
        
        ## list of values to include in default __alltext__ field
        alltexts=[]
        
        ## map of field.name => string of python type for non-unicode fields
        typemap={}
        
        ## for each field, add to alltext and save typemap info if necessary
        for f in self.fields.itervalues():
            assert isinstance(f, SmartField)
            doc.add(f.toField())
            if f.alltext:
                alltexts.append(f.value)
            if f.value is not None and not isinstance(f.value, basestring):
                typemap[f.name]=type2str[type(f.value)]
        
        if alltexts:
            ## build __alltext__ field
            field=SmartField(u"\n".join(alltexts),
                               store=Constants.STORE_NO,
                               index=Constants.INDEX_TOKENIZED,
                               termvector=self._alltext_termvector)
            field.name="__alltext__" # should match SmartStorage.query_parser
            doc.add(field.toField(),)
        
        if typemap:
            ## serialize the typemap. We use repr, since it's purely a dict of
            ## strings at this point
            field=SmartField(unicode(repr(typemap)),
                             store=Constants.STORE_YES,
                             index=Constants.INDEX_NO)
            field.name="__typemap__"
            doc.add(field.toField())
        
        return doc

class SmartHit(object):
    """a search result
    
    Attributes are expensive to calculate and ofent unneeded if skipping hits,
    so we do it lazily.
    """
    
    __slots__=['_hit', '_fields', '_score']
    
    def __init__(self, hit):
        self._hit=Hit.cast_(hit)
    
    @property
    def score(self):
        """relevance score
        
        @type: float
        """
        try:
            return self._score
        except AttributeError:
            self._score=self._hit.getScore()
            return self._score
    
    @property
    def fields(self):
        """
        map of field name=>value
        
        @type: dict
        """
        try:
            return self._fields
        except AttributeError:
            ## typemap will be field.name() => python type
            raw_typemap=self._hit.get('__typemap__')
            if raw_typemap is not None:
                ## raw_typemap is a repr()'d dict of strings. eval() and
                ## convert back to the type()
                typemap=dict((n, str2type[t]) for n, t in eval(raw_typemap).iteritems())
            else:
                typemap={}
            
            ## need to retrieve the doc to get a list of fields
            doc=self._hit.getDocument()
            
            ## f.name() is expensive.  do it once
            ## XXX this should filter the desired fields, see LuceneCollection.__resultFields
            raw_fields=((f.name(), f.stringValue()) for f in imap(Field.cast_, doc.getFields()))
            
            if not typemap:
                convert_fields=((name, value) for name, value in raw_fields
                                if name != '__typemap__')
            else:
                convert_fields=((name,
                                 conversion.fromLucene(value, typemap.get(name, unicode)))
                                 for name, value in raw_fields if name != '__typemap__')
            
            self._fields=dict(convert_fields)
            return self._fields
    
class SmartStorage(object):
    """A wrapper around a lucene index with a smarter API
    
    @ivar index_dir: where the lucene index lives
    @type index_dir: String
    
    @ivar analyzer: a Lucene analyzer
    @type analyzer: L{lucene.Analyzer}
    
    @ivar query_parser: a Lucene QueryParser
    @type query_parser: L{QueryParser}
    
    @ivar directory: the lucene Directory
    @type directory: L{FSDirectory}
    
    @ivar enable_bulk_writes: if true, configure writer optimized for bulk
    loading. If false, configure for updates. Defaults to False.
    @type enable_bulk_writes: bool
    """
    
    logger=logging.getLogger("SmartStorage")

    def __init__(self, index_dir, create=False, analyzer=None):
        """
        @arg create: create the index if it doesn't exist
        @type create: bool
        """
        
        self.index_dir=index_dir
        
        segments_file_exists=os.path.exists(os.path.join(self.index_dir, 'segments.gen'))
        if create == 'auto':
            create = not segments_file_exists
        if create:
            assert not segments_file_exists
            os.makedirs(index_dir)
            self.logger.warn("Created %s", index_dir)      
        else:
            assert segments_file_exists
            self.logger.info("Opened %s", index_dir)

        # analyzer can be overridden in the constructor
        if analyzer is None:
            analyzer = StandardAnalyzer()
        self.analyzer = analyzer
        
        # default field should match L{Smarts.SmartDoc}
        self.query_parser=QueryParser("__alltext__", self.analyzer)
        
        ## FSDirectory is faster than MMapDirectory, per some random email
        ## from google:
        ## http://mail-archives.apache.org/mod_mbox/lucene-java-user/200510.mbox/<200510110916.03843.paul.elschot%40xs4all.nl>
        ## ah, science
        ## XXX it's got concurrency issues tho
        self.directory=FSDirectory.getDirectory(index_dir, create)
        
        # force creation of index if necessary
        if create:
            IndexWriter(self.directory, self.analyzer, create).close()
        
        self.enable_bulk_writes=False
        self.__searcher=None
        self.__writer=None

    def close(self):
        """close all resources"""
        self.closeInternals()
        self.directory.close()
        self.logger.info("Closed %s", self.index_dir)

    def closeInternals(self):
        """close internal L{IndexReader} & L{IndexWriter}"""
        if self.__searcher is not None:
            self.__searcher.getIndexReader().close()
            self.__searcher.close()
            self.__searcher=None
            self.logger.debug("Closed searcher %s", self.index_dir)
            
        if self.__writer is not None:
            self.__writer.close()
            self.__writer=None
            self.logger.debug("Closed writer %s", self.index_dir)
    
    @property
    def searcher(self):
        """@type L{IndexSearcher}"""
        if self.__searcher is not None:
            assert self.__writer is None
            return self.__searcher
        else:
            self.closeInternals()
            self.__searcher=IndexSearcher(IndexReader.open(self.directory))
            return self.__searcher

    @property
    def reader(self):
        """@type L{IndexReader}"""
        return self.searcher.getIndexReader()        
    
    @property
    def writer(self):
        """@type L{IndexWriter}"""
        if self.__writer is not None:
            assert self.__searcher is None
            return self.__writer
        else:
            self.closeInternals()
            self.__writer=IndexWriter(self.directory, self.analyzer, False)
            self.configureWriter(self.__writer)
            return self.__writer

    def optimize(self):
        """Optimize the index."""
        self.writer.optimize()
    
    def configureWriter(self, writer):
        """configure an L{IndexWriter}.
        
        @arg writer: the new IndexWriter
        @type writer: L{IndexWriter}
        """       
        ## optimize for reading, not writing: more frequent merges than
        ## default of 10. 2 comes from Doug Cutting:
        ## http://www.opensubscriber.com/message/lucene-user%40jakarta.apache.org/803308.html
        writer.setMergeFactor(2)
        
        ## Multifile, not compound file, is the default, but we
        ## set it here for explicitness.  Lucene in Action B.3.2
        ## claims that compound files are 5-10% slower for writing,
        ## and <PyLucene>/samples/LuceneInAction/lia/indexing/CompoundVersusMultiFileIndexTest.py,
        ## run as part of the test suite, actually asserts that
        ## compound time > multifile time (self.assert_(cTiming > mTiming))
        ## Note that Lucene will convert existing files from compound
        ## to multifile, or vice versa, at open time.
        ## 
        ## Use compound files to avoid opening zillions of files hitting a bug
        ## in Python (and select limits, if we were using select).
        writer.setUseCompoundFile(True)
        writer.setWriteLockTimeout(10000L)
        
        if self.enable_bulk_writes:
            self.configureBulkWriter(writer)
        else:
            self.configureUpdatingWriter(writer)

    @staticmethod
    def configureUpdatingWriter(writer):
        """configure an L{IndexWriter} for updates.
        
        @arg writer: the new IndexWriter
        @type writer: L{IndexWriter}
        """
        
        ## explicitly set merge factor to 2. 2 comes from Doug Cutting:
        ## http://www.opensubscriber.com/message/lucene-user%40jakarta.apache.org/803308.html
        writer.setMergeFactor(2)

    @staticmethod
    def configureBulkWriter(writer):
        """configure an L{IndexWriter} for bulk loading.
        
        @arg writer: the new IndexWriter
        @type writer: L{IndexWriter}
        """
        ## set merge factor up from default
        writer.setMergeFactor(20)
        
        ## set "the minimal number of documents required before the buffered
        ## in-memory documents are merg[ed] and a new Segment is created."
        
        ## Make sure this doesn't use too much memory: if a doc is ~1K,
        ## then buffering should be ~10M, which sounds very reasonable, but
        ## I could be wrong.  This means we'll have fairly big segments,
        ## and possibly fairly long pauses when merging.
        ##
        ## 10000 seemed to chew through RAM, so dialed it back to 200, which
        ## results in about 700M usage.
        writer.setMaxBufferedDocs(200)

    def search(self, query):
        """search the index
        
        @arg query: a Lucene U{query<http://lucene.apache.org/java/docs/queryparsersyntax.html>}
        @type query: String or L{lucene.Query}
        
        @rtype: L{HitsWrapper} of L{SmartHit}s
        """
        if isinstance(query, basestring):
            query=self.query_parser.parse(query)
        
        assert Query.instance_(query)
        hits=self.searcher.search(query)
        return HitsWrapper(hits, SmartHit)

    def insert(self, doc):
        """insert a document
        
        @arg doc: the document to insert
        @type doc: L{SmartDoc}
        """
        assert isinstance(doc, SmartDoc)
        self.writer.addDocument(doc.toDocument())

    def update(self, doc, name_or_term, value=None):
        if Term.instance_(name_or_term):
            term = name_or_term
        else:
            term = Term(name_or_term, value)
        self.writer.updateDocument(term, doc.toDocument())
    
    def delete(self, name_or_term, value=None):
        """delete documents from lucene index matching Term
        
        @arg name_or_term: field name or Term to delete
        @type name_or_term: string or L{Term}
        
        @arg value: field value, or None if a passing a L{Term}
        @type value: unicode or None     
        """
        if Term.instance_(name_or_term):
            assert value is None
            term=name_or_term
        else:
            assert isinstance(name_or_term, basestring)
            assert isinstance(value, basestring)
            term=Term(name_or_term, value)
        ## XXX this can/should be writer, as of Pylucene r331.
        ## see http://lists.osafoundation.org/pipermail/pylucene-dev/2007-May/001797.html
        logging.warning('deleting %s' % str(term))
        return self.writer.deleteDocuments(term)
    
    def getAllTerms(self, field, include_counts=True):
        """yield all terms for a given field
        
        @arg field: name of the field
        @type field: String
        
        @arg include_counts: should value frequencies be included
        @type include_counts: Boolean
        
        @returns: if include_counts is True, tuples of (term, count). If
        false, just list of terms
        """
        if include_counts:
            return TermIterator.termIteratorCount(self.reader, field, count_needed=True)
        else:
            return TermIterator.termIterator(self.reader, field)
