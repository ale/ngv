from lucene import Term, IndexReader

def termIterator(reader, field, value = ''):
    """
    Wrap L{lucene} term enumerator in Python iterator returning only term text,
    and ensure that we don't iterate past the end of the given field.
    
    @arg reader: Lucene reader
    @type reader: L{IndexReader}
    
    @arg field: field name in index
    @type field: String
    
    @arg value: value to start enumerating with, defaults to '' (enumerate all
    values for this field)    
    @type value: String
    """
    for text, count in termIteratorCount(reader, field, value, count_needed = False):
        yield text

def termIteratorCount(reader, field, value = '', count_needed = True):
    """
    Wrap L{lucene} term enumerator in Python iterator returning (term
    text, count), and ensure that we don't iterate past the end of the
    given field.
    
    @arg reader: Lucene reader
    @type reader: L{IndexReader}
    
    @arg field: field name in index
    @type field: String
    
    @arg value: value to start enumerating with, defaults to '' (enumerate all
    values for this field)
    @type value: String

    @arg count_needed: whether we need an accurate count, or whether we can
    return as soon as we see a single valid document.
    @type count_needed: Boolean
    """
    assert IndexReader.instance_(reader)
    
    term_enumerator = reader.terms(Term(field, value))
    termDocs = reader.termDocs()
    while True:
        term = term_enumerator.term()
        if term is None or term.field() != field:
            break
        termDocs.seek(term_enumerator)
        count = 0
        while termDocs.next():
            value = term.text()
            count += 1
            if not count_needed:
                break
        if count:
            yield term.text(), count
        if not term_enumerator.next():
            break
    termDocs.close()
