import threading
import lucene
lucene.initVM(lucene.CLASSPATH)

from pylons import config
from ngv_frontend.model.lucene.collection import Collection
from ngv_frontend.model.lucene.Smarts import SmartField, SmartDoc
from ngv_analyzer import NGVAnalyzer


class TextField(SmartField):
    def __init__(self, value, store=False):
        SmartField.__init__(self, value,
                            store=store, index='tokenized',
                            termvector=True, alltext=True)


class LuceneAPI(object):

    def __init__(self, index_dir):
        self.collection = Collection(index_dir, create='auto')

    def __del__(self):
       self.collection.close()

    def update(self, video):
        fields = {
            '__id__': SmartField(unicode(video.video_id), store=True,
                                 index='untokenized', termvector=False),
            'title': TextField(video.title),
            'author': TextField(u', '.join([x.name for x in video.authors])),
            'tags': TextField(u', '.join([x.tag for x in video.tags])),
            'location': TextField(video.coverage_location),
            }
        for s in video.synopsis:
            fields['text_%s' % s.lang] = TextField(s.description)
        video_doc = SmartDoc(fields)
        self.collection.create(video_doc)

    def search(self, q):
       results = []
       hits = self.collection.search(q)
       for hit in hits:
           results.append({'score': hit.score, 
                           'id': int(hit.fields['__id__'])})
       return results



_lucene_api = LuceneAPI(config['lucene_index_path'])
def lucene_api():
    tl = threading.local()
    if not hasattr(tl, 'lucene_api'):
        tl.lucene_api = _lucene_api
        lucene.getVMEnv().attachCurrentThread()
    return tl.lucene_api
