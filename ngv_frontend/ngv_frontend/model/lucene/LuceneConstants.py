"""contains L{Constants} for L{lucene}"""
from lucene import Field

class LuceneConstantError(ValueError):
    """error raised when something goes wrong converting"""
    pass

class Constants(object):
    """parameters to control document storage
    
    See nested classes at
    http://lucene.apache.org/java/docs/api/org/apache/lucene/document/Field.html
    """
    ## storage parameters
    STORE_YES=Field.Store.YES
    STORE_NO=Field.Store.NO
    STORE_COMPRESS=Field.Store.COMPRESS
    
    @classmethod
    def validStore(cls, store):
        return cls.toConstant(store, 'store') in (cls.STORE_YES, cls.STORE_NO, cls.STORE_COMPRESS)
    
    ## indexing parameters
    INDEX_NO=Field.Index.NO
    INDEX_NO_NORMS=Field.Index.NO_NORMS
    INDEX_TOKENIZED=Field.Index.TOKENIZED
    INDEX_UNTOKENIZED=Field.Index.UN_TOKENIZED
    
    @classmethod
    def validIndex(cls, index):
        return cls.toConstant(index, 'index') in (cls.INDEX_NO, cls.INDEX_NO_NORMS,
                                                  cls.INDEX_TOKENIZED, cls.INDEX_UNTOKENIZED)
    
    ## term vector parameters
    TERMVECTOR_NO=Field.TermVector.NO
    TERMVECTOR_YES=Field.TermVector.YES
    TERMVECTOR_WITH_OFFSETS=Field.TermVector.WITH_OFFSETS
    TERMVECTOR_WITH_POSITIONS=Field.TermVector.WITH_POSITIONS
    TERMVECTOR_WITH_POSITIONS_OFFSETS=Field.TermVector.WITH_POSITIONS_OFFSETS
    
    @classmethod
    def validTermVector(cls, termvector):
        return cls.toConstant(termvector, 'termvector') in (cls.TERMVECTOR_NO, cls.TERMVECTOR_YES,
                                                            cls.TERMVECTOR_WITH_OFFSETS, cls.TERMVECTOR_WITH_POSITIONS,
                                                            cls.TERMVECTOR_WITH_POSITIONS_OFFSETS)
    
    @classmethod
    def toString(cls, c, which):
        """return a string"""
        orig_c=c
        which=which.upper()
        assert which in ('INDEX', 'STORE', 'TERMVECTOR')
        
        if isinstance(c, basestring):
            c=c.upper()
            if not (c.startswith('INDEX') or c.startswith('STORE') or
                    c.startswith('TERMVECTOR')):
                c="%s_%s"%(which, c)
            
            if c in cls.__dict__: return c
            else: raise LuceneConstantError, orig_c
        
        if isinstance(c, bool):
            if c: 
                if which=='INDEX': raise LuceneConstantError, orig_c
                else: return "%s_YES"%which
            else: return "%s_NO"%which            
        
        # c should be an instance of Field.* (or it's an error)
        try:
            return cls.__reversedict__[c]
        except KeyError:
            raise LuceneConstantError, orig_c
    
    @classmethod
    def toConstant(cls, s, which):
        """return a constant"""
        
        if Field.Store.instance_(s) or Field.Index.instance_(s) or Field.TermVector.instance_(s):
            return s
        
        orig_s=s
        which=which.upper()
        assert which in ('INDEX', 'STORE', 'TERMVECTOR')
        
        if isinstance(s, bool):
            if s: s="%s_YES"%which # this isn't valid for INDEX, we'll raise below
            else: s="%s_NO"%which
        elif isinstance(s, basestring): 
            s=s.upper()
            if not (s.startswith('INDEX') or s.startswith('STORE') or s.startswith('TERMVECTOR')):
                s="%s_%s"%(which, s)
        else:
            raise LuceneConstantError, orig_s
        
        try:
            return cls.__dict__[s]
        except KeyError:
            raise LuceneConstantError, orig_s

def _build_reversedict():
    # stuff a reverse mapping of attr to string in the class.
    Constants.__reversedict__=dict((v, k) for k, v in Constants.__dict__.iteritems() if k.isupper())

_build_reversedict()
