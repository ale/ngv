import logging
from lucene import Term, TermQuery
from OneOfQuery import OneOfQuery
from Smarts import *


ID_STR = '__id__'


class Collection(object):

    def __init__(self, index_dir, create=False, analyzer=None):
        self.storage = SmartStorage(index_dir, create, analyzer)

    @staticmethod
    def __oneUniqueIdQuery(unique_id):
        return TermQuery(Term(ID_STR, unique_id))

    @staticmethod
    def __manyUniqueIdsQuery(unique_ids):
        return OneOfQuery(ID_STR, unique_ids)

    def delete(self, unique_ids):
        if not isinstance(unique_ids, list):
            unique_ids = [unique_ids]
        for uuid in unique_ids:
            self.storage.delete(ID_STR, uuid)

    def retrieve(self, unique_ids):
        if isinstance(unique_ids, list):
            hits = self.storage.search(self.__oneUniqueIdQuery(unique_ids))
        else:
            hits = self.storage.search(self.__manyUniqueIdsQuery(unique_ids))
        return hits

    def create(self, doc):
        doc_id = doc.fields[ID_STR].value
        return self.storage.update(doc, ID_STR, doc_id)

    def search(self, q):
        return self.storage.search(q)

    def list(self):
        return list(self.storage.getAllTerms(ID_STR, include_counts=False))

    def close(self):
        self.storage.close()


