from pylons import config
from lucene import (StandardFilter, StandardTokenizer, StandardAnalyzer,
                    LowerCaseFilter, Token)


class PositionalStopFilter(object):
    def __init__(self, token_stream, stop_words):
        self.input = token_stream
        self.stop_words = set(stop_words)
    def next(self):
        increment = 0
        for token in self.input:
            if not token.termText() in self.stop_words:
                token.setPositionIncrement(token.getPositionIncrement() +
                                           increment)
                return token
            increment += 1
        return None


class NGVAnalyzer(StandardAnalyzer):
    def __init__(self):
        sw = set()
        swf = open(config['lucene_stop_words_file'], 'r')
        for line in swf.readlines():
            word = line.strip()
            if word:
                sw.add(unicode(word, 'iso-8859-1'))
        self.stop_words = sw
    def tokenStream(self, field_name, reader):
        return PositionalStopFilter(
            LowerCaseFilter(
                StandardFilter(
                    StandardTokenizer(reader)
                    ),
                ),
            self.stop_words)
