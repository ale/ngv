"""containts L{HitsWrapper}"""
from itertools import islice

class HitsWrapper(object):
    """a L{lucene.Hits} delegator that yields L{SmartHit} subclasses
    
    We support open intervals of the form hits[10:], but you almost certainly
    don't want to do this, as it will be slow.
    
    @warning: Instances of this class are not threadsafe.
    """
    
    __slots__=['__hits', '__class', '__cache', '__hits_iter']
    
    def __init__(self, hits, klass):
        """
        @arg hits: the raw hits to wrap
        @type hits: L{lucene.Hits}
        
        @arg klass: type of hit to yield up
        @type klass: class        
        """
        self.__hits=hits
        self.__hits_iter=iter(hits)
        self.__class=klass
        self.__cache=[]
    
    def __getattr__(self, attr):
        return getattr(self.__hits, attr)
    
    def __len__(self):
        return len(self.__hits)
    
    # XXX everything below is dramatically not threadsafe
    def __extend_cache(self, n):
        """extend the internal cache s.t. it contains n elements
        
        the cache may contain less than n elements after calling this method if
        the underlying hits are exhausted.
        """
        # unbounded slice - exhaust hits_iter
        if n is None:
            self.__cache.extend(self.__class(raw_hit) for raw_hit in self.__hits_iter)
            return
        
        # bounded -  if cache is long enough, just return
        if len(self.__cache) > n: return
        
        # bounded - extend cache to length n+1 (s.t. there is an element cache[n])
        self.__cache.extend(self.__class(raw_hit) for raw_hit in
                            islice(self.__hits_iter, n-len(self.__cache)+1))  
    
    def __getitem__(self, x):
        ## we only support getting items from the left. Getting items from the
        ## right is:
        ## 1) difficult to do correctly
        ## 2) absurdly slow
        
        ## XXX if you really want to get items from the right, you should
        ## reverse the sort of your underlying query.
        
        if isinstance(x, (int, long)):
            if x < 0:
                raise IndexError("negative indices not supported", x)
            elif x >= len(self):
                raise IndexError, x
            else:
                end=x
        elif isinstance(x, slice):
            if (x.start is not None and x.start < 0) or \
               (x.stop is not None and x.stop < 0) or \
               (x.step is not None and x.step < 0):
                raise IndexError("negative slices not supported", x)
            elif x.start >= len(self):
                return []
            else:
                end=x.stop
                if end is not None: end-=1
        else:
            raise TypeError, "indices must be integers"
        
        self.__extend_cache(end)
        return self.__cache[x]
    
    def __iter__(self):
        n=-1
        while 1:
            n+=1
            if n < len(self.__cache):
                yield self.__cache[n]
            else:
                self.__extend_cache(n)
                if n < len(self.__cache):
                    yield self.__cache[n]
                else:
                    return
