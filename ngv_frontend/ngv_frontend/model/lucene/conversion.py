"""convert python values to and from L{lucene}"""

from lucene import NumberTools
import time
import datetime

## Lucene can't store floats natively, so we need to intify. This gives us 6
## decimal places of precision
## XXX it'd be nice to specify this a per-field basis
_float_scale_factor = float(1e6)

## Lucene's
## U{DateTools<http://lucene.apache.org/java/2_2_0/api/org/apache/lucene/document/DateTools.html>}
## are moronic, so we have our own compatible versions. The automatic timezone
## conversion is particularly stupid.

def toLucene(value):
    """    
    @arg value: a value to be intelligently converted
    @type value: basic Python type
    
    @returns: a Lucene encoding of value
    @rtype: unicode
    """
    if value is None:
        return ''

    # we disallow str, since lucene assumes it's utf8 (I think)
    if isinstance(value, unicode):
        return value
    
    if isinstance(value, (int, long)):
        value=long(value) # XXX bug in lucene-JCC
        return NumberTools.longToString(value)
    
    if isinstance(value, float):
        scaled_float = long(value * _float_scale_factor)
        return NumberTools.longToString(scaled_float)

    if isinstance(value, datetime.datetime):
        return unicode(value.strftime('%Y%m%d%H%M%S'))

    if isinstance(value, datetime.date):
        return unicode(value.strftime('%Y%m%d'))
    
    if isinstance(value, datetime.time):
        return unicode(value.strftime('%H%M%S'))
    
    raise TypeError(value)

def fromLucene(value, typ):
    """    
    @arg value: a Lucene encoding of value
    @type value: unicode

    @arg typ: the desired Python type
    @type typ: type
    
    @returns: an instance of typ
    @rtype: typ
    """
    if isinstance(value, str):
        raise TypeError("Lucene does't support decoding str")
    
    if typ is unicode:
        return value
    
    if typ is str:
        raise TypeError("Lucene doesn't support converting to str")
    
    if typ is int or typ is long:
        return NumberTools.stringToLong(value)
    
    if typ is float:
        return float(NumberTools.stringToLong(value))/_float_scale_factor
    
    if typ is datetime.datetime:
        return datetime.datetime.strptime(value, '%Y%m%d%H%M%S')
    
    if typ is datetime.date:
        return datetime.datetime.strptime(value, '%Y%m%d').date()

    if typ is datetime.time:
        return datetime.datetime.strptime(value, '%H%M%S').time()
    
    raise TypeError, typ

    
    
