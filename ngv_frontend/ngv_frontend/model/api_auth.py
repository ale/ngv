from meta import *
from md5 import md5
from datetime import datetime
import random

from author import Author


def _gen_token(username):
    m = md5('%s-%d-%s' % (
            username, int(1000000 * random.random()),
            datetime.now().isoformat()))
    return m.hexdigest()


api_auth_table = Table(
    'api_auth', metadata,
    Column('token', String(48), primary_key=True),
    Column('author_id', Integer),
    Column('ip_addr', String(15)),
    Column('stamp', DateTime(), default=datetime.now),
    )


class ApiAuth(object):

    def generate(self, username=None, password=None):
        self.token = _gen_token(username)
        author = Author.query.filter_by(username=username).first()
        if not author or not author.check_password(password):
            raise Exception("Invalid credentials")
        self.author_id = author.author_id

    @classmethod
    def validate(cls, token):
        t = cls.query.filter_by(token=token).first()
        if t:
            t.stamp = datetime.now()
            return True
        else:
            return False


Session.mapper(ApiAuth, api_auth_table)
