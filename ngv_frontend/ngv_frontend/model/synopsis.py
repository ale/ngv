# $Id: synopsis.py 217 2007-01-17 18:14:01Z ale $

from meta import *

### sql tables


synopsis_table = Table('synopsis', metadata,
                       Column('synopsis_id', Integer, primary_key=True),
                       Column('video_id', Integer, ForeignKey('videos.video_id'), index=True),
                       Column('lang', String(2), index=True),
                       Column('description', Unicode),
                       Column('short_description', Unicode),
                       )


### classes 

class Synopsis(object):
    def __repr__(self):
        return '<Synopsis[%s] of video %s>' % (
            self.lang, str(self.video_id) )
    def modify(self):
        self.video.modify()
    def to_dict(self):
        return {'lang': self.lang,
                'description': self.description,
                'short_descr': self.short_description}


### object-relational mappings

Session.mapper(Synopsis, synopsis_table,
               properties=dict(description=deferred(synopsis_table.c.description)
                               )
              )

