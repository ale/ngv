
import mimetypes
import os
import re
import shutil

from meta import *
from ngv_frontend.lib import helpers as h
from pylons import g


docs_table = Table('documents', metadata,
                   Column('id', Integer, primary_key=True),
                   Column('parent_id', Integer, ForeignKey('documents.id')),
                   Column('title', String(128), unique=True),
                   )

revisions_table = Table('document_revisions', metadata,
                        Column('id', Integer, primary_key=True),
                        Column('document_id', Integer, ForeignKey('documents.id')),
                        Column('stamp', DateTime, default=func.current_timestamp()),
                        Column('username', String(32)),
                        Column('contents', Unicode()),
                        )

attachments_table = Table('document_attachments', metadata,
                          Column('id', Integer, primary_key=True),
                          Column('document_id', Integer, ForeignKey('documents.id')),
                          Column('name', String(128), index=True),
                          Column('mime_type', String(64)),
                          Column('stamp', DateTime, default=func.current_timestamp()),
                          Column('username', String(32)),
                          )


class Document(object):

    @classmethod
    def create(cls, title, contents, username, 
               parent_id=None, parent=None):
        if not parent:
            if parent_id:
                parent = cls.query.get(parent_id)
        doc = cls(title=title, parent=parent)
        Session.add(doc)
        rev1 = DocumentRevision(username=username, contents=contents)
        doc.revisions.append(rev1)
        Session.add(rev1)
        return doc

    @classmethod
    def index(cls, start_from=None, indent_level=0):
        """Returns a list of (id, parent_id, indent_level, title) tuples,
        ordered by depth-first."""
        if start_from:
            parent_id = start_from.id
        else:
            parent_id = None
        cur = cls.query.filter_by(parent_id=parent_id)\
            .order_by(cls.title)
        out = []
        for doc in cur:
            out.append((doc.id, doc.parent_id, indent_level, doc.title))
            out.extend(cls.index(doc, indent_level + 1))
        return out

    @classmethod
    def all_items_select(cls):
        """Returns a list of tuples suitable to be passed to the
        form.select() function."""
        out = [(0, 'NONE')]
        for dtuple in cls.index():
            (id, parent_id, off, title) = dtuple
            s = '%s%s' % ('&nbsp;&nbsp;' * off, title)
            out.append((id, s))
        return out

    def breadcrumbs(self):
        """Returns the list of parent documents up to the root."""
        out = [ self ]
        cur = self
        while cur.parent:
            cur = cur.parent
            out.insert(0, cur)
        return out

    def add_revision(self, contents, username):
        rev = DocumentRevision(username=username, contents=contents,
                               document=self)
        self.revisions.append(rev)

    def latest_revision(self):
        return DocumentRevision.query.filter_by(document_id=self.id)\
            .order_by(desc(DocumentRevision.stamp))\
            .limit(1)\
            .first()

    def find_attachment(self, name):
        return DocumentAttachment.query.filter_by(document_id=self.id,
                                                  name=name).first()


class DocumentRevision(object):

    __wikiwords = re.compile(r'(\b([A-Z]\w+[A-Z]+\w+)|\[(([A-Z]\w+):(.*))\]|\[(\w+)\])')

    def get_contents(self):
        """Returns contents with wikiwords transformed into links."""
        if not self.contents:
            return ''
        def replace_func(match):
            tag = match.group(4)
            if tag == 'Image':
                att = self.document.find_attachment(match.group(5))
                if att:
                    return '<img src="%s" border="0" />' % (
                        att.url_for(),)
            elif tag == 'Link':
                att = self.document.find_attachment(match.group(5))
                if att:
                    return h.link_to(att.name, url=
                                     h.url_for(action='attachment', id=att.id))
            elif not tag:
                title = match.group(1)
                if match.group(6):
                    title = match.group(6)
                return h.link_to(title,
                                 url=h.url_for(controller='/docs', action='view',
                                               title=title))
            return '<span class="wikierror">%s</span>' % match.group(1)
        return self.__wikiwords.sub(replace_func, self.contents)


class DocumentAttachment(object):
    def __init__(self, file=None, **kwargs):
        for k, v in kwargs.iteritems():
            setattr(self, k, v)
        if file is not None:
            self.name = file.filename.lstrip(os.sep)
            self.store(file)
            self.mime_type = mimetypes.guess_type(self.name)[0]

    def store(self, file):
        new_path = self.path()
        document_dir = os.path.dirname(new_path)
        if not os.path.isdir(document_dir):
            os.mkdir(document_dir)
        perm_file = open(new_path, 'w')
        shutil.copyfileobj(file.file, perm_file)
        file.file.close()
        perm_file.close()

    def path(self):
        return os.path.join(g.document_upload_path, 
                            str(self.document.id),
                            self.name)

    def size(self):
        return os.path.getsize(self.path())

    def url_for(self):
        return h.url_for(controller='docs',
                         action='attachment',
                         title=None,
                         id=self.id,
                         name=self.name)



mimetypes.init()

Session.mapper(DocumentRevision, revisions_table)
Session.mapper(DocumentAttachment, attachments_table)
Session.mapper(Document, docs_table, properties=dict(
        revisions=relation(DocumentRevision, lazy=True, 
                           cascade='all, delete-orphan',
                           order_by=desc(DocumentRevision.stamp),
                           backref='document'),
        attachments=relation(DocumentAttachment, lazy=True,
                             cascade='all, delete-orphan',
                             order_by=desc(DocumentAttachment.stamp),
                             backref='document'),
        children=relation(Document, lazy=True, cascade='none',
                          backref=backref('parent', lazy=True, cascade='none',
                                          remote_side=[docs_table.c.id])),
        ))
