# $Id: $

from meta import *


audit_table = Table('audit', metadata,
                    Column('id', Integer, primary_key=True),
                    Column('username', String(64), index=True),
                    Column('video_id', Integer, index=True),
                    Column('type', String(1), index=True),
                    Column('message', String),
                    Column('stamp', DateTime, default=func.current_timestamp())
                    )

class Audit(object):

    TYPE_NOTICE = 0
    TYPE_ERROR = 3
    
    @classmethod
    def log(cls, message, username=None, type=None, video_id=None):
        if not type:
            type = cls.TYPE_NOTICE
        msg = cls(type=type, username=username, 
                  message=message, video_id=video_id)
        Session.save(msg)
        Session.commit()

    @classmethod
    def error(cls, message, username=None):
        cls.log(message, username, cls.TYPE_ERROR)

    @classmethod
    def by_user(cls, username):
        return cls.query.filter_by(username=username)\
            .order_by(desc(cls.stamp))

    @classmethod
    def by_video_id(cls, video_id):
        return cls.query.filter_by(video_id=video_id)\
            .order_by(desc(cls.stamp))


Session.mapper(Audit, audit_table)
