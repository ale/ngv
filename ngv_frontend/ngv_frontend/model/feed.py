import feedparser
from meta import *
from datetime import datetime, timedelta
import time
import logging


feeds_table = Table('feeds', metadata,
                    Column('id', Integer, primary_key=True),
                    Column('url', String(255), unique=True),
                    Column('updated_at', DateTime, default=datetime.now),
                    Column('last_modified', DateTime),
                    Column('etag', String(64)),
                    Column('data', PickleType())
                    )


class FeedData(object):
    def __init__(self, **kw):
        for k, v in kw.iteritems():
            setattr(self, k, v)


class Feed(object):

    @classmethod
    def get(cls, url):
        now = datetime.now()
        f = cls.query.filter_by(url=url).first()
        if not f:
            f = cls(url=url)
        else:
            if (now - f.updated_at) < timedelta(0, 600):
                return f.data
        modified = f.last_modified and f.last_modified.timetuple() or None
        response = feedparser.parse(url, etag=f.etag, modified=modified)
        if response.bozo:
            logging.error('error downloading %s: %s' % (
                    url, str(response.bozo_exception)))
            return f.data
        logging.debug('downloaded %s, status=%d' % (url, response.status))
        if response.status == 304:
            return f.data
        if response.etag:
            f.etag = response.etag
        if hasattr(response, 'modified') and response.modified:
            f.last_modified = datetime.fromtimestamp(time.mktime(response.modified))
        f.data = FeedData(feed=response.feed, entries=response.entries)
        f.updated_at = now
        Session.add(f)
        Session.commit()
        return f.data


Session.mapper(Feed, feeds_table)

