#!/usr/bin/env python
# $Id: $

import os
import sys

from paste.deploy import appconfig
from pylons import config
from ngv.jobserver.api import JobServer
from ngv_frontend.config.environment import load_environment


def get_files(model, args):
    query = model.File.query.filter_by(ftype='video')
    for clause in args:
        if '=' in clause:
            clause, value = clause.split('=', 1)
        if clause == 'all':
            continue
        elif clause == 'role':
            query = query.filter_by(role=value)
        elif clause == 'video':
            video = model.Video.query.get(value)
            if not video:
                raise Exception("Video id not found!")
            query = query.filter_by(video=video)
        else:
            query = query.filter_by(file_id=int(clause))
    return query


def do_files(model, group, action, args, job_args):
    jobserver_url = config['jobserver_url']
    print 'Jobserver at %s' % jobserver_url
    jobserver = JobServer(server=jobserver_url)
    for file in get_files(model, args):
        print "scheduling %s/%s for %d: %s %s" % (
            group, action, file.file_id, file.name, str(job_args))
        file.schedule_job(jobserver, action, group, **job_args)
    model.Session.commit()


def main(args):
    if len(args) < 1:
        print "Usage: ngv-process-files <CONFIG-FILE> <FILEID>..."
        print "Example:"
        print "  ngv-process-files $PWD/development.ini incoming incoming 1733 \"local_file_path='/srv/ngvision/ngv_frontend/data/storage/1/1733'\""
        sys.exit(1)
    conf = appconfig('config:' + args[0])
    load_environment(conf.global_conf, conf.local_conf)
    import ngv_frontend.model as model

    group = args[1]
    action = args[2]
    filters = [args[3]]
    if len(args) > 3:
        job_args = eval("dict(%s)" % args[4])
    else:
        job_args = {}
    do_files(model, group, action, filters, job_args)


def entry_point():
    main(sys.argv[1:])


if __name__ == "__main__":
    entry_point()
    
