#!/usr/bin/env python
# $Id: $

import os
import sys
import time
import pprint
from datetime import datetime

from paste.deploy import appconfig
from pylons import config
from ngv_frontend.config.environment import load_environment


def update_archive_with_rss(model, archive):
    feed = model.Feed.get(archive.feed_url)
    pprint.pprint(feed.feed)
    pprint.pprint(feed.entries[0])
    for item in feed.entries:
        # check the title first
        if model.Video.query.filter_by(title=item['title']).first():
            print 'we already have "%s"' % item['title']
            continue
        # now the author
        author_name = item.get('author')
        if 'author_detail' in item:
            author_name = item['author_detail']['name']
        author = model.Author.query.filter_by(name=author_name).first()
        if not author:
            print 'creating author "%s"' % author_name
            author = model.Author(author_name, 
                                  status=model.Author.Status.AUTHOR,
                                  archive=archive)
        # create the video now
        print 'creating video "%s"' % item['title']
        video = model.Video(archive=archive,
                            title=item['title'],
                            status=model.Video.Status.PUBLISHED,
                            external_url=item['link'])
        video.authors.append(author)
        if 'summary' in item:
            syn = model.Synopsis(description=item['summary'],
                                 short_description=item['summary'],
                                 lang='en')
            video.synopsis.append(syn)
        if 'tags' in item:
            for tagdict in item['tags']:
                tag = model.Tag.query.filter_by(tag=tagdict['term']).first()
                if not tag:
                    tag = model.Tag(tag=tagdict['term'])
                video.tags.append(tag)
        if 'updated' in item:
            video.publication_date = datetime.fromtimestamp(
                time.mktime(item['updated_parsed']))
        # detect files in media enclosures
        if 'enclosures' in item:
            for enc in item['enclosures']:
                print 'enclosure: %s,%s,%s' % (
                    enc['type'], enc['type'], enc['href'])
                if enc['type'].startswith('video/'):
                    f = model.VideoFile(role='original')
                    dl = model.DirectLink(url=enc['href'])
                    f.direct_links.append(dl)
                elif enc['type'] == 'application/x-bittorrent':
                    f = model.P2PFile(role='torrent')
                    f.link = enc['href']
                f.size = int(enc['length'])
                f.mime_type = enc['type']
                f.name=os.path.basename(enc['href'])
                video.files.append(f)
        video.modify()
        model.Session.commit()



def main(args):
    if len(args) < 1:
        print "Usage: ngv-migrate <CONFIG-FILE>"
        sys.exit(1)
    conf = appconfig('config:' + args[0])
    load_environment(conf.global_conf, conf.local_conf)
    import ngv_frontend.model as model
    from ngv_frontend.model.lucene.ngv_glue import lucene_api

    if len(args) > 1:
        archive_id = int(args[1])
        archives = [model.Archive.query.get(archive_id)]
    else:
        archives = model.Archive.query.filter_by(sync_mode='R')

    for arc in archives:
        print
        print 'Archive "%s", url=%s' % (arc.name, arc.feed_url)
        update_archive_with_rss(model, arc)


def entry_point():
    main(sys.argv[1:])


if __name__ == "__main__":
    entry_point()
    
