#!/usr/bin/env python

import os
import sys

from paste.deploy import appconfig
from pylons import config
from ngv.jobserver.api import JobServer
from ngv_frontend.config.environment import load_environment


def main(args):
    if len(args) < 1:
        print "Usage: ngv-unlock-lucene <CONFIG-FILE>"
        sys.exit(1)
    conf = appconfig('config:' + args[0])
    load_environment(conf.global_conf, conf.local_conf)
    import ngv_frontend.model as model
    from ngv_frontend.model.lucene.ngv_glue import lucene_api

    l = lucene_api()
    def rmlock(l):
        lock_file_name = l.collection.storage.writer.WRITE_LOCK_NAME
        lock_file_path = os.path.join(config['lucene_index_path'], lock_file_name)
        if os.path.exists(lock_file_path):
            print "removing lockfile %s" % lock_file_path
            os.remove(lock_file_path)
    rmlock(l)
    print "optimizing..."
    l.collection.storage.optimize()
    l.collection.close()
    print "done."

    sys.exit(0)


def entry_point():
    main(sys.argv[1:])


if __name__ == "__main__":
    entry_point()
    
