#!/usr/bin/env python
# $Id: $

import os
import re
import sys
import urllib2
from datetime import datetime

from paste.deploy import appconfig
from pylons import config
from ngv_frontend.config.environment import load_environment


def try_download(url, outfile):
    o = open(outfile, 'w')
    try:
        print 'attempting download of %s' % url
        resp = urllib2.urlopen(url)
    except:
        o.close()
        return False 
    o.write(resp.read())
    o.close()
    return True


def download_favicon(model, archive):
    outfile = os.tmpnam()
    fmt_prefix = ''
    # first, try the feed icon (might be better resolution)
    feed = model.Feed.get(archive.feed_url)
    if 'image' in feed.feed:
        if try_download(feed.feed['image']['href'], outfile):
            return (fmt_prefix, outfile)
    # then try standard favicon
    fmt_prefix = 'ico:'
    if try_download(os.path.join(archive.homepage, 'favicon.ico'), outfile):
        return (fmt_prefix, outfile)
    # finally we'll have to parse the stupid html page
    resp = urllib2.urlopen(archive.homepage)
    html = resp.read()
    m = re.search(r'href="?([-_/.a-zA-Z0-9]+/favicon.ico)', html)
    if m:
        if try_download(archive.homepage + m.group(1), outfile):
            return (fmt_prefix, outfile)
    print 'could not find favicon!'
    return (None, None)


def get_favicon(model, archive):
    (fmt_prefix, in_file) = download_favicon(model, archive)
    if not in_file:
        return
    out_file = os.tmpnam() + '.gif'
    os.system("convert %s%s -geometry 24x24 %s" % (
        fmt_prefix, in_file, out_file))
    data = open(out_file).read()
    os.unlink(in_file)
    os.unlink(out_file)
    return data


def add_favicon(model, archive):
    if archive.icon:
        return
    icon = get_favicon(model, archive)
    archive.icon = icon
    model.Session.update(archive)
    model.Session.commit()
    print 'set new icon for "%s"' % archive.name


def main(args):
    if len(args) < 1:
        print "Usage: ngv-migrate <CONFIG-FILE>"
        sys.exit(1)
    conf = appconfig('config:' + args[0])
    load_environment(conf.global_conf, conf.local_conf)
    import ngv_frontend.model as model
    from ngv_frontend.model.lucene.ngv_glue import lucene_api

    archive_id = int(args[1])
    archive = model.Archive.query.get(archive_id)
    add_favicon(model, archive)


def entry_point():
    main(sys.argv[1:])


if __name__ == "__main__":
    entry_point()
    
