#!/usr/bin/env python
#
# Import videos from the current NGV archive
# using the public XML-RPC API.
#

import os
import sys
import pprint
import urllib2
import random
import xmlrpclib
from datetime import datetime

use_pil = True
try:
    import Image
except ImportError:
    use_pil = False

from paste.deploy import appconfig
from pylons import config
from ngv_frontend.config.environment import load_environment


# API XML-RPC di ngvision
ngv_server = xmlrpclib.ServerProxy('http://ngvision.org/RPC2')


# tabella che traduce le categorie in italiano in quelle in inglese
# (perche' sono in inglese nel nuovo database...)
categories_map = {
    'crisi internazionali e conflitti': 'war and global crisis',
    'hacking, diritti digitali, net-art': 'hacking, digital rights, net-art',
    'sorveglianza e controllo': 'surveillance and control',
    'reddito e lavoro': 'income and work',
    'migranti, cittadinanza, noborder': 'migrants, citizenship, noborder',
    'repressione carcere': 'jail and repression',
    'antifascismo': 'antifascism',
    'antimafie': 'antimafia',
    'identita\' genere sessualita\'': 'identity, gender, sexuality',
    'saperi, documenti, formazione': 'knowledge, documents, formation',
    'religioni': 'religion',
    'azione diretta': 'direct action',
    'storia e memoria': 'history and memory',
    'ecologia ambiente salute': 'ecology, health',
    'musica, teatro, danza, arte': 'music, theatre, dance, art',
    'squatting csa autogestioni': 'squatting, csa',
    'fiction corti film': 'fiction, short movies',
    'GenovaG8': 'genoa g8',
    }

def get_category(model, catname):
    tr_catname = unicode(categories_map.get(catname, catname), 'utf-8')
    c = model.Category.query.filter_by(name=tr_catname).first()
    return c


def my_tmpnam():
    return '/tmp/ngv-migration-%d-%08x' % (
        os.getpid(), 
        random.randrange(0, 1<<32)
        )


# per qualche motivo l'encoding delle risposte xml-rpc e' sballato
def fix_enc(s):
    if isinstance(s, unicode):
        return unicode(s.encode('iso-8859-1'), 'utf-8')
    else:
        return unicode(s, 'utf-8')


def nl2br(s):
    return s.replace('\n', '<br/>')


def parsedate(s):
    if s == '0000-00-00':
        return datetime.now()
    year, month, day = map(int, s.split('-'))
    return datetime(year, month, day)


def get_info(videoid):
    info = ngv_server.ngv.get_info(videoid)
    rel_path = 'disc%s/%s' % (info['cd'], info['filename'])
    info['urls'] = [
        'http://ngv.bradipz.net/new_global_vision/%s' % rel_path,
        'ftp://dist.ngvision.org/new_global_vision/%s' % rel_path
        ]
    return info


def get_screenshot(videoid):
    try:
        url = 'http://ngvision.org/shots/%d.jpg' % videoid
        response = urllib2.urlopen(url)
    except urllib2.HTTPError:
        return None
    data = response.read()
    response.close()
    return data


def scale_image(orig_file, orig_data):
    tmp_scaled = my_tmpnam() + '.jpg'
    if use_pil:
        img = Image.new(orig_data)
    else:
        status = os.system("convert '%s' -geometry 80x53 '%s' 2>/dev/null" % (orig_file, tmp_scaled))
        if not os.path.exists(tmp_scaled):
            return None
    return tmp_scaled


def create_screenshots(model, videoid, video, storage):
    data = get_screenshot(videoid)
    if not data:
        return
    tmp_orig = my_tmpnam() + '.jpg'
    f = open(tmp_orig, 'w')
    f.write(data)
    f.close()

    shot_orig = model.ScreenshotFile(video=video,
                                     role='screenshot',
                                     mime_type='image/jpeg',
                                     size=len(data))
    model.Session.save(shot_orig)
    model.Session.commit()
    shot_orig.upload(tmp_orig, storage)

    scaled_path = scale_image(tmp_orig, data)
    if scaled_path:
        shot_small = model.ScreenshotFile(video=video,
                                          role='screenshot_small',
                                          mime_type='image/jpeg',
                                          size=os.path.getsize(scaled_path))
        model.Session.save(shot_small)
        model.Session.commit()
        shot_small.upload(scaled_path, storage)
        os.unlink(scaled_path)
    os.unlink(tmp_orig)



authors_cache = {}
def get_author(authorid):
    if not authorid in authors_cache:
        info = ngv_server.ngv.get_author(authorid)
        authors_cache[authorid] = info
    return authors_cache[authorid]


def create_author(model, info):
    aname = fix_enc(info['name'])
    ea = model.Author.query.filter_by(name=aname).first()
    if ea:
        return ea
    a = model.Author(
        name = aname,
        description = fix_enc(info['description']),
        email = info['email'],
        website = info['url'],
        status = model.Author.Status.AUTHOR
        )
    return a


def create_video(model, info, author):
    vtitle = fix_enc(info['title'])
    v = model.Video.query.filter_by(title=vtitle).first()
    if v:
        return v
    v = model.Video(
        title = vtitle,
        publication_date = parsedate(info['pub_date']),
        coverage_date = parsedate(info['date']),
        coverage_location = fix_enc(info['location']),
        coverage_country = 'IT',
        status = model.Video.Status.PUBLISHED,
        license = 'by-nc-sa',
        )
    s = model.Synopsis(
        lang = 'it',
        description = nl2br(fix_enc(info['text'])),
        short_description = fix_enc(info['text_short'])
        )
    v.synopsis.append(s)
    if info['filename']:
        v.file_base_name = info['filename'][:-4]
        f = model.VideoFile(
            role = 'original',
            name = info['filename'],
            publication_date = parsedate(info['date']),
            size = int(info['size']),
            duration = info['duration'],
            audio_lang = info['language'][:2],
            )
        for url in info['urls']:
            dl = model.DirectLink(url=url)
            f.direct_links.append(dl)
        v.files.append(f)
    cat = get_category(model, info['keywords'].strip())
    if cat:
        v.categories.append(cat)
    else:
        if info['keywords'] == 'telestreet':
            v.tags.append(model.Tag('telestreet'))
        if info['keywords'] == 'oceania':
            v.tags.append(model.Tag('oceania'))
    if author:
        v.authors.append(author)
    v.modify()
    return v


def main(args):
    if len(args) < 1:
        print "Usage: ngv-migrate <CONFIG-FILE> [<VIDEO-ID>]"
        sys.exit(1)
    conf = appconfig('config:' + args[0])
    load_environment(conf.global_conf, conf.local_conf)
    import ngv_frontend.model as model
    from ngv_frontend.model.lucene.ngv_glue import lucene_api
    from ngv.storage.local_storage import LocalStorage

    storage = LocalStorage(config['local_storage_path'])
    if len(args) > 1:
        videoids = [int(args[1])]
    else:
        videoids = xrange(1400)
    old_id_map = {}
    for i in videoids:
        try:
            info = get_info(i)
        except Exception, e:
            print "%d: Does not exist" % i
            continue
        try:
            print "(%d) %s" % (i, info['title'].encode('utf-8'))
            #pprint.pprint(info)
            author_info = get_author(info['author_id'])
            author = create_author(model, author_info)
            video = create_video(model, info, author)
            model.Session.update(video)
            model.Session.commit()
            shots = create_screenshots(model, i, video, storage)
            old_id_map[i] = video.video_id
            model.Audit.log(
                'Imported from http://ngvision.org/mediabase/%d' % i,
                username='admin', video_id=video.video_id
                )
        except Exception, e:
            print "%d: Exception: %s" % (i, str(e))

    print "cleaning up Lucene storage..."
    l = lucene_api()
    l.collection.storage.optimize()
    l.collection.close()
    print "done."


def entry_point():
    main(sys.argv[1:])


if __name__ == "__main__":
    entry_point()
    
