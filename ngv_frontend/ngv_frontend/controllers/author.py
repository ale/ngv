import logging

from ngv_frontend.lib.base import *

log = logging.getLogger(__name__)

class AuthorController(BaseController):

    def index(self, id):
        c.author = model.Author.query.get(int(id))
        if (not h.logged_in() and c.author.status != model.Author.Status.AUTHOR \
                and c.author.status != model.Author.Status.ADMIN) or not c.author:
            #abort(404)
            redirect_to('list?e=1')
        res = model.VideoQuery.query([
            ('author', c.author.author_id),
            ('latest', None)
            ])
        c.num_published_videos = res.query.count()
        c.latest_videos = res.query.limit(3).all()
        return render('author/single.html')

    def list(self):
        c.page_num = int(request.params.get('p', 0))
        c.err = request.params.get('e', None)
        c.authors = h.Page(
            model.Author.all_authors(status=model.Author.Status.AUTHOR),
            page=c.page_num)
        return render('author/list.html')
        
    @authorize(permission.LoggedIn())
    def edit(self, id):
        c.author = model.Author.query.get(int(id))
        if not h.is_admin() and h.logged_author() != c.author:
            return 'Unauthorized'
        return render('author/edit.html')

    @validate(schema=model.forms.EditAuthorForm(), form='edit')
    @authorize(permission.LoggedIn())
    def edit_commit(self, id):
        c.author = model.Author.query.get(int(id))
        c.author.email = self.form_result.get('email')
        c.author.website = self.form_result.get('website')
        c.author.description = self.form_result.get('description')
        c.author.preferred_language = self.form_result.get('preferred_language')
        if h.is_admin():
            c.author.name = request.params.get('name')
            c.author.username = request.params.get('username')
            c.author.status = request.params.get('status')
            groups = request.params.get('groups').split(',')
            c.author.groups = []
            for group in [x.strip() for x in groups]:
                if not group:
                    continue
                g = model.AuthorGroup.get(group)
                c.author.groups.append(g)
        model.Session.update(c.author)
        model.Session.commit()
        model.Audit.log('Author information modified', 
                        username=h.logged_author().username)
        redirect_to(action='index')
