import logging
import md5
import random
import re
import os
from sqlalchemy import desc
from datetime import datetime, timedelta

from ngv_frontend.lib.base import *
from ngv_frontend.model.lucene.ngv_glue import lucene_api

from pylons.decorators.cache import beaker_cache

log = logging.getLogger(__name__)


class TolerantDictionary(dict):
    def __getattr__(self, key):
        return self.get(key, None)


class VideoController(BaseController):

    def _get_video(self, id):
        video = model.Video.query.filter_by(public_id=id).first()
        if not video:
            video = model.Video.query.filter_by(slug=id).first()
            if not video:
                video = model.Video.query.get(id)
                if not video:
                    abort(404)
        return video

    def index(self, id):
        """Show information on a single video."""
        c.video = self._get_video(id)
        if (c.video.status != model.Video.Status.PUBLISHED 
            and not h.logged_in()):
            abort(404)
        if h.logged_in() and h.logged_author().is_admin():
            c.audit_log = model.Audit.by_video_id(c.video.video_id)
        return render('video/single.html')

    def component(self, id):
        """AJAX callback that renders only a video snippet."""
        c.video = self._get_video(id)
        return render('video/component.html')

    @beaker_cache(expire=600, type='memory', query_args=True)
    def search(self):
        c.query = request.params.get('q')
        if c.query:
            results = lucene_api().search(c.query)
            c.page_num = int(request.params.get('p', 0))
            # TODO: insert conditional pagination in template
            c.results = h.Page(
                results, item_count=len(results),
                page=c.page_num, items_per_page=20)
            # TODO: make lazy
            for r in c.results:
                r['video'] = model.Video.query.get(r['id'])
        return render('video/search.html')

    def list(self, what, arg=None):
        """List videos according to several criteria specified by the
        'what' argument (and, eventually, the 'arg' parameter).
        """
        if what == 'queue' \
                and not (h.logged_in() and h.in_group('reviewers')):
            return 'Unauthorized.'
        res = model.VideoQuery.query([(what, arg)])
        # paginate the videos
        page_num = int(request.params.get('p', 0))
        c.videos = h.Page(
            res.query, item_count=res.count,
            page=page_num, items_per_page=10)
        c.title = res.title
        c.what = what
        c.arg = arg
        c.page_num = page_num
        return render('video/list.html')

    def rss(self, what, arg=None):
        """Similar to list, render an RSS 2.0 feed. The caching 
        strategy is also simpler."""
        res = model.VideoQuery.query([(what, arg)])
        c.videos = res.query.order_by([desc(model.Video.publication_date)]).limit(15).all()
        c.title = res.title
        etag = md5.md5(u''.join([x.title for x in c.videos]).encode('utf-8')).hexdigest()
        etag_cache(etag)
        response.headers['Expires'] = h.httpdate(datetime.now() + timedelta(1))
        response.headers['Last-Modified'] = h.httpdate(c.videos[0].publication_date)
        response.headers['Content-Type'] = 'application/rss+xml'
        cache_key = "%s/%s" % (what, arg or '_')
        return render('video/rss.html', cache_key=cache_key, cache_expire=900)

    @authorize(permission.LoggedIn())
    def upload(self):
        return render('video/upload.html', cache_expire=3600)

    @authorize(permission.LoggedIn())
    def edit(self, id=None):
        """Show the form to edit video metadata."""
        if id is None:
            #request.params.get('new') == '1':
            c.video = model.Video()
            c.is_new_video = 1
        else:
            c.video = self._get_video(id)
            if not (h.logged_author().is_admin() 
                    or h.logged_author() in c.video.authors):
                return 'Unauthorized.'
            c.is_new_video = 0
        c.all_categories = model.Category.by_archive(model.Archive.local())
        c.all_authors = model.Author.select_list(
            archive=model.Archive.local(),
            not_status=model.Author.Status.PENDING)
        return render('video/edit.html')

    @validate(schema=model.forms.EditVideoForm(), form='edit')
    @authorize(permission.LoggedIn())
    def edit_commit(self, id=None):
        """Modify video metadata according to form submission."""
        if id:
            video = self._get_video(id)
        else:
            video = model.Video(status=model.Video.Status.INCOMING)
        is_new = int(request.params.get('new'))
        
        # set simple parameters
        args = dict(title=self.form_result.get('title'),
                    coverage_date=self.form_result.get('cov_date'),
                    coverage_location=self.form_result.get('cov_location'),
                    coverage_country=self.form_result.get('cov_country').upper(),
                    license=request.params['license'],
                    published_at=self.form_result.get('pub_date'),
                    tags=self.form_result.get('keywords'),
                    categories=request.params.getall('categories'),
                    username=h.logged_author().username)
        if h.logged_author().is_admin():
            args['status'] = request.params.get('status')
            #args['puglished_at'] = self.form_result.get('pub_date')
        for lang in g.languages.keys():
            args['syn_%s' % lang] = request.params.get('syn_%s' % lang)
            args['syn_short_%s' % lang] = request.params.get('syn_short_%s' % lang)

        # authors
        if is_new:
            audit_fmt = 'Created video %s (%d)'
            args['authors'] = [h.logged_author().author_id]
        else:
            audit_fmt = 'Edited video %s (%d)'
            # only modify authors if in group reviewers
            if h.in_group('reviewers'):
                args['authors'] = request.params.get('authors').split(',')

        video.update(admin=h.logged_author().is_admin(), **args)

        model.Audit.log(audit_fmt % (video.title, video.video_id), 
                        username=h.logged_author().username, 
                        video_id=video.video_id)
        if is_new:
            c.video = video
            return render('video/upload_successful.html')
        redirect_to(action='edit', id=video.video_id)

    @authorize(permission.LoggedIn())
    def files(self, id):
        """Show the list of attached files, and the form to uplad a new one."""
        c.video = self._get_video(id)
        if not (h.logged_author().is_admin() 
                or h.logged_author() in c.video.authors):
            return 'Unauthorized.'
        c.file_id = request.params.get('f')
        if c.file_id:
            c.file = model.File.query.get(c.file_id)
            c.defaults = TolerantDictionary(
                c.file.__dict__.items())
        else:
            c.defaults = TolerantDictionary(
                file_type='video', file_role='original',)
        if not c.video.files:
            c.is_first_file = True
        return render('video/files.html')

    @authorize(permission.LoggedIn())
    def files_commit(self, id):
        """Modify/upload a file."""
        video = self._get_video(id)
        if not (h.logged_author().is_admin() 
                or h.logged_author() in video.authors):
            return 'Unauthorized.'

        # see if the file is new or not
        is_new = False
        has_file = False
        file_id = request.params.get('f')
        if file_id is None or file_id == '':
            is_new = True
            has_file = True
            # recover the file itself, either from the AsyncUpload 
            # pool, or directly from the POST data if no async http
            # upload was used
            if 'token' in request.params:
                async_http = True
                token = request.params.get('token')
                upload = model.AsyncUpload.query.filter_by(token=token).first()
                if not upload:
                    log.error("Could not retrieve async upload %s" % token)
                    abort(404)
                file_size = upload.size
                local_file_path = upload.path()
            else:
                async_http = False
                post_file = request.POST['file']
                file_size = len(post_file.value)
                local_file_path = os.tmpnam()
                f = open(local_file_path, 'w')
                f.write(post_file.value)
                f.close()
            file_type = request.params['ftype']
            file_class = model.FileType.choose(file_type)
            file = file_class(video=video, size=file_size)
        else:
            file = model.File.query.get(file_id)
            file_type = file.ftype

        # sanity check
        if is_new and not has_file:
            raise Exception("File is new but no file was uploaded")
            
        # now set type-specific parameters from request data
        log.info('UPLOAD: ftype=%s, params=%s' % (file_type, request.params))
        if file_type == 'video':
            file.role = request.params['video_role']
            file.audio_lang = request.params['audio_lang']
            file.subtitles_lang = request.params['subtitles_lang']
        elif file_type == 'subtitles':
            file.lang = request.params['lang']
        elif file_type == 'other':
            file.role = request.params['other_role']

        # here we save the file somewhere
        if has_file:
            file.name = file.guess_filename()
            try:
                file.upload(local_file_path, g.local_storage)
            except (OSError, IOError), e:
                log.error('video %s: Error uploading: %s' % (
                        str(video.video_id), str(e)))
                model.Session.delete(file)
                abort(500)

        # leave a trace in the audit log
        if is_new:
            msgstr = 'Uploaded new file %d/%s/%s (%d bytes)' % (
                file.file_id, file.ftype, file.role, file.size)
        else:
            msgstr = '%s file %d' % (
                (has_file and 'Re-uploaded' or 'Edited'), file.file_id)
        model.Audit.log(msgstr, username=h.logged_author().username, 
                        video_id=video.video_id)

        video.modify()
        model.Session.add(file)
        model.Session.add(video)
        model.Session.commit()

        # schedule new 'original' videos for processing
        if has_file and file.role == 'original':
            # use the path from the local storage, so that we can
            # safely remove the temporary file (see later comment)
            job_file_path = g.local_storage.map_key(file.key())
            file.schedule_job(g.jobserver, action='incoming', 
                              group='incoming',
                              local_file_path=job_file_path)
            model.Session.add(file)
            model.Session.commit()

        # since the file was copied somewhere, we can now safely
        # remove it from the incoming directory
        #if is_new and async_http:
        #    upload.remove()

        redirect_to(action='files')

    @authorize(permission.LoggedIn())
    def delete_file(self, id):
        video = self._get_video(id)
        file = model.File.query.get(request.params['f'])
        if file not in video.files:
            abort(400)
        model.Audit.log('Deleted file %d/%s/%s' % (file.file_id, file.ftype, file.role),
                        username=h.logged_author().username,
                        video_id=video.video_id)
        video.files.remove(file)
        model.Session.delete(file)
        model.Session.commit()
        return 'OK'

    @authorize(permission.LoggedIn())
    def schedule_job(self, id):
        action = request.params['j_action']
        group = request.params.get('group', 'processing')
        video = self._get_video(id)
        file = model.File.query.get(request.params['f'])
        file.schedule_job(g.jobserver, action=action, group=group)
        model.Session.add(file)
        model.Session.commit()
        return 'OK'

    def comments(self, id):
        """Show the list of comments for a video."""
        c.video = self._get_video(id)
        return render('video/comments.html')

    def reviews(self, id):
        """Show the list of reviews for a video."""
        c.video = self._get_video(id)
        return render('video/reviews.html')

    @authorize(permission.IsAdmin())
    def comment_delete(self, id):
        """Delete a comment."""
        comment_id = request.params.get('c')
        comment = model.Comment.query.get(comment_id)
        model.Session.delete(comment)
        model.Session.commit()
        return 'OK'
    
    @validate(schema=model.forms.CommentForm(), form='comments')
    @authorize(permission.LoggedIn())
    def comment_commit(self, id):
        """Create a new comment."""
        video = self._get_video(id)
        cmt = model.Comment(type=model.Comment.Type.COMMENT,
                            author=h.logged_author(),
                            text=self.form_result.get('text'))
        video.comments.append(cmt)
        model.Session.add(cmt)
        model.Session.commit()
        redirect_to(action='comments')

    @validate(schema=model.forms.ReviewForm(), form='reviews')
    @authorize(permission.LoggedIn())
    def review_commit(self, id):
        """Create a new review."""
        video = self._get_video(id)
        cmt = model.Comment(type=model.Comment.Type.REVIEW,
                            author=h.logged_author(),
                            text=self.form_result.get('text'))
        video.reviews.append(cmt)
        if video.status == model.Video.Status.CONFIRMED:
            model.Audit.log('Review of %s (%d) from %s' % (
                    video.title, video.video_id, h.logged_author().username))
            video.set_status(model.Video.Status.REVIEW)
        model.Session.add(cmt)
        model.Session.commit()
        redirect_to(action='reviews')
