import logging

from ngv_frontend.lib.base import *

log = logging.getLogger(__name__)

class AccountController(BaseController):

    def index(self):
        redirect_to(action='manage')
    
    @authorize(permission.LoggedIn())
    def manage(self):
        """Account management page. Also shows messages, warnings etc.
        relative to the current logged in user."""
        c.author = h.logged_author()
        c.audit_log = model.Audit.by_user(c.author.username)
        if request.params.get('full_audit') != 'y':
            c.audit_log = c.audit_log.limit(8)
        # show number of videos awaiting review, if any
        if h.in_group('reviewers'):
            inc = model.VideoQuery.query([('queue', 'incoming')])
            c.to_review = inc.count
            rev = model.VideoQuery.query([('queue', 'reviewed')])
            c.to_publish = rev.count
        return render('account/manage.html')

    @authorize(permission.LoggedIn())
    def changepw(self):
        """Render the change password form."""
        c.author = h.logged_author()
        return render('account/changepw.html')

    @authorize(permission.LoggedIn())
    @validate(schema=model.forms.ChangePasswordForm(), form='changepw')
    def changepw_commit(self):
        """Change the password of the user."""
        author = h.logged_author()
        author.password = model.Author.encrypt(self.form_result.get('password'))
        model.Session.update(author)
        model.Session.commit()
        model.Audit.log('Password change', username=author.username)
        redirect_to(action='manage')

    def resetpw_request(self):
        """Request a password change. Sends a mail to the user with
        the password reset code."""
        username = request.params.get('u')
        a = model.Author.query.filter_by(username=username).one()
        if not a:
            abort(404)
        c.reset_url = h.url_for(action='resetpw',
                                id=a.author_id,
                                token=a.resetpw_token())
        # send email to user with 'reset_url'
        return render('account/resetpw_request.html')

    def resetpw(self, id, token):
        """Link for password reset. Confirms the password reset code."""
        a = model.Author.query.get(id)
        if not a:
            abort(404)
        if token != a.resetpw_token():
            abort(400)
        model.Audit.log('Reset password from %s' % (
                request.environ.get('REMOTE_ADDR', 'unknown'),),
                        username=a.username)
        new_password = a.generate_password()
        a.password = model.Author.encrypt(new_password)
        model.Session.update(a)
        model.Session.commit()
        # send email with new password
        c.new_password = new_password
        return render('account/resetpw.html')

    def register(self):
        """Show registration form."""
        return render('account/register_1.html')

    @validate(schema=model.forms.RegistrationForm(), form='register')
    def register_2(self):
        """Register a new user."""
        new_pass = model.Author.generate_password()
        a = model.Author(name=self.form_result.get('name'),
                         email=self.form_result.get('email'),
                         website=self.form_result.get('website'),
                         description=self.form_result.get('description'),
                         password=model.Author.encrypt(new_pass),
                         status=model.Author.Status.PENDING,
                         group='users')
        model.Session.commit()
        model.Audit.log('Registration request from %s' % (
                request.environ.get('REMOTE_ADDR', 'unknown'),),
                        username=a.username)
        # send an email to the user with its login data
        c.new_username = a.username
        c.password = new_pass
        return render('account/register_2.html')
