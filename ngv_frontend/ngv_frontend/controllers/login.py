import logging

from ngv_frontend.lib.base import *
from datetime import datetime

log = logging.getLogger(__name__)

class LoginController(BaseController):

    def __checkpass(self, username, password):
        a = model.Author.query.filter_by(username=username).one()
        if a:
            if a.password == model.Author.encrypt(password):
                return a
        return None

    def login(self):
        if not (request.params.get('username') and request.params.get('password')):
            return render('account/login.html', cache_expire='never')

        c.username = request.params['username']
        a = self.__checkpass(request.params['username'],
                             request.params['password'])
        if a:
            request.environ['paste.auth_tkt.set_user'](str(a.username))
            session['logged_in'] = 1
            session['user_id'] = a.author_id
            session['groups'] = [ g.name for g in a.groups ]
            session.save()
            # set lastlog
            #if a.last_login:
            #    h.flash_message(_('Last login on %s') % str(a.last_login))
            a.last_login = datetime.now()
            # confirm the user at first login
            if a.status == model.Author.Status.PENDING:
                a.status = model.Author.Status.CONFIRMED
                model.Audit.log('Registration confirmed', a.username)
            model.Session.update(a)
            model.Session.commit()
            model.Audit.log('Login', a.username)
            redirect_to('/account/manage')
        else:
            c.message = "The username and password that you supplied were not correct."
        return render('account/login.html')

    def logout(self):
        model.Audit.log('Logout', h.logged_author().username)
        session['logged_in'] = 0
        session.delete()
        return render('account/logout.html')
