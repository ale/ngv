import logging

from ngv_frontend.lib.base import *

log = logging.getLogger(__name__)


def iter_and_close(fd):
    while 1:
        buf = fd.read(65536)
        if not buf:
            fd.close()
            raise StopIteration
        yield buf


class DownloadController(BaseController):

    def index(self, id):
        file = model.File.query.filter_by(file_id=id).first()
        if not file:
            abort(404)
        url = file.get_url(g.local_storage)
        # prevent infinite recursion
        if url.startswith('/'):
            etag = '%d-%s' % (file.file_id, file.modified_at.strftime("%s"))
            etag_cache(etag)
            response.headers['Last-Modified'] = file.modified_at
            response.headers['Content-Length'] = file.size
            if file.mime_type:
                response.headers['Content-Type'] = file.mime_type
            if (not file.mime_type) or (file.mime_type[:6] != 'image/'):
                response.headers['Content-Disposition'] = 'download; filename="%s"' % file.name
            # use acceleration
            full_path = g.local_storage.map_key(file.key())
            accel_url = '/_download' + full_path[len(g.local_storage.base_path):]
            response.headers['X-Accel-Redirect'] = accel_url
            #return iter_and_close(file.open(g.local_storage))
        redirect_to(url)
