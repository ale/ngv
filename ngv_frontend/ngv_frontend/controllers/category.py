import logging

from ngv_frontend.lib.base import *

log = logging.getLogger(__name__)

class CategoryController(BaseController):

    def index(self):
        """Return the list of categories."""
        c.categories = model.Category.by_archive(model.Archive.local()).all()
        return render('/channels.html')

