import logging

from ngv_frontend.lib.base import *

log = logging.getLogger(__name__)

class TagCloudController(BaseController):

    def index(self):
        cloud = []
        max_count = 0
        min_count = 999999
        for tag in model.Tag.all():
            count = len(tag.videos)
            if count > max_count:
                max_count = count
            if count < min_count:
                min_count = count
            cloud.append((tag.tag, count))
        if max_count == min_count:
            max_count += 1
        min_size, max_size = (10, 36)
        def to_size(v):
            return int(min_size + 
                       (max_size - min_size) * 
                       (v - min_count) / (max_count - min_count))
        c.cloud = cloud
        c.to_size = to_size
        return render('/tag_cloud.html')

