import logging
from datetime import datetime
from pylons.controllers import XMLRPCController

from ngv_frontend.lib.base import *

log = logging.getLogger(__name__)


class ApiController(XMLRPCController):

    allow_none = True

    def __call__(self, environ, start_response):
        try:
            return XMLRPCController.__call__(self, environ, start_response)
        finally:
            model.Session.remove()

    def _validate(self, token):
        if not model.ApiAuth.validate(token):
            abort(403)

    def GetAuthToken(self, username, password):
        api_token = model.ApiAuth()
        api_token.generate(username, password)
        model.Session.add(api_token)
        model.Session.commit()
        return api_token.token

    def GetVideoById(self, id):
        v = model.Video.query.get(id)
        return v.to_dict()

    def GetVideoByTitle(self, title):
        v = model.Video.query.filter_by(title=title).first()
        if not v:
            abort(404)
        return v.to_dict()

    def GetAuthorById(self, id):
        a = model.Author.query.get(id)
        return a.to_dict()

    def GetAuthorByName(self, name):
        a = model.Author.query.filter_by(name=name).first()
        if not a:
            abort(404)
        return a.to_dict()

    def GetVideos(self, token, idlist):
        self._validate(token)
        videos = model.Video.query.filter(
            model.Video.video_id.in_(idlist))
        return [x.to_dict() for x in videos]

    def GetAuthors(self, token, idlist):
        self._validate(token)
        authors = model.Author.query.filter(
            model.Author.author_id.in_(idlist))
        return [x.to_dict() for x in authors]

    def GetChangedVideos(self, token, timestamp=0):
        self._validate(token)
        threshold = datetime.fromtimestamp(timestamp)
        videos = model.Video.query.filter(
            model.Video.modified_at > threshold)
        return [x.video_id for x in videos]

    def GetFileInfoById(self, file_id):
        file = model.File.query.get(file_id)
        return file.to_dict()

    def UpdateFile(self, token, file_id, attrs):
        self._validate(token)
        file = model.File.query.get(file_id)
        file.update(attrs)
        model.Session.update(file)
        model.Session.commit()
        return 'OK'

    def CreateFile(self, token, video_id, attrs):
        try:
            self._validate(token)
            video = model.Video.query.get(video_id)
            file_class = model.FileType.choose(attrs['ftype'])
            file = file_class(video=video, **attrs)
            model.Session.save_or_update(file)
            video.modify()
            model.Session.commit()
            model.Session.refresh(file)
            model.Audit.log('Created new file %d/%s/%s (%d bytes) via API' % (
                    file.file_id, file.ftype, file.role, file.size),
                video_id=video.video_id)
            return file.file_id
        except Exception, e:
            logging.error('Got Exception: %s' % str(e))
            raise e

    def CreateDirectLink(self, token, file_id, url):
        self._validate(token)
        file = model.File.query.get(file_id)
        dl = model.DirectLink(url=url)
        file.direct_links.append(dl)
        model.Session.update(file)
        model.Session.commit()
        return 'OK'
