import logging
import re

from ngv_frontend.lib.base import *

log = logging.getLogger(__name__)


class AdminController(BaseController):

    @authorize(permission.IsAdmin())
    def index(self):
        def count_in_queue(q):
            return model.VideoQuery.query([('queue', q)]).count
        c.num_published = model.VideoQuery.query([('all', None)]).count
        c.num_incoming = count_in_queue('incoming')
        c.num_reviewed = count_in_queue('reviewed')
        c.num_errors = count_in_queue('errors')
        return render('admin/index.html')

    @authorize(permission.IsAdmin())
    def archives(self):
        c.archives = model.Archive.query.order_by(model.Archive.name)
        return render('admin/archives.html')

    @authorize(permission.IsAdmin())
    def archive_edit(self, id):
        c.archive = model.Archive.query.get(id)
        c.archive_syncmodes = model.Archive.Sync
        return render('admin/archive_edit.html')

    @authorize(permission.IsAdmin())
    @validate(model.forms.ArchiveEditForm, form='archive_edit')
    def archive_edit_commit(self, id):
        archive = model.Archive.query.get(id)
        archive.name = self.form_result['name']
        archive.homepage = self.form_result['homepage']
        archive.sync_mode = self.form_result['sync_mode']
        archive.feed_url = self.form_result['feed_url']
        icon_file = request.POST['icon']
        if icon_file:
            archive.icon = icon_file.value
        model.Session.update(archive)
        model.Session.commit()
        redirect_to(controller='/admin', action='archives')

    @authorize(permission.IsAdmin())
    def users(self, status = None):
        c.page_num = int(request.params.get('p', 0))
        c.match = request.params.get('q')
        if c.match:
            users = model.Author.search(c.match)
        else:
            users = model.Author.all_authors(status)
        c.users = h.Page(users, page=c.page_num, items_per_page=30)
        return render('admin/users.html')

    @authorize(permission.IsAdmin())
    def userdel(self):
        a = model.Author.query.get(int(request.params['uid']))
        model.Session.delete(a)
        model.Session.commit()
        redirect_to(action="users")

    @authorize(permission.IsAdmin())
    def usersetpw(self, id):
        a = model.Author.query.get(int(id))
        if not a:
            abort(404)
        c.author = a
        c.new_password = a.generate_password()
        a.password = model.Author.encrypt(c.new_password)
        model.Session.update(a)
        model.Session.commit()
        return render('admin/usersetpw.html')

    @authorize(permission.IsAdmin())
    def groups(self):
        c.groups = model.Group.query.order_by(model.Group.name)
        return render('admin/groups.html')

    @authorize(permission.IsAdmin())
    @validate(schema=model.forms.NewGroupForm(), form='groups')
    def groupnew(self):
        grp = model.Group(name=self.form_result.get('name'),
                          description=self.form_result.get('description'))
        model.Session.save(grp)
        model.Session.commit()
        redirect_to(action='groups')

    @authorize(permission.IsAdmin())
    def groupdel(self, id):
        group = model.Group.query.get(int(id))
        model.Session.delete(group)
        del group
        model.Session.commit()
        redirect_to(action='groups')

    @authorize(permission.IsAdmin())
    def groupedit(self, id):
        c.group = model.Group.query.get(int(id))
        all = model.VideoQuery.query([('all', None)])
        c.published_videos = all.query
        return render('admin/groupedit.html')

    @authorize(permission.IsAdmin())
    def groupedit_commit(self, id):
        group = model.Group.query.get(int(id))
        if request.params.has_key('order'):
            old_videos = hasattr(group, 'videos') and group.videos[:] or []
            group.videos = []
            for vid in request.params['order'].split(','):
                group.videos.append(model.Video.query.get(int(vid)))
            model.Session.update(group)
            model.Session.commit()
        redirect_to(action='groupedit')

    @authorize(permission.IsAdmin())
    def groupedit_add(self, id):
        group = model.Group.query.get(int(id))
        if request.params.has_key('video'):
            video = model.Video.query.get(int(request.params['video']))
            group.videos.append(video)
            model.Session.update(group)
            model.Session.commit()
        redirect_to(action='groupedit')
    
    @authorize(permission.IsAdmin())
    def audit(self):
        c.user = request.params.get('u')
        c.video_id = request.params.get('v')
        if c.user:
            c.logs = model.Audit.by_user(c.user)
        elif c.video_id:
            c.logs = model.Audit.by_video_id(c.video_id)
        return render('admin/audit.html')

