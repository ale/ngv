import logging

from ngv_frontend.lib.base import *

log = logging.getLogger(__name__)

class HomeController(BaseController):

    def index(self):
        def __render():
            c.latest_videos = model.Video.latest_videos(limit=3)
            homepage_group = model.Group.query.filter_by(name='homepage').one()
            c.homepage_videos = homepage_group.videos
            c.feeds = {
                'ngv': model.Feed.get('http://news.ngvision.org/atom/'),
                }
            return render('/home.html')
        if h.logged_in():
            return __render()
        else:
            home_cache = cache.get_cache('homepage')
            return home_cache.get_value('/', createfunc=__render, 
                                        type='memory', expiretime=900)

