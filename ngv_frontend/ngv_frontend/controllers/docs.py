from md5 import md5
import re
import logging

from ngv_frontend.lib.base import *

log = logging.getLogger(__name__)

class DocsController(BaseController):

    def index(self):
        redirect_to(controller='/docs', action='view', title='Home')

    def view(self, title=None):
        if not title:
            return redirect_to(controller='/docs', action='view', title='Home')
        c.doc = model.Document.query.filter_by(title=title).first()
        if not c.doc:
            if request.environ.has_key('HTTP_REFERER'):
                match = re.search(r'/view/(\w+)$', request.environ['HTTP_REFERER'])
                if match:
                    title = match.group(1)
                else:
                    title = 'Home'
                matching_doc = model.Document.query.filter_by(title=title).first()
                if matching_doc:
                    c.parent_id = matching_doc.id
            return render('docs/create.html')
        c.title = title
        c.revision_id = request.params.get('r')
        if c.revision_id:
            c.revision = model.DocumentRevision.query.get(c.revision_id)
        else:
            c.revision = c.doc.latest_revision()
        return render('docs/view.html')

    def all_pages(self):
        c.index_data = model.Document.index()
        return render('docs/all_pages.html')

    def edit(self, title):
        def save(title):
            contents = request.params.get('contents')
            parent_id = request.params.get('parent_id', '')
            username = h.logged_author().username
            doc = model.Document.query.filter_by(title=title).first()
            if doc:
                # see if the contents changed at all
                if doc.latest_revision().contents != contents:
                    doc.add_revision(contents, username)
                # re-parent logic
                if doc.parent and parent_id != doc.parent.id:
                    if parent_id:
                        doc.parent = model.Document.query.get(parent_id)
                    else:
                        doc.parent = None
                elif not doc.parent and parent_id:
                    doc.parent = model.Document.query.get(parent_id)
            else:
                doc = model.Document.create(
                    title=title, contents=contents,
                    username=username, parent_id=parent_id
                    )
            model.Session.add(doc)
            model.Session.commit()
            redirect = 'view'
            # handle attachment uploads
            if hasattr(request.params.get('attachment'), 'filename'):
                att = model.DocumentAttachment(document=doc, file=request.POST['attachment'])
                model.Session.save(att)
                redirect = 'edit'
            # eventually remove attachments
            if request.params.get('attachment_to_delete'):
                a_id = request.params['attachment_to_delete']
                att = model.DocumentAttachment.query.get(a_id)
                doc.attachments.remove(att)
                model.Session.delete(att)
                redirect = 'edit'
            model.Session.commit()
            redirect_to(action=redirect, title=title)

        if request.method == 'POST':
            if request.POST.get('mode') == 'preview':
                return h.wikify(
                                   request.POST.get('wikitext', 'error occurred')
                                   )
            elif request.POST.get('mode') == 'save':
                return save(title)
        elif request.GET.get('mode') == 'debug':
                return '<form method="post" action="#"><input type="hidden" name="mode" value="preview" /><input type="text" name="wikitext" value="cane" /></form>'

        c.doc = model.Document.query.filter_by(title=title).first()
        if c.doc:
            c.parent_id = c.doc.parent_id
        else:
            c.parent_id = request.params.get('parent_id')
            if c.parent_id:
                c.parent = model.Document.query.get(c.parent_id)
        c.title = title
        c.all_parents_for_select = \
            model.Document.all_items_select()
        c.edit_flag = True
        return render('docs/edit.html')

    def history(self, title):
        c.doc = model.Document.query.filter_by(title=title).first()
        return render('docs/history.html')        

    def attachment(self, id, name):
        # we actually ignore the 'name' argument, is there just
        # to give a default for download names to stupid browsers.
        at = model.DocumentAttachment.query.get(id)
        etag = md5(str(at.stamp)).hexdigest()
        etag_cache(key=etag)
        response.headers['Last-Modified'] = h.httpdate(at.stamp)
        response.headers['Content-Length'] = '%d' % (at.size(),)
        if at.mime_type[:6] != 'image/':
            response.headers['Content-Disposition'] = 'attachment; filename="%s"' % (
                at.name, )
        response.headers['Content-Type'] = at.mime_type
        def iter_and_close(f):
            while 1:
                buf = f.read(16384)
                if not buf:
                    f.close()
                    raise StopIteration
                yield buf
        return iter_and_close(open(at.path(), 'r'))
