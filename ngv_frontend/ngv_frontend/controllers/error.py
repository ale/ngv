import os.path

import paste.fileapp
from pylons.middleware import error_document_template, media_path

from ngv_frontend.lib.base import *

class ErrorController(BaseController):
    """Generates error documents as and when they are required.

    The ErrorDocuments middleware forwards to ErrorController when error
    related status codes are returned from the application.

    This behaviour can be altered by changing the parameters to the
    ErrorDocuments middleware in your config/middleware.py file.
    """

    def document(self):
        c.code = request.params.get('code', '')
        c.message = request.params.get('message', '')
        return render('/error.html')
