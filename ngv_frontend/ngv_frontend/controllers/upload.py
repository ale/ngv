import os
import cgi
import logging
from datetime import datetime

from ngv_frontend.lib.base import *

log = logging.getLogger(__name__)


class UploadController(BaseController):

    def index(self):
        return render('/uploadtest.html')
