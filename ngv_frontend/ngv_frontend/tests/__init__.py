"""Pylons application test package

When the test runner finds and executes tests within this directory,
this file will be loaded to setup the test environment.

It registers the root directory of the project in sys.path and
pkg_resources, in case the project hasn't been installed with
setuptools. It also initializes the application via websetup (paster
setup-app) with the project's test.ini configuration file.
"""
import os
import sys
from unittest import TestCase

import pkg_resources
import paste.fixture
import paste.script.appinstall
from paste.deploy import loadapp
from routes import url_for
from datetime import datetime

__all__ = ['url_for', 'TestController', 'model']

here_dir = os.path.dirname(os.path.abspath(__file__))
conf_dir = os.path.dirname(os.path.dirname(here_dir))

sys.path.insert(0, conf_dir)
pkg_resources.working_set.add_entry(conf_dir)
pkg_resources.require('Paste')
pkg_resources.require('PasteScript')

test_file = os.path.join(conf_dir, 'test.ini')
cmd = paste.script.appinstall.SetupCommand('setup-app')
cmd.run([test_file])

import ngv_frontend.model as model


class TestController(TestCase):

    def __init__(self, *args, **kwargs):
        wsgiapp = loadapp('config:test.ini', relative_to=conf_dir)
        self.app = paste.fixture.TestApp(wsgiapp)
        TestCase.__init__(self, *args, **kwargs)

    def setUp(self):
        admin = model.Author.query.get(1)
        video = model.Video(title=u'TEST VIDEO',
                            coverage_date=datetime.now(),
                            coverage_location=u'LOCATION',
                            coverage_country='IT',
                            status=model.Video.Status.PUBLISHED,
                            assigned_to=admin.author_id,
                            approved_by=admin.author_id,
                            archive=model.Archive.local()
                            )
        syn1 = model.Synopsis(description=u'LONGDESC',
                              short_description=u'SHORTDESC',
                              lang='en')
        video.authors.append(admin)
        video.synopsis.append(syn1)
        model.Session.add(video)
        model.Session.commit()

    def tearDown(self):
        for video in model.Video.query.all():
            model.Session.delete(video)
        model.Session.commit()
        model.Session.remove()


