import sys
from ngv_frontend.tests import *
from sqlalchemy.exceptions import IntegrityError

class TestModels(TestController):

    def test_local_archive(self):
        arc = model.Archive.local()
        self.assert_(arc)
        self.assert_(arc.is_local())

    def test_author(self):
        a = model.Author(name=u'TEST USER',
                         username='test',
                         password=model.Author.encrypt('test'),
                         status=model.Author.Status.CONFIRMED)
        model.Session.add(a)
        a_r = model.Author.query.filter_by(username='test').one()
        self.assertEqual(a, a_r)
        model.Session.delete(a_r)
        model.Session.commit()

    def test_files(self):
        video = model.Video.query.first()
        # create and retrieve a file, test they're the same
        f = model.SubtitleFile(name='test.srt', size=1024, video=video,
                               mime_type='text/plain', lang='jp',
                               role=model.FileRoles.subtitle.DEFAULT)
        dl = model.DirectLink(url='http://ngvision.org/testurl')
        f.direct_links.append(dl)
        model.Session.add(f)
        f_r = model.SubtitleFile.query.filter_by(name='test.srt').one()
        self.assertEqual(f_r.lang, 'jp')

        model.Session.delete(f)
        leftover_dl = model.DirectLink.query.all()
        self.assertEqual(leftover_dl, [])
        model.Session.commit()

    def test_video_01_queries(self):
        v = model.Video.query.first()
        queries = [
            ('all', None, True),
            ('latest', None, True),
            ('queue', 'incoming', False),
            ('location', u'LOCATION', True),
            ('location', u'WRONG', False)
            ]
        for qname, qarg, exp_result in queries:
            query = model.VideoQuery.query([(qname, qarg)])
            result = (v in query.query)
            self.assertEqual(result, exp_result)
        
    def test_video_02_duplicate_insertion(self):
        video = model.Video.query.first()
        # create and retrieve a file, test they're the same
        sess = model.Session()
        v2 = model.Video(title=video.title,
                         coverage_location=u'SOMEWHERE ELSE')
        sess.add(v2)
        self.assertRaises(IntegrityError, sess.commit)
        sess.rollback()
