from ngv_frontend.tests import *

class TestDownloadController(TestController):

    def test_index(self):
        video = model.Video.query.get(1)
        f = model.SubtitleFile(name='test.srt', size=1024, video=video,
                               mime_type='text/plain', lang='jp',
                               role=model.FileRoles.subtitle.DEFAULT)
        dl = model.DirectLink(url='http://ngvision.org/dl/test.srt')
        f.direct_links.append(dl)
        model.Session.add(f)
        model.Session.commit()
        self.file_id = f.file_id
        response = self.app.get(url_for(controller='download',
                                        id=self.file_id))
        self.assertEqual(response.status, 302)
        model.Session.delete(f)
