from ngv_frontend.tests import *

class TestDocsController(TestController):

    def test_index(self):
        response = self.app.get(url_for(controller='docs'))
        self.assertEqual(response.status, 302)

    def test_home(self):
        home = model.Document.query.filter_by(title='Home').first()
        response = self.app.get(url_for(controller='docs', action='view', title='Home'))
        self.assert_('Home' in response)
        contents = home.latest_revision().get_contents()
        self.assert_(contents in response)

    def test_all_pages(self):
        response = self.app.get(url_for(controller='docs', action='all_pages'))
        self.assert_('Home' in response)
