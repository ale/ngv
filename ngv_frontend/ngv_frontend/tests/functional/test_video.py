from ngv_frontend.tests import *
from datetime import datetime

class TestVideoController(TestController):

    def video_in_response(self, video, response):
        self.assert_(video.title in response)

    def test_index(self):
        video = model.Video.query.first()
        response = self.app.get(url_for(controller='video', id=video.video_id))
        self.video_in_response(video, response)

    def test_component(self):
        video = model.Video.query.first()
        response = self.app.get(url_for(controller='video', action='component', id='1'))
        self.video_in_response(video, response)        

    def test_search_no_results(self):
        query = 'notexisting'
        video = model.Video.query.first()
        response = self.app.get(url_for(controller='video', action='search'),
                                params={'q': query})
        self.assert_('No results found' in response)

    def test_list_and_rss(self):
        video = model.Video.query.first()
        year = datetime.now().year
        args_to_test = [
            dict(what='latest'),
            dict(what='author', arg='1'),
            dict(what='location', arg='LOCATION'),
            dict(what='date', arg='%s' % year),
            ]
        for args in args_to_test:
            for action in ['list', 'rss']:
                url = url_for(controller='/video', action=action, **args)
                response = self.app.get(url)
                self.video_in_response(video, response)

    def test_edit(self):
        video = model.Video.query.first()
        response = self.app.get(url_for(controller='/video', action='edit', 
                                        id=video.video_id))
        print response
        self.video_in_response(video, response)


        

