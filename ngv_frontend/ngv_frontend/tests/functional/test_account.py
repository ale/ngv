from ngv_frontend.tests import *
from ngv_frontend import model

class TestAccountController(TestController):

    def test_register(self):
        response = self.app.get(url_for(controller='account', action='register'))
        self.assertEqual('type="submit"' in response, True)

    def test_register_commit(self):
        parms = dict(name='TEST USER',
                     description='Example description',
                     email='test@incal.net',
                     website='http://test.incal.net')
        response = self.app.post(url_for(controller='account', action='register_2'),
                                 params=parms)
        self.assertEqual('Registration complete' in response, True)

        a = model.Author.query.filter_by(name=u'TEST USER').one()
        self.assertEqual(a.email, 'test@incal.net')
        self.assertEqual(a.website, 'http://test.incal.net')
        self.assertEqual(a.description, 'Example description')
        self.assertEqual(a.status, model.Author.Status.PENDING)

