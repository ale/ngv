from ngv_frontend.tests import *
from datetime import timedelta
import xmlrpclib
import re


class TestApiController(TestController):

    _api_endpoint = url_for(controller='api')

    def rpc_call(self, method, *args):
        data = xmlrpclib.dumps(args, method, allow_none=True)
        response = self.app.post(
            self._api_endpoint, params=data,
            headers={'Content-Type': 'text/xml',
                     'Content-Length': str(len(data))})
        self.assertEquals(200, response.status,
                          "Call to %s failed with status %d" % (
                method, response.status))
        return xmlrpclib.loads(response.body)[0][0]

    def get_token(self):
        token = self.rpc_call('GetAuthToken', 'admin', 'admin')
        self.assert_(len(token) > 0)
        return token

    def test_auth_token(self):
        token = self.get_token()
        self.assert_(re.search(r'^[a-f0-9]{32}$', token),
                     "Token does not seem to be an MD5 hash")

    def test_get_video_by_id(self):
        video = model.Video.query.first()
        response = self.rpc_call('GetVideoById', video.video_id)
        self.assertEquals(video.title, response['title'])

    def test_get_author_by_id(self):
        author = model.Author.query.first()
        response = self.rpc_call('GetAuthorById', author.author_id)
        self.assertEquals(author.name, response['name'])

    def test_get_author_by_name(self):
        author = model.Author.query.first()
        response = self.rpc_call('GetAuthorByName', author.name)
        self.assertEquals(author.name, response['name'])

    def test_get_videos(self):
        token = self.get_token()
        video = model.Video.query.first()
        response = self.rpc_call('GetVideos', token, [video.video_id])
        self.assertEquals(1, len(response))
        self.assertEquals(video.title, response[0]['title'])

    def test_get_authors(self):
        token = self.get_token()
        author = model.Author.query.first()
        response = self.rpc_call('GetAuthors', token, [author.author_id])
        self.assertEquals(1, len(response))
        self.assertEquals(author.name, response[0]['name'])

    def test_changed_videos(self):
        token = self.get_token()
        video = model.Video.query.first()
        # check that video is in response with timestamp=0
        response = self.rpc_call('GetChangedVideos', token, 0)
        self.assertEquals(1, len(response))
        self.assertEquals(video.video_id, response[0])
        # check that video is not in response with timestamp>video.stamp
        ts = video.modified_at + timedelta(0, 3600)
        response = self.rpc_call('GetChangedVideos', token, int(ts.strftime('%s')))
        self.assert_(video.video_id not in response)

    def test_create_file(self):
        token = self.get_token()
        video = model.Video.query.first()
        file_info = dict(ftype='subtitle', lang='jp', name='subtitle.srt',
                         mime_type='text/plain', size=1234)
        response = self.rpc_call('CreateFile', token, video.video_id, file_info)
        self.assert_(isinstance(response, int))
        self.assert_(response > 0)
        file_in_db = model.File.query.get(response)
        self.assert_(file_in_db)
        self.assertEquals(response, file_in_db.file_id)
        self.assertEquals(file_info['mime_type'], file_in_db.mime_type)
        model.Session.delete(file_in_db)

    def test_update_file(self):
        token = self.get_token()
        video = model.Video.query.first()
        file = model.SubtitleFile(video=video, lang='jp', name='subtitle.srt',
                                  mime_type='text/plain', size=1234)
        model.Session.add(file)
        model.Session.commit()
        file_id = file.file_id
        file_info = dict(mime_type='application/x-srt', size=2345)
        response = self.rpc_call('UpdateFile', token, file_id, file_info)
        self.assertEquals('OK', response)
        file_in_db = model.File.query.get(file_id)
        self.assertEquals(file_info['mime_type'], file_in_db.mime_type)
        self.assertEquals(file_info['size'], file_in_db.size)
        model.Session.delete(file_in_db)

