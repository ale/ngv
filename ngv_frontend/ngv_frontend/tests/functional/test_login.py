from ngv_frontend.tests import *

class TestLoginController(TestController):

    def test_index(self):
        response = self.app.get(url_for(controller='login', action='login'))
        self.assert_('username' in response)
        self.assert_('password' in response)

    def test_login(self):
        response = self.app.post(url_for(controller='login', action='login'),
                                 params={'username': 'admin',
                                         'password': 'admin'})
        self.assertEquals(1, response.session['logged_in'])

    def test_invalid_login(self):
        response = self.app.post(url_for(controller='login', action='login'),
                                 params={'username': 'admin',
                                         'password': 'wrongpass'})
        self.assert_('not correct' in response)
