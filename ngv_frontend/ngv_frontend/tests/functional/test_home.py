from ngv_frontend.tests import *

class TestHomeController(TestController):

    def test_index(self):
        response = self.app.get(url_for(controller='home'))
        self.assert_('New Global Vision' in response)
        self.assert_('TEST VIDEO' in response)
        self.assert_('LOCATION' in response)
        self.assert_('SHORTDESC' in response)

