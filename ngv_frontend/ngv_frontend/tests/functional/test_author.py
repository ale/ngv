from ngv_frontend.tests import *

class TestAuthorController(TestController):

    def test_index(self):
        response = self.app.get(url_for(controller='author', id='1'))
        self.assert_('Administrator' in response)

