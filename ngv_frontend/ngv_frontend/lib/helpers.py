"""Helper functions

Consists of functions to typically be used within templates, but also
available to Controllers. This module is available to both as 'h'.
"""
import re
from webhelpers import *
from webhelpers.paginate import Page
from datetime import datetime
from pylons import cache, request, response, session
from pylons import g

# workaround
from webhelpers.rails.wrapped import *
from routes import url_for, redirect_to

from wikimarkup import parse

def wikify(text):
    return parse(text)

def logged_in():
    if session.has_key('logged_in'):
        return session.get('logged_in')
    return False


def in_group(g):
    if logged_in():
        return g in session['groups']
    return False


def is_admin():
    return in_group('admins')


def logged_author():
    from ngv_frontend import model
    if 'paste.testing' in request.environ:
        return model.Author.query.first()
    return model.Author.query.get(session['user_id'])


def datefmt(d):
    """Format a datetime object according to our standard Y/M/D."""
    if d is None:
        return ''
    return '%04d/%02d/%02d' % (d.year, d.month, d.day)


def urldatefmt(d, base_uri=''):
    """Format a datetime so that every part is a link."""
    parts = [ ('%04d' % d.year, '%04d' % d.year),
              ('%02d' % d.month, '%04d-%02d' % (d.year, d.month)),
              ('%02d' % d.day, '%04d-%02d-%02d' % (d.year, d.month, d.day)) ]
    links = [ link_to(s, url=base_uri + '/%s' % p)
              for (s, p) in parts ]
    return '/'.join(links)


def formdatefmt(d):
    """Format a date in order to put it into a text field."""
    if d is None:
        return ''
    return '%02d/%02d/%04d' % (d.day, d.month, d.year)


def httpdate(d=None):
    """Format a datetime according to the HTTP standard."""
    if d is None:
        d = datetime.now()
    return d.strftime("%a, %d %b %Y %H:%M:%S GMT")


def sizefmt(size):
    """Format 'size' nicely with units."""
    if size > (1<<30):
        size = float(size) / (1<<30)
        units = 'Gb'
    elif size > (1<<20):
        size = float(size) / (1<<20)
        units = 'Mb'
    elif size > (1<<10):
        size = float(size) / (1<<10)
        units = 'Kb'
    else:
        size = float(size)
        units = 'bytes'
    return '%.3g %s' % (size, units)

# initialize the FCK editor
def fckeditor_init(textArea):
    '''adapted from vanilla distro'''
    return '''<script type="text/javascript" src="%s"></script>
<script type="text/javascript">
window.onload = function()
{
var oFCKeditor = new FCKeditor( '%s' );
oFCKeditor.Config["CustomConfigurationsPath"] = "%s";
oFCKeditor.BasePath = "%s";
oFCKeditor.ReplaceTextarea();
}
</script>

''' % (
       url_for('/javascripts/fckeditor/fckeditor.js'),
       textArea,
       url_for('/javascripts/ngv/fck-ngv.js'),
       url_for('/javascripts/fckeditor/'))

# initialize the tinyMCE editor
def tinymce_init(theme='simple'):
    return ('''
 <script type="text/javascript" src="%s"></script>
 <script type="text/javascript">
tinyMCE.init({
    mode: "textareas",
    theme: "%s",
    editor_selector: "use_mce",
    strict_loading_mode: true,
    apply_source_formatting: true,
    entity_encoding: "raw",
    inline_styles: true,
    button_tile_map: true
});
 </script>
''' % (url_for('/javascripts/tiny_mce-3.0.8/tiny_mce.js'), theme))


cc_licenses = {
    'by': 'Attribution',
    'by-sa': 'Attribution-Share Alike',
    'by-nd': 'Attribution-No Derivatives',
    'by-nc': 'Attribution-Noncommercial',
    'by-nc-sa': 'Attribution-Noncommercial-Share Alike',
    'by-nc-nd': 'Attribution-Noncommercial-No Derivatives',
    'pd': 'Public Domain',
}


def cc_image(arg):
    return url_for('/images/licenses/cc_%s.png' % arg.sub('-','_'))


def cc_name(arg):
    return cc_licenses[arg]


def language_name(l):
    for k, v in g.languages.iteritems():
        if k == l:
            return v
    return 'unknown'


def sanitize_html(html):
    """Remove all tags from HTML except for a few ones."""
    allowed_tags = [ 'a', 'em', 'b', 'i', 'strong' ]
    tag_pattern = re.compile(r'</?(\w+)[^>]*/?>')
    def _match_tag(m):
        if m.group(0).lower() == '</p>':
            return '<br/>'
        if m.group(1).lower() in allowed_tags:
            return m.group(0)
        return ''
    out = tag_pattern.sub(_match_tag, html)
    # trim consecutive <br/>s
    out = re.sub(r'(<br/?>\s*){2,}', '<br/>', out)
    # trim final <br/> if any
    out = re.sub(r'<br/?>\s*$', '', out)
    return out


def format_video_file(f):
    icon_type = 'video'
    url = url_for(controller='/download', action='index', id=f.file_id)
    other_info = ''
    if f.ftype == 'video':
        title = u'%s / %s' % ('Download', f.role)
        if f.role == 'flash':
            url = url_for(controller='/stream', id=f.file_id)
            title = 'Play this video'
        else:
            other_info = u'%s - <strong>%s</strong> - audio: %s' % (
                str(f.duration), sizefmt(f.size), f.audio_lang)
        if f.subtitles_lang:
            other_info += ' - sub: %s' % f.subtitles_lang
        if f.frame_width and f.frame_height:
            other_info += ' - %sx%s' % (str(f.frame_width), str(f.frame_height))
        if f.video_codec:
            other_info += ' - %s/%s' % (f.video_codec, f.audio_codec)
        if f.fps:
            other_info += ' %sfps' % (f.fps,)
    elif f.ftype == 'subtitle':
        title = 'Subtitles - %s' % f.lang
        other_info = sizefmt(f.size)
    elif f.ftype == 'p2p':
        title = 'Download - %s' % f.role
        other_info = sizefmt(f.size)
    else:
        title = '%s / %s' % (f.ftype, f.role)
        other_info = sizefmt(f.size)
    return (icon_type, url, title, other_info)


def nl2br(s):
    return s.replace('\n', '<br/>')


_file_types_map = {
    'video': 'video',
    'subtitle': 'type',
    'p2p': 'loop'
    }
def file_type_icon(ftype):
    return _file_types_map.get(ftype, 'document')


def scaled_color(base_color, factor):
    cur_color = map(int,
                    map(lambda x: (255 - factor * (255 - x)),
                        base_color))
    return "#%02x%02x%02x" % tuple(cur_color)
