# $Id: $

from authkit.permissions import Permission, \
    NotAuthenticatedError, NotAuthorizedError
from pylons import session
from ngv_frontend.lib import helpers as h

class LoggedIn(Permission):
    def check(self, app, environ, start_response):
        if not ('paste.testing' in environ or h.logged_in()):
            raise NotAuthenticatedError('Not logged in.')
        return app(environ, start_response)

class IsAdmin(Permission):
    def check(self, app, environ, start_response):
        if not h.logged_in():
            raise NotAuthenticatedError('Not logged in.')
        if not h.logged_author().is_admin():
            raise NotAuthorizedError('Not authorized.')
        return app(environ, start_response)

