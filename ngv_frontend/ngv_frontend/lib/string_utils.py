import re

_latin1_map = [
    (u'\xc0\xc1\xc2\xc3\xc4\xc5\xe0\xe1\xe2\xe3\xe4\xe5', 'a'),
    (u'\xc8\xc9\xcA\xcB\xe8\xe9\xeA\xeB', 'e'),
    (u'\xcc\xcd\xce\xcf\xec\xed\xee\xef', 'i'),
    (u'\xd2\xd3\xd4\xd5\xd8\xf2\xf3\xf4\xf5\xf8', 'o'),
    (u'\xd6\xf6', 'oe'),
    (u'\xd9\xda\xdb\xf9\xfa\xfb', 'u'),
    (u'\xdc\xfc', 'ue'),
    (u'\xd1\xf1', 'n'),
    (u'\xc7\xe7', 'c'),
    (u'\xc6\xe6', 'ae'),
    ]


def sanitize(s, pad_char='_'):
    """Transform a string to ASCII, replacing all non-alphanumeric
    characters with '_'.  This function will also attempt to strip
    accents from letters in some latin languages."""
    s = s.lower()
    for letters, subst in _latin1_map:
        s = re.sub(r'[%s]' % letters, subst, s)
    s = re.sub(r'[^a-zA-Z0-9.]+', pad_char, s)
    s = s.strip(pad_char)
    return s

