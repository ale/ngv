"""The application's Globals object"""
from pylons import config
from ngv.storage.local_storage import LocalStorage
from ngv.jobserver.api import JobServer


class Globals(object):
    """Globals acts as a container for objects available throughout the
    life of the application
    """

    def __init__(self):
        """One instance of Globals is created during application
        initialization and is available during requests via the 'g'
        variable
        """
        self.languages = dict([(k, v) for k, v in 
                               [x.strip().split('/') for x in 
                                config['languages'].split(',')]])

        self.document_upload_path = config['document_upload_path']
        self.async_upload_path = config.get('async_upload_path', '/tmp')

        self.local_storage = LocalStorage(config['local_storage_path'])

        self.jobserver = JobServer(server=config['jobserver_url'])
        self.uploadserver_host = config['uploadserver_host']
