"""Setup the ngv_frontend application"""
import logging

from paste.deploy import appconfig
from pylons import config

from ngv_frontend.config.environment import load_environment

log = logging.getLogger(__name__)

def setup_config(command, filename, section, vars):
    """Place any commands to setup ngv_frontend here"""
    conf = appconfig('config:' + filename)
    load_environment(conf.global_conf, conf.local_conf)

    # model setup
    from ngv_frontend import model
    sa_engine = config['pylons.g'].sa_engine
    print "Dropping all tables..."
    model.metadata.drop_all(bind=sa_engine)
    print "Creating tables..."
    model.metadata.create_all(bind=sa_engine)

    # create some fundamental objects
    # first, the NGV archive itself
    print "Pre-populating database..."
    print "+ local archive"
    archive = model.Archive(name=u'New Global Vision',
                            homepage='http://ngvision.org/')
    model.Session.add(archive)

    # next the main users groups
    print "+ groups"
    authors_groups = [
        ( 'admins', u'Site Administrators' ),
        ( 'reviewers', u'Reviewers' ),
        ( 'users', u'Normal users' ),
        ]
    admin_groups = []
    for name, desc in authors_groups:
        ag = model.AuthorGroup(name=name, description=desc)
        model.Session.add(ag)
        admin_groups.append(ag)

    # admin user
    print "+ admin user"
    admin = model.Author(
        archive=archive,
        name=u'NGV Administrator', 
        username='admin',
        password=model.Author.encrypt('admin'),
        status=model.Author.Status.ADMIN,
        )
    admin.groups = admin_groups
    model.Session.add(admin)

    # homepage group
    print "+ homepage group"
    hg = model.Group(
        name='homepage',
        description=u'Highlights'
        )
    model.Session.add(hg)

    # predefined categories
    print "+ categories"
    categories = [ u'mediascape',
                   u'war and global crisis',
                   u'global demos',
                   u'corpwatch',
                   u'biowar',
                   u'hacking, digital rights, net-art',
                   u'surveillance and control',
                   u'income and work',
                   u'migrants, citizenship, noborder',
                   u'antiproibizionismo',
                   u'jail and repression',
                   u'antifascism',
                   u'antimafia',
                   u'identity, gender, sexuality',
                   u'knowledge, documents, formation',
                   u'religion',
                   u'direct action',
                   u'history and memory',
                   u'ecology, health',
                   u'music, theatre, dance, art',
                   u'squatting, csa',
                   u'global demos italy',
                   u'fiction, short movies',
                   u'genoa g8'
                   ]
    for cname in categories:
        c = model.Category(name=cname, archive=archive)
        model.Session.add(c)

    # root document
    print "+ wiki root page"
    root_doc = model.Document.create(
        title='Home',
        contents=u'<p>Wiki homepage. Change with your own docs.</p>',
        username='admin'
        )

    model.Session.commit()

    print "Setup done."
