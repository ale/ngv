
// Write debug messages
debug = function(msg) {
  $('#out').append(msg + '<br/>');
};

error = function(msg) {
  $('#out').append('<span style="color:red;">' + msg + '</span><br/>');
};


// Pretty-print a file size
prettySize = function(sz) {
  var suffix = 'bytes';
  if (sz > 1024 * 1024) { 
    sz = sz / (1024 * 1024);
    suffix = 'Mb';
  } else if (sz > 1024) {
    sz = sz / 1024;
    suffix = 'Kb';
  }
  return Math.round(sz) + ' ' + suffix;
};

// Show progress made so far
showProgress = function(cur, tot) {
  var pct = 0;
  if (tot > 0) {
    pct = Math.round(cur * 100 / tot);
  }
  $('#progressTxt').html(prettySize(cur) + ' of ' + prettySize(tot) + ' (<strong>' + pct + '%</strong>)');
  $('#progressBar').css('width', pct + '%');
};

// Handle the 'ping' result
pingHandler = function(stat, cur, tot) {
  if (stat == 'E') {
    error('Upload failed!');
    uploadFailed();
    return false;
  }
  if (stat == 'C') {
    if (cur < (tot - 4096)) {
      error('Upload aborted by user');
      uploadFailed();
      return false;
    }
    debug('Upload complete!');
    showProgress(tot, tot);
    $('#newFileForm').submit();
    return false;
  }
  showProgress(cur, tot);
  return true;
};

// Periodical polling of the uploadserver
getUploadPoller = function(token) {
  var pingUrl = uploadserverUrl + '/ping/' + token;
  var uploadPoller = function(timer) {
    $.ajax({
      type: "GET",
      url: pingUrl,
      dataType: "json",
      success: function(values) {
        if (!pingHandler(values[0], values[1], values[2])) {
          timer.stop();
        }
      }});
  };
  return uploadPoller;
};

// Obtain an upload token, and initiate the upload process
getToken = function() {
  var url = uploadserverUrl + '/gen';
  debug('requesting upload token from ' + url);
  $.ajax({
    type: "GET",
    dataType: "json",
    url: url,
    success: function(token) {
      debug('received token: ' + token);
      // Save the token in the final form
      $('#newFileFormToken').attr("value", token);
      // Now, submit the upload form
      action = $('#uploadForm').attr('action');
      action += '/' + token;
      $('#uploadForm').attr('action', action);
      debug('submitting form to ' + action);
      $('#uploadForm').submit();
      $.timer(1000, getUploadPoller(token));
    },
    error: function(){
      error('Could not communicate with the upload server (no upload token)');
      uploadFailed();
    }
  });
};

// Start the upload process, user callback
startUpload = function() {
  debug('starting asynchronous upload');
  $('#startUploadBtn').attr("disabled", "disabled");
  $('#progress').show();
  getToken();
};

// Callback for failed uploads
uploadFailed = function() {
  $('#progress').hide();
  $('#startUploadBtn').attr("disabled", "enabled");
};


