
/*
 * Miscellaneous functions
 */

function flashmsg(msg) {
    $('#flashdiv').append('<p style="color:green;">' + msg + '</p>');
}

function flasherr(msg) {
    $('#flashdiv').append('<p class="error_message">' + msg + '</p>');
}

/*
 * Files management
 */

function updateTypeGroup() {
  var curType = $('#ftype').find(':selected')[0].value;
  var divName = '#file_' + curType + '_div';
  $('.type_group').hide();
  $(divName).show();
}

function identifyFile(fileid) {
  $.ajax({
    type: "POST",
    url: file_identify_url,
    data: "f=" + fileid,
    success: function(msg) {
      flashmsg("Requested identification of file " + fileid);
    },
    error: function(req, status, exc) {
      flasherr("Error: " + status);
    }
  });
}

function reprocessFile(fileid) {
  $.ajax({
    type: "POST",
    url: file_reprocess_url,
    data: "f=" + fileid,
    success: function(msg) {
      flashmsg("Requested reprocessing of file " + fileid);
    },
    error: function(req, status, exc) {
      flasherr("Error: " + status);
    }
  });
}

function deleteFile(fileid) {
  $.ajax({
    type: "POST",
    data: "f=" + fileid,
    url: file_delete_url,
    success: function(msg) {
      flashmsg('File ' + fileid + ' deleted.');
      $('#filediv_' + fileid).hide();
    },
    error: function(req, status, exc) {
      flasherr('Could not delete file: ' + status);
    }
  });
}



/*
 * Authors management
 */

function updateAuthors() {
  var all_authors = $('#all_authors');
  var opt = all_authors.options[all_authors.selectedIndex];
  var authorid = opt.value;
  var authorname = opt.text;
  authors_list.push(authorid);
  $('#authors_box').innerHTML += '<option value="' + authorid + '">' + authorname + '</option>';
  $('#authors').value = authors_list.join(",");
}

function removeAuthors() {
  var newhtml = '';
  var ab = $('#authors_box');
  var to_remove = ab.options[ab.selectedIndex].value;
  var i;
  authors_list = Array();
  for (i = 0; i < ab.options.length; i++) {
	var opt = ab.options[i];
	if (opt.value != to_remove) {
	  newhtml += '<option value="' + opt.value + '">' + opt.text + '</option>';
	  authors_list.push(opt.value);
	}
  };
  $('#authors').value = authors_list.join(",");
  ab.html(newhtml);
}
