"""Routes configuration

The more specific and detailed routes should be defined first so they
may take precedent over the more generic routes. For more information
refer to the routes manual at http://routes.groovie.org/docs/
"""
from pylons import config
from routes import Mapper

def make_map():
    """Create, configure and return the routes Mapper"""
    map = Mapper(directory=config['pylons.paths']['controllers'],
                 always_scan=config['debug'])

    # The ErrorController route (handles 404/500 error pages); it should
    # likely stay at the top, ensuring it can always be resolved
    map.connect('error/:action/:id', controller='error')

    # CUSTOM ROUTES HERE
    map.connect('tag_cloud', controller='tag_cloud', action='index')
    map.connect('channels', controller='category', action='index')

    map.connect('account/login', controller='login', action='login')
    map.connect('account/logout', controller='login', action='logout')
    map.connect('account/resetpw/:id/:token', controller='account', action='resetpw')

    map.connect('docs/attachment/:id/:name', controller='docs', action='attachment')
    map.connect('docs/:action/:title', controller='docs')

    map.connect('author', 'author/:id', controller='author', action='index',
                requirements={'id':'\d+'})

    for what in [ 'all', 'latest', 'author', 'category',
                  'location', 'country', 'tag', 'date', 'queue' ]:
        map.connect('video/%s/:arg' % what,
                    controller='video', action='list', what=what, arg=None)
        map.connect('rss/video/%s/:arg' % what,
                    controller='video', action='rss', what=what, arg=None)

    map.connect('video/edit', controller='video', action='edit')
    map.connect('video/upload', controller='video', action='upload')
    map.connect('video/:id', controller='video', action='index',
                requirements={'id':'[-a-zA-Z0-9]+'})
    #map.connect('video/:action', controller='video', action='index')

    map.connect('download/:id', controller='download', action='index')
    map.connect('search', controller='video', action='search')
    map.connect('api', controller='api')
    
    map.connect(':controller/:action/:id')
    map.connect('home', '', controller='home', action='index')
    map.connect('*url', controller='template', action='view')

    return map
