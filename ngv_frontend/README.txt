This file is for you to describe the ngv_frontend application. Typically
you would include information such as the information below:

Installation and Setup
======================

Install ``ngv_frontend`` using easy_install::

    easy_install ngv_frontend

Make a config file as follows::

    paster make-config ngv_frontend config.ini

Tweak the config file as appropriate and then setup the application::

    paster setup-app config.ini

Then you are ready to go.
