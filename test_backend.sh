#!/bin/sh
base_dir=`dirname $0`
if [ -z "$base_dir" ]; then
  base_dir=`pwd`
fi
export PYTHONPATH=${base_dir}:${base_dir}/ngv_frontend
echo "PYTHONPATH: '$PYTHONPATH'"
python setup.py build
cd ngv && nosetests --verbose --with-coverage --cover-package=ngv
