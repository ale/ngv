#!/usr/bin/python
#
# JobServer main application
# Copyright 2008, ale@incal.net
#
# $Id$

from ngv import flags
from ngv import config
from ngv import application
from ngv import web
from ngv.jsonrpc.middleware import jsonify

from datetime import datetime, timedelta
from sqlalchemy import desc
import model
import logging
import os
import simplejson
import sys
import threading
import time


flags.define_string('bind_addr', None, '--bind',
                    help='Specify the bind address (default 0.0.0.0)',
                    section='jobserver',
                    default='0.0.0.0')
flags.define_string('port', '-p', '--port',
                    help='TCP port to bind to (default 9011)',
                    section='jobserver',
                    default='9011')
flags.define_string('server_root', '-R', '--root',
                    help='Root for the static contents of the web interface',
                    section='jobserver',
                    default=os.path.dirname(__file__))


class JSGet:
    @jsonify
    def GET(self, node_name):
        node = model.Node.query.filter_by(name=node_name).first()
        if not node:
            return web.notfound()
        return model.Job.next_for(node)


class JSAdd:
    """Add a new job. Data is JSON-encoded in the request."""
    @jsonify
    def POST(self):
        inp = web.input(group='default')
        data = simplejson.loads(inp.data)
        job = model.Job.add(data=data, group=inp.group)
        return job.id


class JSCommit:
    """Notify about completion of a successful job."""
    @jsonify
    def GET(self, job_id):
        job = model.Job.query.get(job_id)
        if not job:
            return web.notfound()
        job.complete()
        return 'OK'


class JSError:
    """Notify about an error occurred while processing a job."""
    @jsonify
    def POST(self, job_id):
        job = model.Job.query.get(job_id)
        if not job:
            return web.notfound()
        inp = web.input()
        job.error(inp.error)
        return 'OK'


class JSLog:
    """Add a log message to the job's worklog."""
    @jsonify
    def POST(self, job_id):
        job = model.Job.query.get(job_id)
        if not job:
            return web.notfound()
        inp = web.input(message='')
        job.log(inp.message)
        return 'OK'


class JSQuery:
    """Query about the current status of a job."""
    @jsonify
    def GET(self, job_id):
        job = model.Job.query.get(job_id)
        if not job:
            return web.notfound()
        result = {'status': job.status,
                  'created_at': job.created_at.isoformat(),
                  'worklog': job.worklog()}
        if job.status == model.Job.STATUS_ASSIGNED:
            result['assigned_to'] = {'name': job.assigned_to.name,
                                     'group': job.assigned_to.group_name}
            result['assigned_at'] = job.assigned_at.isoformat()
        elif job.status in [model.Job.STATUS_COMPLETE, model.Job.STATUS_ERROR]:
            result['completed_at'] = job.completed_at.isoformat()
            if job.status == model.Job.STATUS_ERROR:
                result['errors'] = job.errors
        return result


class JSRegisterNode:
    @jsonify
    def POST(self):
        inp = web.input(group='default')
	data = {}
	if inp.data:
	    data = simplejson.loads(inp.data)
        model.Node.register(inp.name, inp.group, data)
        return 'OK'


class JSPing:
    @jsonify
    def GET(self, node_name):
        node = model.Node.query.filter_by(name=node_name).one()
        node.ping()
        return 'OK'


class JSStatusRedirector:
    def GET(self):
        return web.redirect('/status')


class app_quit:
    def GET(self):
        logging.warning('termination requested via http')
        os._exit(0)


class HTMLPage:
    """A basis for our HTML pages."""
    def start_page(self, title):
        web.output("""
<html>
<head>
 <title>%s</title>
 <link rel="stylesheet" type="text/css" href="/static/style.css"/>
</head>
<body>
 <br/>
 <h1>%s</h1>
""" % (title, title))
    def end_page(self):
        web.output("<br/><br/><br/><br/>\n</body></html>")
    def fmt_date(self, d):
        return d.strftime("%Y-%m-%d %H:%M:%S")
    def link_to(self, label, url):
        return '<a href="%s">%s</a>' % (
            web.urlquote(url), web.htmlquote(label))
    def print_status(self, status):
        status_labels = {
            model.Job.STATUS_NEW: ('PENDING', None),
            model.Job.STATUS_ASSIGNED: ('ASSIGNED', None),
            model.Job.STATUS_COMPLETE: ('COMPLETE', None),
            model.Job.STATUS_ERROR: ('ERROR', '#600')
            }
	sl, sc = status_labels[status]
	if sc:
            return '<span style="color:%s;">%s</span>' % (sc, sl)
	else:
	    return sl
    def print_payload(self, payload, short=True):
	if isinstance(payload, basestring):
	    return payload
        if short:
            s = ["%s=%s" % (k, str(v)) for k, v in payload.iteritems()]
            s = ", ".join(s)
            if len(s) > 512:
                s = s[:512] + '...'
            return s
        else:
            s = ["<b>%s</b> = %s" % (k, web.htmlquote(str(payload[k])))
                 for k in sorted(payload.keys())]
            return "<br/>".join(s)
    def tablerow(self, label, value):
        web.output('<tr><th>%s:</th><td>%s</td></tr>' % (label, value))
    def list_jobs(self, jobs):
	out = []
        for job in jobs:
	    if job.status == model.Job.STATUS_ASSIGNED:
		out.append('<div class="job selected">')
	    else:
                out.append('<div class="job">')
            out.append('<div class="jobid">%s</div>' %
                       self.link_to(job.id, '/status/job/%s' % job.id))
            out.append('<div class="jobdescr"><span class="group">%s</span>' %
                       job.group_name)
	    out.append(' -- %s' % self.print_status(job.status))
            if job.status == model.Job.STATUS_ASSIGNED and job.assigned_to:
		elapsed = datetime.now() - job.assigned_at
                out.append(' -- %s (since %s)' % (self.link_to(
                    job.assigned_to.name,
                    '/status/node/%s' % job.assigned_to.name),
		    str(elapsed)[:-7]))
            if job.status == model.Job.STATUS_NEW:
                out.append('&nbsp;&nbsp;&nbsp;&nbsp;<small><a style="color:red;" href="/status/delete_job/%s">del</a></small>' % job.id)
	    if job.status == model.Job.STATUS_ERROR:
	        shorterr = job.short_error()
		if shorterr:
	            out.append('<br/><span class="error">%s</span>' % (
		        web.htmlquote(shorterr)))
            out.append('<br/><span class="payload">%s</span>' % (
                web.htmlquote(self.print_payload(job.payload))))
            dates = [job.created_at]
            if job.status in [model.Job.STATUS_ERROR, model.Job.STATUS_COMPLETE]:
                dates.append(job.completed_at)
            if job.status == model.Job.STATUS_ASSIGNED:
                dates.append(job.assigned_at)
            date_str = '&nbsp; - &nbsp;'.join(map(self.fmt_date, dates))
            out.append('<br/><span class="date">%s</span>' % date_str)
            out.append('</div></div>\n')
	return ''.join(out)
    def list_nodes(self, nodes):
        out = ['<table class="nodes">']
	now = datetime.now()
	for node in nodes:
	    bgcol = status = ""
	    tdiff = now - node.stamp
	    # show nodes that are about to be disabled for inactivity
	    if tdiff > timedelta(0, 900):
	        bgcol = ' style="background-color:#eee;"'
		status = "<b>(unresponsive)</b>"
            out.append(("<tr%s><td>%s</td><td>%s</td><td>%s</td>" +
	                "<td><small>%s</small></td><td>%s</td></tr>") % (
		bgcol, self.link_to(node.name, '/status/node/%s' % node.name),
                node.group_name, node.data.get('ngv_version'),
		self.fmt_date(node.stamp), status))
	out.append('</table>')
        return ''.join(out)


class JSStatusSummary(HTMLPage):
    """Jobserver dashboard."""
    def GET(self):
        self.start_page("jobserver status")
        web.output("""
	<script>
	function jumpToJob(form) {
	  document.location.pathname = '/status/job/' + form.job.value;
	}
	</script>
	<form id="jump" onsubmit="jumpToJob(this); return false;">
	<input type="text" name="job" size="12" />
	<input type="submit" value="find job" />
	</form>
	""")
        # table of (title, status, sort_field, results_count)
        joblists = [
            ("Currently running jobs", model.Job.STATUS_ASSIGNED,
             model.Job.assigned_at, None),
            ("Pending jobs", model.Job.STATUS_NEW,
             model.Job.created_at, None),
            ("Last completed jobs", model.Job.STATUS_COMPLETE,
             model.Job.completed_at, 5),
            ("Last errors <a href=\"/status/errors\">(all)</a>",
             model.Job.STATUS_ERROR, model.Job.completed_at, 5)
            ]
        for label, status, sort_field, limit in joblists:
            query = model.Job.query.filter_by(status=status)\
	        .order_by(desc(sort_field))
	    if limit:
	        query = query.limit(limit)
	    if query.count() == 0:
	        continue
            web.output("<h4>%s</h4>" % label)
            web.output(self.list_jobs(query))

        web.output("<h4>Active nodes</h4>")
	web.output('<table class="nodes">')
	active_nodes = model.Node.query.filter_by(enabled=True).order_by(
	    desc(model.Node.stamp))
	web.output(self.list_nodes(active_nodes))
        self.end_page()


class JSErrorSummary(HTMLPage):
    """List all errors."""
    def GET(self):
        self.start_page("jobserver errors")
        error_jobs = model.Job.query.filter_by(status=model.Job.STATUS_ERROR)\
                     .order_by(desc(model.Job.completed_at))
        web.output("<h4>All errors</h4>")
        web.output(self.list_jobs(error_jobs))
        self.end_page()


class JSJobSummary(HTMLPage):
    """Summary page for a job. Shows full logs."""
    def GET(self, jobid):
        job = model.Job.query.get(jobid)
        if not job:
            return web.notfound()
        self.start_page("job info: %s" % jobid)
        web.output('<br/><table>')
        self.tablerow('status', self.print_status(job.status))
        if job.assigned_to:
            self.tablerow('assigned to', self.link_to(
                job.assigned_to.name,
                '/status/node/%s' % job.assigned_to.name))
            self.tablerow('assigned at', str(job.assigned_at))
        self.tablerow('group', '<span class="group">%s</span>' % job.group_name)
        self.tablerow('created at', str(job.created_at))
        if job.status == model.Job.STATUS_COMPLETE:
            self.tablerow('completed at', str(job.completed_at))
        web.output('</table>')
        web.output('<hr/><br/><h4>Job payload</h4>')
        web.output('<p><tt>%s</tt></p>' % self.print_payload(job.payload, short=0))
        if job.errors:
            web.output('<h4 class="error">Error log</h4>')
            web.output('<pre>%s</pre>' % web.htmlquote(job.errors))
        worklog = job.worklog()
        if worklog:
            web.output('<h4>Log</h4>')
            web.output('<pre>%s</pre>' % web.htmlquote(worklog))
        web.output('<br/><p><a href="/status">Return to jobserver status page</a></p>')
        self.end_page()


class JSNodeSummary(HTMLPage):
    """Summary page for a specific node."""
    def GET(self, nodename):
        node = model.Node.query.filter_by(name=nodename).first()
        if not node or not node.enabled:
            return web.notfound()
        self.start_page("node info: %s" % nodename)
        web.output('<br/><table>')
        self.tablerow('group', '<span class="group">%s</span>' % node.group_name)
        self.tablerow('last seen', str(node.stamp))
	if node.data:
	    self.tablerow('data', self.print_payload(node.data, short=0))
        web.output('</table>')
        web.output('<h4>Latest jobs</h4>')
        query = model.Job.query.filter_by(assigned_to=node).\
                order_by(desc(model.Job.assigned_at)).limit(5)
        web.output(self.list_jobs(query))
        web.output('<br/><p><a href="/status">Return to jobserver status page</a></p>')
        self.end_page()


class JSDeleteJob(HTMLPage):
    """Delete a job. Ask for confirmation."""
    def GET(self, jobid):
        job = model.Job.query.get(jobid)
        if not job:
            return web.notfound()
        model.Session.delete(job)
        model.Session.commit()
        return web.redirect('/status')


urls = (
    '/add_job', 'JSAdd',
    '/get/(.*)', 'JSGet',
    '/pop/(.*)', 'JSPop',
    '/commit/(.*)', 'JSCommit',
    '/error/(.*)', 'JSError',
    '/log/(.*)', 'JSLog',
    '/query/(.*)', 'JSQuery',
    '/register', 'JSRegisterNode',
    '/ping/(.*)', 'JSPing',
    '/status', 'JSStatusSummary',
    '/status/errors', 'JSErrorSummary',
    '/status/job/(.*)', 'JSJobSummary',
    '/status/delete_job/(.*)', 'JSDeleteJob',
    '/status/node/(.*)', 'JSNodeSummary',
    '/quitquitquit', 'app_quit',
    '/', 'JSStatusRedirector'
    )


class node_expiration_thread(threading.Thread):
    def run(self):
        while 1:
            time.sleep(60)
            model.Node.disable_expired()
            model.Job.detect_dropped_jobs()


class sqlalchemy_wrapper:
    def __init__(self, application):
        self.application = application
    def __call__(self, environ, start_response):
        try:
            res = self.application(environ, start_response)
        finally:
            model.Session.remove()
        return res


def start_webserver(addr_port):
    application.run_webserver(addr_port, urls, globals(), 
                              sqlalchemy_wrapper)


def main(args):
    logging.debug('using server root %s' % config.jobserver.server_root)
    os.chdir(config.jobserver.server_root)
    addr = config.jobserver.bind_addr
    port = int(config.jobserver.port)
    n = node_expiration_thread()
    n.setDaemon(1)
    n.start()
    logging.info('starting jobserver on %s:%d' % (addr, port))
    try:
        start_webserver((addr, port))
    except KeyboardInterrupt:
        sys.exit(0)


def entry_point():
    application.run(main, daemon=True)


if __name__ == '__main__':
    entry_point()

