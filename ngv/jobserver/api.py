# Copyright 2008, ale@incal.net
# $Id$

"""JobServer client API.

This module offers an interface for applications to interact with 
the JobServer daemon.

The JobServer model follows a scheme where jobs are created by
'producers' so that a number of 'workers' can perform predetermined
tasks.  Every worker specifies a 'group' it is part of, allowing
applications to partition tasks (hereby called 'jobs') to workers of
different kind.

Workers
=======

Normally, the workflow of a worker would be as follows:

    job = jobserver.get(group)
    job_id = job['job_id']
    ... do some work ...
    jobserver.commit(job)

Every job is identified by its unique 'job_id' member.  Jobs also
support the concept of a 'worklog', where related log messages and
errors can be stored:

    jobserver.log(job_id, "A message")

All errors are considered fatal, the error() method is mutually
exclusive with commit(), which instead indicates a successful
completion:

    try:
        ... do some work ...
        jobserver.commit(job)
    except Exception, e:
        jobserver.error(job_id, "Exception: " + str(e))

Workers are expected to regularly 'ping' the server to signal that it
is alive.  This can be done using a separate thread, if your
application is expected to run for a long time, or maybe only once at
program start time if it is run from cron.  If the server fails to
receive pings for a certain period from a node that has a job
assigned, that job will be rescheduled, so be sure to regularly ping
the server when processing long-running jobs.


Producers
=========

The producer can create jobs, and query their status:

    job_id = jobserver.put(job_data, group)
    ...
    status = jobserver.status(job_id)

The status object is a dictionary containing the following elements:

status:
  'e' - the job had errors
  'c' - the job has completed successfully
  'a' - the job is being processed
  'n' - the job is waiting to be assigned

assigned_to:
  The name of the worker that is currently processing the job.

Once created, jobs cannot be canceled.  This will be fixed in a future
release.

"""

from ngv import flags
from ngv import config
import simplejson
import httplib2
from urllib import urlencode
import logging
import os
import socket


# find out the version number of this installation
try:
    import pkg_resources
    _ngvmeta = pkg_resources.get_distribution('ngv')
    VERSION = _ngvmeta.version
except:
    VERSION = 'unknown'


flags.define_string('jobserver_host', None, '--jobserver',
                    help='Jobserver host (host:port)',
                    default='jobserver.ngvision.org:9011')


class HttpError(Exception):
    pass

class HttpStatusError(HttpError):
    def __init__(self, status, msg):
        HttpError.__init__(self, msg)
        self.status_code = status

class RemoteFault(HttpError):
    pass

class JobServerError(Exception):
    pass


class JobServer(object):

    def __init__(self, group=None, server=None):
        """Create a new JobServer proxy.
        
        This class exposes the JobServer API, performing the necessary
        JSON-RPC requests for you.  The 'group' argument must be
        specified only by workers, and they will register themselves
        with the server.
        """
        if not server:
            server = config.jobserver_host
        self.server = server
        self.name = None
        self.group = group
        if group:
            self.name = '%s_%d' % (socket.gethostname(), os.getpid())
            self._register()

    def _register(self):
	uname = os.uname()
	node_info = {'os': uname[0],
	             'os_version': uname[2],
		     'arch': uname[4],
		     'ngv_version': VERSION}
        registration_data = {
            'name': self.name,
            'group': self.group,
            'data': simplejson.dumps(node_info)
            }
        if not self._http('POST', '/register', registration_data):
            raise JobServerError('Registration to jobserver failed')

    def _http(self, method, relative_url, args=None):
        url = 'http://%s%s' % (self.server, relative_url)
        request_data = None
        if method == 'GET' and args:
            url += '?%s' % urlencode(args)
            request_data = None
        elif method == 'POST':
            request_data = urlencode(args)
        try:
            h = httplib2.Http()
            response, content = h.request(url, method, request_data)
        except socket.error, e:
            raise HttpError(str(e))
        if not response.status == 200:
            if response.status == 500:
                data = None
                try:
                    data = simplejson.loads(content)
                except:
                    pass
                if data and 'fault' in data:
                    raise RemoteFault(data['exc'])
            raise HttpStatusError(response.status,
                                  '%s: HTTP status %d' % (url, response.status))
        return simplejson.loads(content)

    def ping(self):
        """Ping the server, telling that this worker is alive."""
        if not self.name:
            raise Exception('ping() cannot be called by anonymous clients')
        return self._http('GET', '/ping/%s' % self.name) == 'OK'

    def put(self, data, group='default'):
        """Create a new job on the server. 'data' can be anything that
        JSON can serialize."""
        enc_data = simplejson.dumps(data)
        ans = self._http('POST', '/add_job', {'group':group, 'data':enc_data})
        return ans

    def get(self):
        """Get the next available job for this node.
        Returns a false value if there is nothing to do.  If the
        server answers with a 404, we might want to try to register
        again.
        """
        if not self.name:
            raise JobServerError('This client is not registered!')
        for i in range(2):
            try:
                return self._http('GET', '/get/%s' % self.name)
            except HttpStatusError, e:
                if e.status_code == 404:
                    self._register()
                    continue
                raise e
        raise JobServerError('Node not recognized')

    def commit(self, job_id):
        """Tell the server that a job was processed successfully."""
        ans = self._http('GET', '/commit/%s' % job_id)
        return ans == 'OK'

    def error(self, job_id, error_message):
        """Tell the server that a fatal error occurred and that this
        job should not be scheduled anymore."""
        ans = self._http('POST', '/error/%s' % job_id,
                         {'error': error_message})
        return ans == 'OK'

    def log(self, job_id, message):
        """Log a message in the job's worklog."""
        ans = self._http('POST', '/log/%s' % job_id,
                         {'message': message})
        return ans == 'OK'

    def status(self, job_id):
        """Retrieve the status of the specified job."""
        return self._http('GET', '/query/%s' % job_id)
