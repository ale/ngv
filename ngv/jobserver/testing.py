# $Id$

from ngv.jobserver import jobserver
from ngv.jobserver import model
import threading
import time


class LocalJobServerThread(threading.Thread):
    JS_PORT = 17001
    def __init__(self):
        threading.Thread.__init__(self)
        self.port = self.JS_PORT
        self.setDaemon(1)
    def run(self):
        addr = ('127.0.0.1', self.port)
        model.meta.session_init('sqlite:///test.db')
        jobserver.start_webserver(addr)


class LocalJobServer:
    _ls = None

    @classmethod
    def start(cls):
        if not cls._ls:
            cls._ls = LocalJobServerThread()
            cls._ls.start()
            time.sleep(1)
        return cls._ls

