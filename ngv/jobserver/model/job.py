#
# JobServer model: Job
# Copyright 2008, ale@incal.net
#
# $Id$

from datetime import datetime
from meta import *
from node import Node
from ngv.lock import Lock
import base64
import logging
import md5
import random
import re
import struct
import time


def gen_id():
    r32 = lambda: struct.pack('L', random.getrandbits(32))
    r64 = r32() + r32()
    # 'ab' introduces a statistical anomaly. we don't really care.
    return base64.b64encode(r64, 'ab').rstrip('=')


jobs_table = Table(
    'jobs', metadata,
    Column('id', String(12), primary_key=True),
    Column('group_name', String(32), index=True),
    Column('status', String(1)),
    Column('assigned_to_id', Integer, ForeignKey('nodes.id')),
    Column('created_at', DateTime),
    Column('assigned_at', DateTime),
    Column('completed_at', DateTime),
    Column('payload', PickleType),
    Column('errors', TEXT),
    )

worklog_table = Table(
    'worklog', metadata,
    Column('id', Integer, primary_key=True),
    Column('job_id', String(12), ForeignKey('jobs.id'), index=True),
    Column('message', TEXT),
    )


class Worklog(object):
    pass


class Job(object):

    STATUS_NEW = 'n'
    STATUS_ASSIGNED = 'a'
    STATUS_COMPLETE = 'c'
    STATUS_ERROR = 'e'

    def complete(self):
        """Set this job status to complete."""
        if (self.status != self.STATUS_ASSIGNED):
            raise Exception("complete() called on job with status=%s" % self.status)
        self.status = self.STATUS_COMPLETE
        self.completed_at = datetime.now()
        Session.add(self)
        Session.commit()

    def error(self, err_msg):
        """Signal an error for this job."""
        if (self.status != self.STATUS_ASSIGNED):
            raise Exception("error() called on job with status=%s" % self.status)
        self.status = self.STATUS_ERROR
        self.completed_at = datetime.now()
        self.errors = 'ERROR (node=%s, job=%s):\n\n%s' % (
             self.assigned_to.name, self.id, err_msg)
        Session.add(self)
        Session.commit()

    def short_error(self):
        """Try to find a Python exception message in the error output."""
	if not self.errors:
	    return None
	error_re = re.compile(r'^[A-Z][a-zA-Z]+(Exception|Error):')
        for line in self.errors.split('\n'):
	    if error_re.search(line):
	        return line
	return None

    def log(self, message):
        """Append a log message for this job."""
        self.worklogs.append(Worklog(message=message))
        Session.add(self)
        Session.commit()

    def worklog(self):
        """Return the complete worklog for this job."""
        if not self.worklogs:
            return ''
        return '\n'.join([x.message for x in self.worklogs])

    @classmethod
    def add(cls, data, group='default'):
        """Add a job to the queue."""
        job = cls(payload=data, group_name=group,
                  created_at=datetime.now(),
                  status=cls.STATUS_NEW,
                  id=gen_id())
        Session.add(job)
        Session.commit()
        return job

    @classmethod
    def next_for(cls, node):
        """Return a job from the queue for 'node', if available."""
        session = Session()
        retries = 10
        while retries > 0:
            next = cls.query.filter(
                (cls.group_name == node.group_name)
                & (cls.status == cls.STATUS_NEW)).first()
            if next is None:
                return None
            lock = Lock(next.id)
            if lock.try_lock():
                try:
                    data = next.payload
                    next.status = cls.STATUS_ASSIGNED
                    next.assigned_to = node
                    next.assigned_at = datetime.now()
                    session.commit()
                    return {'id':next.id, 'data':data}
                finally:
                    lock.unlock()
            retries -= 1
            time.sleep(0.1)
        raise Exception("Couldn't acquire lock!")

    @classmethod
    def detect_dropped_jobs(cls):
        """Detect jobs sent to dead nodes and reset them."""
        session = Session()
        dropped = cls.query.filter((cls.status == cls.STATUS_ASSIGNED)
                                   & (cls.assigned_to_id == Node.id) 
                                   & (Node.enabled == False))
        for j in dropped:
            logging.warning("Clearing stale job %s assigned to dead node %s/%s"
                         % (j.id, j.assigned_to.name, j.assigned_to.group_name))
            j.status = cls.STATUS_NEW
            j.assigned_to = None
            session.add(j)
        session.commit()


Session.mapper(Worklog, worklog_table)
Session.mapper(Job, jobs_table, properties=dict(
        assigned_to=relation(Node, backref=backref('jobs')),
        worklogs=relation(Worklog, order_by=asc(Worklog.id)),
        ))


