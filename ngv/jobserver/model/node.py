#
# JobServer model: Node
# Copyright 2008, ale@incal.net
#
# $Id$

from datetime import datetime, timedelta
from meta import *


nodes_table = Table('nodes', metadata,
                    Column('id', Integer, primary_key=True),
                    Column('enabled', Boolean),
                    Column('name', String(128), index=True, unique=True),
                    Column('group_name', String(32), index=True),
                    Column('stamp', DateTime),
                    Column('data', PickleType),
                    )


class Node(object):
    
    EXPIRATION_TIME = 1800

    def ping(self):
        session = Session()
        self.stamp = datetime.now()
        session.add(self)
        session.commit()

    @classmethod
    def register(cls, name, group, data=None):
        session = Session()
        node = cls.query.filter_by(name=name).first()
        if node:
            node.group_name = group
            node.data = data
        else:
            node = cls(name=name, group_name=group, data=data)
        node.stamp = datetime.now()
        node.enabled = True
        session.add(node)
        session.commit()

    @classmethod
    def disable_expired(cls):
        session = Session()
        now = datetime.now()
        limit = now - timedelta(0, cls.EXPIRATION_TIME)
        for n in cls.query.filter(nodes_table.c.stamp < limit):
            n.enabled = False
            session.add(n)
        session.commit()


Session.mapper(Node, nodes_table)
