#
# JobServer model: shared code
# Copyright 2008, ale@incal.net
#
# $Id$

from sqlalchemy import Column, MetaData, Table, ForeignKey, create_engine
from sqlalchemy import asc, desc, func
from sqlalchemy.types import *
from sqlalchemy.orm import mapper, relation, backref
from sqlalchemy.orm import scoped_session, sessionmaker

from ngv import flags


def session_init(dburi, echo=False):
    engine = create_engine(dburi, echo=echo)
    metadata.create_all(bind=engine)
    Session.configure(bind=engine)


flags.define_string('dburi', '-D', '--dburi', section='jobserver',
                    help='Specify the URI for the database connection (default: sqlite:///jobserver.db)',
                    default='sqlite:///jobserver.db',
                    on_set=session_init)


metadata = MetaData()
Session = scoped_session(sessionmaker(autoflush=True, autocommit=False))
