#!/usr/bin/env python

import unittest
from ngv import lock


class TestLock(unittest.TestCase):

    def test_lock_01(self):
        l = lock.Lock('test1')
        self.assert_(l.lock())
        self.assert_(l.unlock())

    def test_lock_02(self):
        l = lock.Lock('test2')
        self.assert_(l.lock())
        self.assert_(not l.try_lock())
        self.assert_(l.unlock())


if __name__ == '__main__':
    unittest.main()
