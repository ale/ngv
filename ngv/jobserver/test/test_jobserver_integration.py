#!/usr/bin/env python

import unittest
from ngv import application
from ngv import config
from ngv.jobserver import api
from ngv.jobserver import jobserver
from ngv.jobserver import model
from ngv.jobserver.testing import LocalJobServer


SAMPLE_DATA = {'test1': 123,
               'test2': ["a", "b", "c"],
               'test3': {"key": "value"}}


class TestJobServer(unittest.TestCase):

    def setUp(self):
        self.server = LocalJobServer.start()
        self.server_addr = 'localhost:%d' % self.server.port

    def tearDown(self):
        model.Session.remove()

    def _get_js(self, group):
        print "connecting to jobserver on %s, group %s" % (
            self.server_addr, group)
        return api.JobServer(group, self.server_addr)

    def _create_job(self, js, group):
        job_id = js.put(SAMPLE_DATA, group)
        self.assert_(job_id is not None)
        print "created job %s -> %s" % (job_id, group)
        # verify the model directly
        j = model.Job.query.get(job_id)
        self.assert_(j is not None)
        self.assertEqual(j.id, job_id)
        self.assertEqual(j.group_name, group)
        return job_id

    def test_simple_job(self):
        js = self._get_js('test')
        job_id = self._create_job(js, 'test')

        response = js.get()
        self.assert_(response is not None)
        self.assertEqual(SAMPLE_DATA, response['data'])
        self.assertEqual(job_id, response['id'])

        js.commit(job_id)

    def test_commit_on_new_raises_exception(self):
        js = self._get_js('test_conre')
        job_id = self._create_job(js, 'test_conre')
        self.assertRaises(api.RemoteFault,
                          lambda: js.commit(job_id))

    def test_log_and_status(self):
        js = self._get_js('test_log')
        job_id = self._create_job(js, 'test_log')

        test_message = 'quick brown fox'
        self.assert_(js.log(job_id, test_message))

        response = js.status(job_id)
        self.assert_(response is not None)
        for attr in ['status', 'worklog', 'created_at']:
            self.assert_(attr in response,
                         "JS status response missing attr %s" % attr)

        self.assert_(test_message in response['worklog'])

    def test_job_errors(self):
        js = self._get_js('test_err')
        job_id = self._create_job(js, 'test_err')

        err_message = 'test error'
        # tests that you can't call an error on a job that is 
        # not assigned
        self.assertRaises(api.RemoteFault,
                          lambda: js.error(job_id, err_message))

        response = js.get()
        self.assert_(response is not None)
        self.assertEqual(response['id'], job_id)
        js.error(job_id, err_message)

        response = js.status(job_id)
        self.assert_(response is not None)
        self.assert_(response['status'] == 'e')
        self.assert_('errors' in response)
        self.assert_('completed_at' in response)
        self.assert_(err_message in response['errors'])

    def test_reassignment(self):
        js = self._get_js('test_reas')
        job_id = self._create_job(js, 'test_reas')

        # assign it once
        response = js.get()
        self.assert_(response is not None)
        self.assertEqual(job_id, response['id'])

        # tweak the model and disable our node
        node = model.Node.query.filter_by(name=js.name).first()
        self.assert_(node is not None)
        node.enabled = False
        model.Session.update(node)
        print "disabled node %s (group: %s)" % (node.name, node.group_name)
        model.Session.commit()
        model.Session.remove()

        # run the dropped jobs detection routine
        model.Job.detect_dropped_jobs()

        # check status of job
        response = js.status(job_id)
        self.assertEqual(response['status'], 'n')

        # check that we can obtain it again
        response = js.get()
        self.assert_(response is not None)
        self.assertEqual(job_id, response['id'])


if __name__ == '__main__':
    unittest.main()
