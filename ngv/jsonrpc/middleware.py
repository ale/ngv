# $Id$

import logging
import simplejson
import traceback
from ngv import web


class JSONMiddleware(object):
    def __init__(self, next_app):
        self.next_app = next_app
    def __call__(self, environ, start_response):
        try:
            value = self.next_app(environ, start_response)
            output = simplejson.dumps(value)
        except Exception, e:
            logging.error('got exception %s' % str(e))
            output = simplejson.dumps({'fault': True, 
                                       'exc': str(e),
                                       'traceback': traceback.format_exc()})
        return output



def jsonify(func):
    def jsonify_decorator(self, *args):
        try:
            value = func(self, *args)
            output = simplejson.dumps(value)
        except Exception, e:
            logging.error('got exception %s' % str(e))
            output = simplejson.dumps({'fault': True, 
                                       'exc': str(e),
                                       'traceback': traceback.format_exc()})
            web.ctx.status = "500 Internal Server Error"
        web.header('Content-Type', 'text/x-json')
        web.output(output)
    return jsonify_decorator
