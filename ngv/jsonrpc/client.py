# $Id$

### DEPRECATED -- rebuild using httplib2 please!

import logging
import urlparse
import urllib
import urllib2
import simplejson


class RemoteException(Exception):
    pass


class JSONMethodProxy(object):

    def __init__(self, base_url, auth, method_name):
        self._auth = auth
        self._base_url = base_url
        self._method_name = method_name

    def __call__(self, *args, **kwargs):
        auth = self._auth
        if 'auth' in kwargs:
            auth = kwargs['auth']
            del kwargs['auth']
        method = 'GET'
        if 'method' in kwargs:
            method = kwargs['method'].upper()
            del kwargs['method']
        data = urllib.urlencode(kwargs)
        url = '%s/%s' % (self._base_url, self._method_name)
        if args:
            url += '/' + '/'.join(args)
        if method == 'GET':
            url += '?' + data
        if auth:
            realm, user, password = auth
            auth_handler = urllib2.HTTPBasicAuthHandler()
            url_parts = urlparse.urlsplit(url)
            auth_handler.add_password(realm, url_parts[1], user, password)
        else:
            auth_handler = None
        opener = urllib2.build_opener(auth_handler)
        if method == 'POST':
            handle = opener.open(urllib2.Request(url, data))
        else:
            handle = opener.open(url)
        response = simplejson.loads(handle.read())
        if isinstance(response, dict) and 'fault' in response:
            raise RemoteException(response['exc'])
        return response

    def __getattr__(self, attr):
        if attr in self.__dict__:
            return self.__dict__[attr]
        else:
            return JSONMethodProxy(self._base_url, self._auth,
                                   self._method_name + '/' + attr)



class JSONProxy(object):

    def __init__(self, url, auth=None):
        self.auth = auth
        self.url = url

    def __getattr__(self, attr):
        if attr in self.__dict__:
            return self.__dict__[attr]
        else:
            return JSONMethodProxy(self.url, self.auth, attr)

