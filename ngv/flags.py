#
# NGV / Configuration reading interface
# Copyright 2008, ale@incal.net
#
# $Id$
#
"""Handle configuration in a flexible way.

Configuration can be given in the form of command line options,
ini files, with interactions with the environment.  The idea is 
to offer a flexible way of specifying program configuration.

Only one such configuration will be available per-process.
"""

import os
import optparse
import sys
import ConfigParser


class ConfigDict(dict):
    def __getattr__(self, key):
        return self[key]


class ConfigReader(object):

    def __init__(self):
        self.options = []
        self.sections = {}

    def set_usage(self, usage):
        self.parser.set_usage(usage)

    def add_option(self, option):
        self.options.append(option)
        if option.default is not None:
            self._set_option(option, option.default)

    def _set_option(self, opt, value):
        if opt.section != 'DEFAULT':
            if not hasattr(config, opt.section):
                config[opt.section] = ConfigDict()
            config[opt.section][opt.name] = value
        else:
            config[opt.name] = value

    def _parse_files(self):
        config_files = [ '/etc/ngv.conf', 
                         os.path.join(os.getenv('HOME'), '.ngvrc') ]
        if os.getenv('NGVRC'):
            config_files.append(os.getenv('NGVRC'))
        c_parser = ConfigParser.ConfigParser()
        c_parser.read(config_files)
        for opt in self.options:
            try:
                self._set_option(opt, c_parser.get(opt.section, opt.name))
            except (ConfigParser.NoOptionError,
                    ConfigParser.NoSectionError):
                continue
    
    def _parse_commandline(self):
        parser = optparse.OptionParser()
        for opt in self.options:
            opt.to_parser(parser)
        opts, args = parser.parse_args(sys.argv[1:])
        for opt in self.options:
            if hasattr(opts, opt.name) and getattr(opts, opt.name) is not None:
                self._set_option(opt, getattr(opts, opt.name))
        return args

    def parse(self):
        """Parse all options, return command-line arguments."""
        self._parse_files()
        args = self._parse_commandline()
        # fire on-set callbacks
        for opt in self.options:
            if opt.on_set and config[opt.section].get(opt.name):
                opt.on_set(config[opt.section][opt.name])
        return args


class Option(object):

    def __init__(self, name, short_opt=None, long_opt=None, help=None, 
                 section='DEFAULT', default=None, on_set=None):
        self.name = name
        self.short_opt = short_opt
        self.long_opt = long_opt
        self.help = help
        self.default = default
        self.section = section
        self.on_set = on_set

    def _opts_to_args(self):
        args = []
        if self.short_opt:
            args.append(self.short_opt)
        if self.long_opt:
            args.append(self.long_opt)
        return args



class StringOption(Option):
    def to_parser(self, parser):
        args = self._opts_to_args()
        parser.add_option(dest=self.name, help=self.help, 
                          metavar='ARG', *args)


class BoolOption(Option):
    def to_parser(self, parser):
        args = self._opts_to_args()
        parser.add_option(dest=self.name, action='store_true',
                          help=self.help, *args)


config = ConfigDict()
reader = ConfigReader()


def define_string(name, short_opt, long_opt, **kwargs):
    reader.add_option(
        StringOption(name, short_opt, long_opt, **kwargs)
        )

def define_bool(name, short_opt, long_opt, **kwargs):
    reader.add_option(
        BoolOption(name, short_opt, long_opt, **kwargs)
        )
