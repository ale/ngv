# $Id$

import xmlrpclib
from time import time

TOKEN_REFRESH_INTERVAL = 600

class NGVServer(xmlrpclib.ServerProxy):
    def __init__(self, url, user, password):
        xmlrpclib.ServerProxy.__init__(self, url, allow_none=1)
        self._user = user
        self._password = password
        self._token = None
    def token(self):
        if (not self._token or
            (time() - self._token_stamp) > TOKEN_REFRESH_INTERVAL):
            self._token = self.GetAuthToken(self._user, self._password)
            self._token_stamp = time()
        return self._token


