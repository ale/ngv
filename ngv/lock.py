# $Id$

import os
import fcntl
import logging
import flags
from flags import config


flags.define_string('lock_dir', None, '--lock-dir',
                    help='Directory to store lock files (default /tmp)',
                    default='/tmp')


class Lock:

    def __init__(self, key):
        self.key = key
        self.fd = None

    def _path(self):
        return os.path.join(config.lock_dir, '.lock_%s' % str(self.key))

    def _open(self):
        try:
            self.fd = os.open(self._path(), os.O_CREAT | os.O_RDWR | os.O_EXCL)
            return True
        except (IOError, OSError), e:
            return False

    def _close(self):
        try:
            os.close(self.fd)
            os.unlink(self._path())
        except IOError:
            pass

    def try_lock(self):
        if not self._open():
            return False
        try:
            fcntl.lockf(self.fd, fcntl.LOCK_EX | fcntl.LOCK_NB)
            return True
        except IOError:
            self._close()
            return False

    def lock(self):
        if not self._open():
            return False
        try:
            fcntl.lockf(self.fd, fcntl.LOCK_EX)
            return True
        except IOError, e:
            logging.debug('lock() got IOError: %s' % str(e))
            self._close()
            return False

    def unlock(self):
        fcntl.lockf(self.fd, fcntl.LOCK_UN)
        self._close()
        return True

            
        
