#
# NGV / Application common code
# Copyright 2008, ale@incal.net
#
# $Id$

import os
import sys
import web
import signal

import flags
import logging
from logging.handlers import SysLogHandler
from flags import config


flags.define_bool('foreground', '-F', '--foreground', default=False,
                  help='Don\'t detach controlling terminal')
flags.define_bool('debug', '-d', '--debug', default=False,
                  help='Show debugging output')
flags.define_string('access_log_dir', None, '--access-log-dir',
                  default='/var/log', 
                  help='Location of HTTP access log file')
program_name = os.path.basename(sys.argv[0])
flags.define_string('pidfile', '-P', '--pidfile',
                    default='/var/run/%s.pid' % program_name,
                    help='PID file location')


def run(func, daemon=False):
    # parse configuration
    args = flags.reader.parse()

    # set up logging as soon as possible
    if config.foreground:
        log_format = '%%(asctime)-15s [%s(%%(process)d)] - %%(levelname)s: %%(message)s' % program_name
        logging.basicConfig(level=logging.DEBUG, format=log_format, 
                            stream=sys.stderr)

    # daemonize, if debug is not enabled
    if daemon and not config.foreground:
        if os.fork():
            os._exit(0)
        os.setsid()
        pid = os.fork()
        if pid:
            os._exit(0)
        nullfd = open('/dev/null', 'r+')
        for fd in [0,1,2]:
            os.dup2(nullfd.fileno(), fd)
        root_logger = logging.getLogger()
	root_handler = SysLogHandler('/dev/log')
	nice_program_name = program_name
	if program_name.startswith('ngv-'):
	    nice_program_name = program_name[4:]
	root_handler.setFormatter(logging.Formatter(
	    'ngv/%s[%%(process)d]: %%(message)s' % nice_program_name))
        root_logger.addHandler(root_handler)
        if config.debug:
            root_logger.setLevel(logging.DEBUG)
        else:
            root_logger.setLevel(logging.INFO)

    # write pidfile
    try:
        pidf = open(config.pidfile, 'w')
        pidf.write('%s\n' % os.getpid())
        pidf.close()
        def remove_pidfile_and_exit(signo, frame):
	    logging.info('terminating on signal %d' % signo)
            try:
                os.remove(config.pidfile)
            except:
                pass
            os._exit(0)
        signal.signal(signal.SIGTERM, remove_pidfile_and_exit)
        signal.signal(signal.SIGINT, remove_pidfile_and_exit)
    except IOError, e:
        logging.error("Error writing PID file: %s" % str(e))

    func(args)


def ngv_runwsgi(func, address):
    """A copy of web.runwsgi() where we get the address from 
    command-line options."""
    if os.environ.has_key('SERVER_SOFTWARE'):
        os.environ['FCGI_FORCE_CGI'] = 'Y'
    if (os.environ.has_key('PHP_FCGI_CHILDREN')
        or os.environ.has_key('SERVER_SOFTWARE')):
        return web.runfcgi(func, None)
    if 'fcgi' in sys.argv or 'fastcgi' in sys.argv:
        return web.runfcgi(func, address)
    if 'scgi' in sys.argv:
        return web.runscgi(func, address)
    return web.httpserver.runsimple(func, address)


def run_webserver(address, inp, fvars, *middleware):
    # redirect sys.stderr to dump access logs to a file
    access_log = os.path.join(config.access_log_dir,
                              '%s_access.log' % program_name)
    try:
        sys.stderr = open(access_log, 'a', 0)
    except IOError:
        pass
    return ngv_runwsgi(
        web.webapi.wsgifunc(
            web.webpyfunc(inp, fvars),
            *middleware
            ),
        address
        )
