#!/usr/bin/python
# $Id$

from ngv import flags
from ngv import config
from ngv import application
from ngv.workflow.agent import run_agent
from ngv.workflow import *
from ngv.workflow.processing.misc import *
from ngv.workflow.processing.identify import identify
from ngv.workflow.processing.screenshots import screenshot
from ngv.storage.local_storage import LocalStorage
from ngv.storage.ssh_storage import SSHStorage
from ngv.workflow.scripts.common import *
import os
import time
import logging


flags.define_string('storage_path', '-s', '--storage-path',
                    help='Local storage directory',
                    section='agents')
flags.define_string('dist_ssh_url', None, '--dist-ssh-url',
                    help='SSH location of the dist storage',
                    section='agents',
                    default='ngvision@dist.ngvision.org:dl/')
flags.define_string('dist_url', None, '--dist-url',
                    help='Base URL of the dist storage',
                    section='agents',
                    default='http://dist.ngvision.org/new_global_vision/dl/')


def push_to_dist(upload_storage, remote_server, dl_base_url):
    def do_push_to_dist(parms):
        dl_url = upload_storage.get_url(parms['file_id'], dl_base_url)
        remote_server.CreateDirectLink(
            remote_server.token(),
            parms['file_id'], 
            dl_url)
        return parms
    return do_push_to_dist


def main(args):
    # this should point to the frontend storage directory
    local_storage = LocalStorage(config.agents.storage_path)
    dist_storage = SSHStorage(config.agents.dist_ssh_url)
    dist_base_url = config.agents.dist_url
    remote_server = get_remote_server()

    screenshots_task = with_video_info(
        remote_server,
        par(
            take_screenshot_task(
                remote_server, local_storage, 'screenshot'),
            take_screenshot_task(
                remote_server, local_storage, 'screenshot_small')
	    )
	)

    # define the 'incoming' task
    task = with_file_info(
        remote_server,
        seq(
            identify(),
            upload_to_storage(remote_server),
            push_to_dist(dist_storage, remote_server, dist_base_url),
	    ignore_parms(screenshots_task),
            )
        )

    run_agent('incoming', task)


def entry_point():
    return application.run(main, daemon=True)


if __name__ == '__main__':
    sys.exit(entry_point())
