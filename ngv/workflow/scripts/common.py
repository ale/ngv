#!/usr/bin/python
# $Id$

from ngv import flags
from ngv import config
from ngv.workflow import *
from ngv.frontend_api import NGVServer
from ngv.storage.local_cache import LocalCache
from ngv.storage.local_storage import LocalStorage
from ngv.workflow.processing.screenshots import (
    screenshot, create_screenshot_file)


# Define some common options that will be used by other scripts
flags.define_string('ngv_server', None, '--ngv-server',
                    help='URL for the NGV server XMLRPC API',
                    section='agents',
                    default='http://experimental.ngvision.org/api')
flags.define_string('ngv_user', None, '--ngv-user',
                    help='User for authentication',
                    section='agents',
                    default='admin')
flags.define_string('ngv_password', None, '--ngv-password',
                    help='Password for authentication',
                    section='agents')
flags.define_string('cache', None, '--cache',
                    help='Cache to use for storage',
                    section='agents',
                    default='cache:data/storage')


def upload_to_storage(upload_storage):
    def do_upload_to_storage(parms):
        parms['log']('uploading %s to remote upload storage' % parms['local_file_path'])
        upload_storage.put(parms['file_id'], parms['local_file_path'])
        return parms
    return do_upload_to_storage


def check_role_does_not_exist(role, next):
    def do_check_role(parms):
        files = parms['video_info']['files']
        found = False
        for f in files:
            if f['role'] == role:
                found = True
                break
        if not found:
            return next(parms)
        return parms
    return do_check_role


def ignore_parms(next):
    def do_ignore(parms):
        next(parms)
        return parms
    return do_ignore


def get_cache():
    """Return a storage object according to --cache."""
    parts = config.agents.cache.split(':')
    if parts[0] == 'cache':
        path = parts[1]
        if len(parts) == 3:
            size = parts[2]
        else:
            size = '3G'
        return LocalCache(path, size)
    elif parts[0] == 'local':
        path = parts[1]
        return LocalStorage(path)
    else:
        raise Exception('unknown storage type "%s"' % parts[0])


def get_remote_server():
    """Returns a NGVServer object created using configuration
    parameters."""
    return NGVServer(config.agents.ngv_server,
                     config.agents.ngv_user,
                     config.agents.ngv_password)


def take_screenshot_task(remote_server, dist_storage, role):
    return ignore_parms(
        check_role_does_not_exist(
            role,
            seq(
                screenshot(3, 5, role),
                create_screenshot_file(remote_server),
                upload_to_storage(dist_storage),
                )
            ))
