#!/usr/bin/python
# $Id$


from ngv import flags
from ngv import config
from ngv import application
from ngv.workflow.agent import run_agent
from ngv.workflow import *
from ngv.workflow.processing.misc import *
from ngv.workflow.processing.identify import identify
from ngv.workflow.processing.encode import encode
from ngv.storage.local_storage import LocalStorage
from ngv.storage.ssh_storage import SSHStorage
from ngv.workflow.scripts.common import *
import os
import time
import logging


flags.define_string('flv_storage_addr', None, '--flv-storage',
                    help='Remote storage host (ssh/scp), format is user@host:path',
                    default='localhost',
                    section='agents')
flags.define_string('flv_storage_url', None, '--flv-url',
                    help='Base URL for the remote FLV storage',
                    section='agents')
flags.define_string('screenshots_ssh_url', None, '--screenshots-url',
                    help='Remote storage host for screenshots',
                    section='agents',
                    default='ngvision@experimental.ngvision.org:/srv/ngvision/ngv_frontend/data/storage')


class FLVStorage(SSHStorage):
    def map_key(self, key):
        return SSHStorage.map_key(self, key) + '.flv'
    def url_for(self, key):
        return "%s/%s.flv" % (config.agents.flv_storage_url, 
                              self._hash_key(key))


def verify_parms():
    def do_verify_parms(parms):
        for key in ['file_id', 'video_id', 'links', 'name']:
            if key not in parms:
                raise Exception("Error in parameters: missing '%s': %s" %
                                (key, str(parms)))
        return parms
    return do_verify_parms


def choose_action(actions):
    def do_choose_action(parms):
        next_action = actions[parms['action']]
        return next_action(parms)
    return do_choose_action


def upload_new_video_file(remote_server, role):
    def do_upload_new_file(parms):
        video_id = parms['video_id']
        attrs = {}
        for key in NGV_FILE_FIELDS:
            if key in parms:
                attrs[key] = parms[key]
        attrs['role'] = role
        attrs['ftype'] = 'video'
        attrs['size'] = os.path.getsize(parms['local_file_path'])
        parms['log']('creating new file on server, parms:\n%s' % str(attrs))
        file_id = remote_server.CreateFile(remote_server.token(), 
                                           video_id, attrs)
        parms['log']('new file ID is %d' % file_id)
        parms['file_id'] = file_id
        return parms
    return do_upload_new_file


def flv_direct_link(remote_server, flv_storage):
    def do_flv_direct_link(parms):
        remote_server.CreateDirectLink(
            remote_server.token(),
            parms['file_id'],
            flv_storage.url_for(parms['file_id'])
            )
        return parms
    return do_flv_direct_link


def main(args):
    flv_storage = FLVStorage(config.agents.flv_storage_addr)
    temp_storage = get_cache()
    remote_server = get_remote_server()
    screenshots_storage = SSHStorage(config.agents.screenshots_ssh_url)

    screenshots_task = par(
        take_screenshot_task(
            remote_server, screenshots_storage, 'screenshot'),
        take_screenshot_task(
            remote_server, screenshots_storage, 'screenshot_small')
        )

    encode_flv_task = check_role_does_not_exist(
        'stream_flv',
        ignore_parms(
            seq(
                encode('flv'),
                identify(),
                upload_new_video_file(remote_server, 'stream_flv'),
                upload_to_storage(flv_storage),
                flv_direct_link(remote_server, flv_storage)
                )
            ))

    process_task = par(
        screenshots_task,
        encode_flv_task
        )

    encode_theora_task = check_role_does_not_exist(
        'stream_theora',
        ignore_parms(
        seq(
            encode('theora'),
            identify(),
            upload_new_video_file(remote_server, 'stream_theora'),
            upload_to_storage(flv_storage)
            )
        ))

    task = with_file_info(
        remote_server,
        seq(
            verify_parms(),
            with_local_copy(
                remote_server,
                temp_storage,
                with_video_info(
                    remote_server,
                    choose_action({
                            'identify': identify(),
                            'encode_flv': encode_flv_task,
                            'encode_theora': encode_theora_task,
                            'process': process_task,
                            'screenshots': screenshots_task,
                            })
                    )
                )
            )
        )

    run_agent('processing', task)


def entry_point():
    return application.run(main, daemon=True)


if __name__ == '__main__':
    sys.exit(entry_point())
