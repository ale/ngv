# $Id$

from ngv import flags
from ngv import config
from ngv import application
from ngv.jobserver.api import JobServer
import os
import time
import logging


def main(args):
    group = args[0]
    data = eval(args[1])
    jobserver = JobServer(register=False)
    jobserver.put(data, group)
    return 0


def entry_point():
    return application.run(main)


if __name__ == '__main__':
    sys.exit(entry_point())
