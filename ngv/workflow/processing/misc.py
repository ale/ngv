# $Id$

import os
import logging
import random
import re
import shutil
import subprocess
import urllib2
import time
from datetime import datetime
from ngv.workflow import FatalError


NGV_FILE_FIELDS = ['role', 'name', 'ftype', 'mime_type',
                   'size', 'notes', 'frame_width', 'frame_height', 
                   'duration', 'fps', 'video_codec', 'audio_codec',
                   'audio_lang', 'subtitles_lang', 'lang', 'width',
                   'height', 'link']


def run(logger, *args):
    """Run a command. Returns status and stdout/stderr output."""
    logger("executing %s" % ' '.join(args))
    pipe = subprocess.Popen(args,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT)
    output = pipe.communicate()[0]
    status = pipe.wait()
    if status != 0:
        logger("error running '%s': status=%d, output:\n%s" % (
            args[0], status, output))
    return (status, output)


def change_ext(filename, ext):
    """Change the extension of 'filename'. Ensure that 'ext' 
    includes a leading dot (.txt, .tar.gz etc)."""
    result, n = re.subn(r'\.[a-z]{1,4}$', ext, filename)
    if n == 0:
        return filename + ext
    return result


def with_job(jobserver, next):
    def do_with_job(parms):
        _logs = []
        def logger(msg):
            logging.info(msg)
            _fmt = '%s %s' % (datetime.now().isoformat(), msg)
            _logs.append(_fmt)
        parms['log'] = logger
        result = next(parms)
        job_id = parms['job_id']
        if _logs:
            jobserver.log(job_id, '\n'.join(_logs))
        if 'error' in result:
            jobserver.error(job_id, result['error'])
        else:
            jobserver.commit(job_id)
        return result
    return do_with_job


def with_file_info(remote_server, next):
    """Fetch information on a file, execute the child tasks
    and then upload it back to the server."""
    def do_with_file_info(parms):
        file_id = parms['file_id']
        file_info = remote_server.GetFileInfoById(file_id)
        # prevent error looping in debug messages...
        if 'errors' in file_info:
            del file_info['errors']
        parms.update(file_info)
        result = next(parms)
        update_info = {}
        for k in NGV_FILE_FIELDS:
            if k in result:
                update_info[k] = result[k]
        remote_server.UpdateFile(remote_server.token(), file_id, update_info)
        if 'log' in parms:
            parms['log']("update file information for file_id=%s" % (
                    str(file_id),))
        return result
    return do_with_file_info


def with_video_info(remote_server, next):
    def do_with_video_info(parms):
        video_info = remote_server.GetVideoById(parms['video_id'])
        parms['video_info'] = video_info
        return next(parms)
    return do_with_video_info


def with_local_storage(storage, next):
    def do_with_local_storage(parms):
        tmpf = os.tmpnam()
        storage.get(parms['file_id'], tmpf)
        parms['local_file_path'] = tmpf
        result = next(parms)
        os.unlink(tmpf)
        return result
    return do_with_local_storage


# To download files we can take advantage of PyCURL if it's installed.
try:
    import pycurl
    import signal
    def _download(storage, key, url):
        prev_handler = signal.signal(signal.SIGPIPE, signal.SIG_IGN)
        path = storage.map_key(key)
        outfd = storage.open(key, 'w')
        cur_size = 0
        if os.path.exists(path):
            cur_size = os.path.getsize(path)
        c = pycurl.Curl()
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.TIMEOUT, 900)
        c.setopt(pycurl.CONNECTTIMEOUT, 30)
        c.setopt(pycurl.WRITEDATA, outfd)
        c.setopt(pycurl.LOW_SPEED_LIMIT, 10000)
        c.setopt(pycurl.LOW_SPEED_TIME, 300)
        c.setopt(pycurl.NOSIGNAL, 1)
        if os.path.exists(path):
            c.setopt(pycurl.RESUME_FROM, os.path.getsize(path))
        try:
            c.perform()
            c.close()
        finally:
            outfd.close()
            signal.signal(signal.SIGPIPE, prev_handler)
except ImportError:
    def _download(storage, key, url):
        try:
            outfd = storage.open(key, 'w')
            response = urllib2.urlopen(url)
            shutil.copyfileobj(response, outfd)
        finally:
            outfd.close()


def with_local_copy(remote_server, storage, next):
    def do_with_local_copy(parms):
        if storage.exists(parms['file_id']):
            parms['log']('file %d already cached locally' % parms['file_id'])
        else:
            retries = 5
            start_time = time.time()
            while retries > 0:
                url = parms['links'][random.randrange(0, len(parms['links']))]
                if url.startswith('/'):
                    parms['log']('warning: relative download path: %s' % url)
                    url = 'http://experimental.ngvision.org%s' % url
                try:
                    parms['log']('downloading file from %s' % url)
                    outf = storage.open(parms['file_id'], 'w')
                    _download(storage, parms['file_id'], url)
                    outf.close()
                    break
                except Exception, e:
                    parms['log']("Error downloading %s: %s" % (url, str(e)))
                    retries -= 1
                    if retries > 0:
                        time.sleep(3)
                        continue
                    os.unlink(storage.map_key(parms['file_id']))
                    raise e
            end_time = time.time()
            size = os.path.getsize(storage.map_key(parms['file_id']))
            kbps = int((size / 1000.0) / (end_time - start_time))
            parms['log']('successfully downloaded file %s (%d bytes, %d KB/s)' % (
                    parms['file_id'], size, kbps))
        parms['local_file_path'] = storage.map_key(parms['file_id'])
        result = next(parms)
        return result
    return do_with_local_copy

