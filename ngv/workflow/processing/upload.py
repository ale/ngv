# $Id$

import os
import logging
from pstore import storage
from ngv.workflow import FatalError
from ngv.app import ctx


def upload(file_id=None, kind='default'):
    def do_upload(parms):
	if not parms.has_key('local_file_path'):
	    raise FatalError("upload: No local_file_path!")
	input_file = parms['local_file_path']
	fid = str(parms.get('file_id', file_id))
	if not fid:
	    raise FatalError("upload: no file_id!")
	r = storage.put(fid, input_file, kind)
	logging.debug('uploaded %s with key %s' % (input_file, fid))
	if not r:
	    raise FatalError("Error uploading file %s to storage!" % input_file)
	return parms
    return do_upload


def update(file_id=None):
    def do_update(parms):
        fid = parms.get('file_id', file_id)
        result = ctx.server('file/' + str(fid)).post(parms)
        if not result:
            raise FatalError("Error updating file %d!" % fid)
        return parms
    return do_update


def register(file_name='untitled', ftype='other', role='', video_id=None, **kw):
    def do_register(parms):
	input_file = parms['local_file_path']
	vid = parms.get('video_id', video_id)
	out_parms = parms.copy()
	out_parms['name'] = file_name
	out_parms['ftype'] = ftype
	out_parms['role'] = role
	out_parms['video_id'] = vid
	for k, v in kw.iteritems():
	    out_parms[k] = v
	file_id = ctx.server('file/new').post(out_parms)
	if not file_id:
	    raise FatalError("Error registering new file!")
	logging.debug('registered file %s: %s' % (str(file_id), str(out_parms)))
	out_parms['file_id'] = file_id
	return out_parms
    return do_register


