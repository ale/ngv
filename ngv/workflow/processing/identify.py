# $Id$
# identify a video parameters using ffmpeg

__all__ = [ 'identify' ]

import os
import re
import commands
import logging


# convert container to mime type
fmt_to_mime = {
        'asf': 'video/x-msvideo',
        'mpeg': 'video/mpeg',
	'mpegts': 'video/mpeg',
	'mpegps': 'video/mpeg',
        'avi': 'video/x-msvideo',
        'mp4': 'video/mp4',
        'rm': 'application/vnd.vn-realmedia',
        'mov': 'video/quicktime'
        }


def __run_ffmpeg(path):
    """Use 'ffmpeg' to parse information about a video."""
    info = dict(streams=[])
    output = commands.getoutput('ffmpeg -i %s' % commands.mkarg(path))
    for line in output.split('\n'):
	m = re.search(r'Input #0, (\w+)', line)
	if m:
	    info['fmt'] = m.group(1)
	    if fmt_to_mime.has_key(info['fmt']):
		info['mime_type'] = fmt_to_mime[info['fmt']]
	    else:
		info['mime_type'] = 'application/octet-stream'
	m = re.search(r'Duration: ([0-9:.]+)', line)
	if m:
	    info['duration'] = m.group(1)
	m = re.search(r'Stream #0\.(\d+).*: Audio: (\w+), (\d+ Hz), (\w+)', line)
	if m:
	    sid = int(m.group(1))
	    info['streams'].append(dict(type = 'audio',
					codec = m.group(2),
					channels = m.group(4)))
	m = re.search(r'Stream #0\.(\d+).*, +([0-9.]+).*Video: (\w+), (\w+), (\d+)x(\d+)', line)
	if m:
	    sid = int(m.group(1))
	    info['streams'].append(dict(type = 'video',
					codec = m.group(3),
					fps = m.group(2),
					encoding = m.group(4),
					frame_width = int(m.group(5)),
					frame_height = int(m.group(6))))
	else:
	    m = re.search(r'Stream #0\.(\d+).*Video: +(\w+), +(\w+), +(\d+)x(\d+)', line)
	    if m:
		sid = int(m.group(1))
		streaminfo = dict(type = 'video',
                                  codec = m.group(2),
                                  encoding = m.group(3),
                                  frame_width = int(m.group(4)),
                                  frame_height = int(m.group(5)))
                m2 = re.search(r' ([0-9.]+)fps', line)
                if m2:
                    streaminfo['fps'] = m2.group(1)
                info['streams'].append(streaminfo)
    if not info.has_key('fmt'):
	raise Exception("The video file has an unknown format")
    return (info, output)


def identify():
    def do_identify(parms):
	"""Parse the information about the video from 'identify' into
	a format that the database can handle."""
	path = parms['local_file_path']
        parms['log']('identifying %s' % path)
	info, ffmpeg_output = __run_ffmpeg(path)
        parms['log']('parsed output:\n%s' % str(info))
	out = dict(video_codec = '',
		   audio_codec = '',
		   fps = '',
		   frame_width = '',
		   frame_height = '',
		   duration = info['duration'],
		   mime_type = info['mime_type'])
	for s in info['streams']:
	    if s['type'] == 'video' and not out['video_codec']:
		out['video_codec'] = s['codec']
		out['frame_width'] = s['frame_width']
		out['frame_height'] = s['frame_height']
		out['fps'] = s.get('fps', '')
	    elif s['type'] == 'audio' and not out['audio_codec']:
		out['audio_codec'] = s['codec']
	out['size'] = os.path.getsize(path)
	parms.update(out)
	return parms
    return do_identify
