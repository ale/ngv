# $Id$

from screenshots import screenshot
from upload import upload, register
from identify import identify
from app.agents.task import seq, parallel


def incoming():
    return seq(
	    identify(),
	    upload()
	    )

	
