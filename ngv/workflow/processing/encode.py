# $Id$
# re-encode a video to various predefined formats

import os
import logging
from ngv.workflow.processing.misc import run, change_ext
from ngv.workflow import FatalError


def _encode_flv(logger, input_file, output_file):
    status, output = run(
        logger, 
        'ffmpeg', '-y', '-i', input_file, '-f', 'flv',
        '-acodec', 'libmp3lame', '-ar', '22050', '-ab', '32000', 
        '-ac', '1', '-b', '300000', '-s', '320x240', output_file)
    if status != 0:
        raise FatalError('Error in ffmpeg FLV encoding of %s' % input_file)
    status, output = run(logger, 'flvtool2', '-U', output_file)
    if status != 0:
        raise FatalError('Error in FLV annotation of %s' % output_file)


def _encode_streaming(logger, input_file, output_file):
    if not run(logger, 'ffmpeg2theora', '-i', input_file, output_file):
        raise FatalError('Error in ffmpeg2theora conversion of %s' % input_file)


def encode(method, filename=None):
    def do_encode(parms):
	out_parms = parms.copy()
	input_file = parms['local_file_path']
        #if filename:
        #    output_file = os.path.join(parms['work_dir'], filename)
        #else:
        #    output_file = input_file + '_' + method
        output_file = os.tmpnam()
	out_parms['local_file_path'] = output_file
	if method == 'flv' or method == 'flash':
	    _encode_flv(parms['log'], input_file, output_file)
            out_parms['name'] = change_ext(out_parms['name'], '.flv')
	elif method == 'theora':
	    _encode_streaming(parms['log'], input_file, output_file)
            out_parms['name'] = change_ext(out_parms['name'], '.ogg')
	else:
	    raise FatalError("No such encoding method: %s" % method)
	return out_parms
    return do_encode
