# $Id$

from screenshots import screenshot
from upload import upload, register
from download import download
from encode import encode
from identify import identify

from ngv.app import ctx
from ngv.workflow import seq, par, unless


def check_for_filename(file_name):
    def do_check_for_filename(parms):
	video_info = ctx.server('video/' + str(parms['video_id'])).get()
	for f in video_info['files']:
	    if f['name'] == file_name:
		return True
	return False
    return do_check_for_filename

def check_for_role(role):
    def do_check_for_role(parms):
	video_info = ctx.server('video/' + str(parms['video_id'])).get()
	for f in video_info['files']:
	    if f['role'] == role:
		return True
	return False
    return do_check_for_role


# TODO: add more low-quality renderings (a low-bitrate MPEG-1 for example)

def render_variants():
    return seq(
    	    download(),
	    par(unless(check_for_role('screenshot'),
                       par(seq(screenshot(1, 3), 
                               register(file_name='shot_1.png', ftype='screenshot', 
                                        role='screenshot', mime_type='image/png'),
                               upload()),
                           seq(screenshot(2, 3), 
                               register(file_name='shot_2.png', ftype='screenshot', 
                                        role='screenshot', mime_type='image/png'),
                               upload()),
                           seq(screenshot(3, 3), 
                               register(file_name='shot_3.png', ftype='screenshot', 
                                        role='screenshot', mime_type='image/png'),
                               upload()))
                       ),
		unless(check_for_role('flash'),
		       seq(encode('flv', 'preview.flv'),
			   identify(),
			   register(ftype='video', role='flash', 
				    mime_type='application/shockwave-flash'),
			   upload())
                       ),
                ),
	    )


