# $Id$

import logging
import traceback
from ngv.workflow import TemporaryError, FatalError


def _format_error(exc, parms):
    return """%s

Traceback:
%s

Current context:
%s
""" % (str(exc), traceback.format_exc(), str(parms))


def catch_errors(next):
    """A modification of the 'seq' pipeline function that 
    catches errors in the processing pipeline and reports them
    to the main server, so that they can be associated to the
    processed video file."""
    def do_catch_errors(parms):
        try:
            return next(parms)
        except Exception, e:
            logging.error('Received Exception: %s' % str(e))
            if 'log' in parms:
                parms['log']('received Exception: %s' % str(e))
            if 'job_id' in parms:
                error_message = _format_error(e, parms)
                parms['error'] = error_message
                return parms
            else:
                raise e
    return do_catch_errors
