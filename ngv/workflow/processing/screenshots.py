# $Id$
# create screenshots

__all__ = [ 'gen_time_series', 'screenshot' ]


import os
import logging
from datetime import timedelta
from ngv.workflow.processing.misc import run, change_ext
from ngv.workflow import FatalError


def gen_time_series(duration_str, n):
    d = [ float(x) for x in duration_str.split(':') ]
    duration_secs = 0
    while d: 
	duration_secs = duration_secs * 60 + d.pop(0)
    times = []
    for i in range(n + 1):
	times.append(str(timedelta(0, int(duration_secs * i / (n+1)))))
    return times[1:n+1]


def _screenshot(logger, input_file, output_file, time='00:00:10', size='160x120'):
    status, output = run(
        logger, 'ffmpeg', '-y', '-i', input_file,
        '-vframes', '1', '-ss', time,
        '-an', '-vcodec', 'gif', '-f', 'rawvideo',
        '-s', size, output_file + '.gif')
    if status != 0:
        raise FatalError("Error creating screenshot (%s) at %s for %s" % (
	    size, time, input_file))
    status, output = run(
        logger, 'convert', output_file + '.gif',
        '-quality', '70', 'jpeg:' + output_file)
    if status != 0:
        raise FatalError("Error converting screenshot to JPEG (%s) at %s for %s" % (
	    size, time, input_file))
    os.remove(output_file + '.gif')
    return True


def screenshot(s_num, s_total, role='screenshot'):
    def do_screenshot(parms):
        role_sizes = {'screenshot': '320x240',
	              'screenshot_small': '160x120'}

	input_file = parms['local_file_path']
	logging.info('about to take screenshot of %s' % input_file)
	out_parms = parms.copy()
	output_file = os.tmpnam()
	shot_time = gen_time_series(parms['duration'], s_total)[s_num - 1]

	msg = "creating %s (%s), %d out of %d, file:%d, time %s" % (
	    role, role_sizes[role], s_num, s_total, parms['file_id'], shot_time)
	parms['log'](msg)

	if not _screenshot(parms['log'], input_file, output_file, shot_time, role_sizes[role]):
	    raise FatalError("Could not create screenshot")

        out_parms['name'] = change_ext(parms['name'], '-%s.jpg' % role)
	out_parms['local_file_path'] = output_file
	out_parms['size'] = os.path.getsize(output_file)
	if out_parms['size'] == 0:
	    raise FatalError("Screenshot has size 0!")
	out_parms['ftype'] = 'other'
	out_parms['role'] = role
	return out_parms
    return do_screenshot


def create_screenshot_file(remote_server):
    def do_create_screenshot_file(parms):
        video_id = parms['video_id']
	attrs = {'role': parms['role'],
	         'ftype': 'other',
		 'mime_type': 'image/jpeg',
		 'size': parms['size']}
	parms['log']('creating new file on server, parms:\n%s' % str(attrs))
	file_id = remote_server.CreateFile(remote_server.token(),
	                                   video_id, attrs)
	parms['log']('new file ID is %d' % file_id)
	parms['file_id'] = file_id
	return parms
    return do_create_screenshot_file

