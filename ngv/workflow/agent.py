# $Id$

from ngv.jobserver.api import JobServer
from ngv.workflow.base import par, seq, unless, FatalError, TemporaryError
from ngv.workflow.processing.misc import *
from ngv.workflow.processing.error_catcher import catch_errors
import logging
import time
import traceback
import threading
import sys


class ping_thread(threading.Thread):
    """Pinger thread.
    Thread that keeps pinging the server so that during long
    computations it won't think that we're dead and reallocate
    the job to some other node."""
    
    PING_TIME = 600

    def __init__(self, jobserver):
        threading.Thread.__init__(self)
        self.setDaemon(1)
        self.jobserver = jobserver

    def run(self):
        while 1:
            time.sleep(self.PING_TIME)
            self.jobserver.ping()


def run_agent(group, task):
    """Run the agent.

    The agent will register itself with the job server and
    periodically check for new jobs for the given 'group'.  When a job
    is available, it will be processed using 'task'.  Errors and
    uncaught exceptions will be logged on the server.
    """
    try:
        jobserver = JobServer(group=group)
    except Exception, e:
        logging.fatal("Could not register with the JobServer: %s" % str(e))
        sys.exit(1)

    task = with_job(jobserver, catch_errors(task))
    logging.info("starting agent: %s" % group)

    pinger = ping_thread(jobserver)
    pinger.start()

    while 1:
        time.sleep(60)
        try:
            job = jobserver.get()
        except Exception, e:
            logging.error("JobServer Exception: %s" % str(e))
            continue
        try:
            if not job:
                continue
            if not ('id' in job and 'data' in job):
                logging.error("Wrong job data: %s" % str(job))
                continue
            logging.debug("received job %s (%s)" % (job['id'], job['data']))
            job['data']['job_id'] = job['id']
            result = task(job['data'])
            logging.debug("job completed")
        except Exception, e:
            err_msg = "Uncaught Exception: %s\n%s" % (
                str(e), traceback.format_exc())
            logging.error(err_msg)
            try:
                jobserver.error(job['id'], err_msg)
            except:
                logging.warning("Error while canceling job %s" % job['id'])
            continue
