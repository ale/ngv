# $Id$


import os
import logging
import subprocess
import cPickle as pickle


### errors 

class FatalError(Exception):
    pass


class TemporaryError(Exception):
    pass


### basic sequential / parallel operation

def par(*pipeline):
    """Returns a function that invokes its arguments in parallel,
    What this really means is that each one of them is passed
    a copy of the incoming state, which is then returned as
    output."""
    def do_parallel(parms):
	for t in pipeline:
	    result = t(parms)
	return result
    return do_parallel


def seq(*pipeline):
    """Returns a function that invokes its arguments sequentially,
    that is, functions' input and output states are linearly
    connected. The last output state is returned."""
    def do_seq(parms):
	cur = parms
	for t in pipeline:
	    cur = t(cur)
	return cur
    return do_seq


### conditional operations

def unless(condition, next):
    """Executes 'next' if 'condition' holds false."""
    def do_unless(parms):
	if condition(parms):
	    return parms
	else:
	    return next(parms)
    return do_unless


