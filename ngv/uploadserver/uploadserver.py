# $Id$

from ngv import flags
from ngv import config
from ngv import application
from ngv import web
from model import Session, AsyncUpload

import cgi
import logging
import sys
import time
import os
import simplejson
from md5 import md5
import random


flags.define_string('bind_addr', '-b', '--bind',
                    help='Specify the bind address (default 0.0.0.0)',
                    section='uploadserver',
                    default='0.0.0.0')
flags.define_string('port', '-p', '--port',
                    help='TCP port to bind to (default 9012)',
                    section='uploadserver',
                    default='9012')



class ProgressFile(file):
    def write(self, *k, **p):
        if hasattr(self, 'callback'):
            self.callback(self, *k, **p)
        return file.write(self, *k,**p)
    def set_callback(self, callback):
        self.callback = callback


def stream(file_object):
    class CustomFieldStorage(cgi.FieldStorage):
        def make_file(self, binary=None):
            self.open_file = file_object
            return self.open_file
    return CustomFieldStorage


class Generate:
    def GET(self):
        m = md5()
        m.update(str(time.time()))
        m.update(str(random.randint(0, 9999999)))
        token = m.hexdigest()
        aobj = AsyncUpload(token=token, size=0, cur_size=0,
                           exp_size=0, status='N')
        Session.add(aobj)
        Session.commit()
        web.header('Content-Type', 'application/json')
        web.output(simplejson.dumps(token))


class Ping:
    def GET(self, token):
        aobj = AsyncUpload.query.filter_by(token=token).first()
        if not aobj:
            return web.notfound()
        web.header('Content-Type', 'application/json')
        web.output(simplejson.dumps([aobj.status, aobj.cur_size, aobj.exp_size]))


class Receiver:
    def POST(self, token):
        # first create the temporary object corresponding to this session
        # if this method aborts, the object is destroyed.
        completed = False

	logging.info('request for token ' + token)
        temp_obj = AsyncUpload.query.filter_by(token=token).first()
        if not temp_obj:
            return web.notfound()
        if temp_obj.size != 0:
            # do not allow another upload if already started
            return web.notfound()
        temp_path = temp_obj.path()
        rough_size = int(web.ctx.environ.get('CONTENT_LENGTH', '0'))
        temp_obj.exp_size = rough_size
        Session.add(temp_obj)
        Session.commit()
        logging.info("upload: new AsyncUpload: %s" % token)

        class Buffered(object):
            def __init__(self, obj):
                self.obj = obj
                self.last_sz = 0
            def update(self, pos):
                self.obj.cur_size = pos
                if pos - self.last_sz > 16384:
                    Session.add(self.obj)
                    Session.commit()
                    self.last_sz = pos
        buffobj = Buffered(temp_obj)
        def callback(file, *k, **p):
            buffobj.update(file.tell())
        fp = ProgressFile(temp_path, 'wb')
        fp.set_callback(callback)
        custom_field_storage_class = stream(fp)
        custom_field_storage = custom_field_storage_class(
            environ=web.ctx.environ,
            keep_blank_values=1,
            fp=web.ctx.environ['wsgi.input']
            )
        fp.close()

        final_size = os.path.getsize(temp_path)
        logging.info("%s: uploaded %d bytes" % (token, final_size))
        temp_obj.exp_size = temp_obj.size = temp_obj.cur_size = final_size
        temp_obj.status = 'C'
        Session.add(temp_obj)
        Session.commit()

        web.output('OK')


urls = (
    '/_uploadserver/receiver/(.*)', 'Receiver',
    '/_uploadserver/ping/(.*)', 'Ping',
    '/_uploadserver/gen', 'Generate',
    )


def main(args):
    addr = config.uploadserver.bind_addr
    port = int(config.uploadserver.port)
    logging.info('starting server on %s:%d' % (addr, port))
    try:
        application.run_webserver((addr, port), urls, globals())
    except KeyboardInterrupt:
        sys.exit(0)


def entry_point():
    application.run(main, daemon=True)


if __name__ == '__main__':
    entry_point()

