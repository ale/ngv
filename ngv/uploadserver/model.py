from sqlalchemy import Column, MetaData, Table, create_engine
from sqlalchemy.types import *
from sqlalchemy.orm import scoped_session, sessionmaker

from ngv import flags
from ngv import config
import os
import logging


def session_init(dburi):
    sa_engine = create_engine(dburi)
    Session.configure(bind=sa_engine)

flags.define_string('dburi', '-D', '--dburi', section='uploadserver',
                    help='Specify the URI for the database connection (to the main ngv database)',
                    default='sqlite:///ngv_frontend.db',
                    on_set=session_init)
flags.define_string('upload_path', '-U', '--upload-path',
                    section='uploadserver',
                    help='Base path for storage',
                    default='.')

metadata = MetaData()

Session = scoped_session(sessionmaker(autoflush=True, autocommit=True))
                                               

# Status:
# N = new/uploading
# C = complete
# E = error
uploads_table = Table('async_uploads', metadata,
                      Column('token', String(48), primary_key=True),
                      Column('status', String(1)),
                      Column('size', Integer),
                      Column('cur_size', Integer, default=0),
                      Column('exp_size', Integer, default=0),
                      )


class AsyncUpload(object):
    def path(self):
        return os.path.join(config.uploadserver.upload_path, 
                            'upload_%s' % self.token)
    def remove(self):
        try:
            os.remove(self.path())
        except IOError:
            pass
        Session.delete(self)


Session.mapper(AsyncUpload, uploads_table)
