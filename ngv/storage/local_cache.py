# $Id$

import os
import fcntl
import shutil
import logging
import cPickle as pickle
from local_storage import LocalStorage


def _parse_size(s):
    if s[-1:] in "mgkMGK":
        mul = {'k': 1000,
               'm': 1000000,
               'g': 1000000000}
        i = int(s[:-1]) * mul[s[-1:].lower()]
    else:
        i = int(s)
    return i


class LocalCache(LocalStorage):

    def __init__(self, base_path, size='2G'):
        LocalStorage.__init__(self, base_path)
        self.cache_size = _parse_size(size)
        self.state_file = os.path.join(base_path, '.cache_state')

    def _with_cache(self, fn):
        try:
            statefd = open(self.state_file, 'r+')
        except IOError:
            statefd = open(self.state_file, 'w+')
        try:
            cache = pickle.load(statefd)
        except EOFError:
            cache = []
        fcntl.lockf(statefd, fcntl.LOCK_EX)
        try:
            ret = fn(cache)
            statefd.seek(0)
            pickle.dump(cache, statefd)
        finally:
            fcntl.lockf(statefd, fcntl.LOCK_UN)
            statefd.close()
        return ret

    def _add_to_cache(self, path, size):
        if size > self.cache_size:
            raise Exception("File too big for cache!")
        def add_fn(cache):
            used_space = sum([x[1] for x in cache])
            free_space = self.cache_size - used_space
            logging.info("cache used/free = %d / %d" % (
                    used_space, free_space))
            # remove items from FIFO until there is enough space
            while free_space < size:
                item_path, item_size = cache.pop(0)
                try:
                    os.remove(item_path)
                except OSError:
                    pass
                logging.info("removed %s (%d bytes)" % (
                        item_path, item_size))
                free_space += item_size
            # append new entry to FIFO and save
            logging.info("add to cache: %s (%d bytes)" % (
                    path, size))
            cache.append((path, size))
        self._with_cache(add_fn)

    def put(self, key, input_file_name):
        size = os.path.getsize(input_file_name)
        self._add_to_cache(self.map_key(key), size)
        return LocalStorage.put(self, key, input_file_name)

    def update_cache(self, key):
        path = self.map_key(key)
        size = os.path.getsize(path)
        self._add_to_cache(path, size)

    def delete(self, key):
        LocalStorage.delete(self, key)
        path = self.map_key(key)
        def delete_fn(cache):
            for item in cache:
                if item[0] == path:
                    cache.remove(item)
                    break
        self._with_cache(delete_fn)

