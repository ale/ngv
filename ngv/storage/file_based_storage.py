# $Id$

import os


class FileBasedStorage(object):

    def _hash_key(self, key):
        key = str(key)
        return os.path.join(key[0].lower(), key)

