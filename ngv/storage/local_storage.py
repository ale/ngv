# $Id$

import os
import shutil
import logging
from file_based_storage import FileBasedStorage


class LocalStorage(FileBasedStorage):

    def __init__(self, base_path):
        self.base_path = base_path

    def map_key(self, key):
        key = str(key)
        return os.path.join(self.base_path, self._hash_key(key))

    def _check_dir(self, file_name):
        directory = os.path.dirname(file_name)
        if not os.path.isdir(directory):
            logging.debug("Creating directory '%s'" % directory)
            os.makedirs(directory)

    def put(self, key, input_file_name):
	local_file_name = self.map_key(key)
        logging.debug("Copying %s to %s" % (input_file_name, local_file_name))
        self._check_dir(local_file_name)
        shutil.copyfile(input_file_name, local_file_name)

    def get(self, key):
        f = open(self.map_key(key), 'r')
	contents = f.read()
	f.close()
	return contents

    def open(self, key, mode='r'):
        local_file_name = self.map_key(key)
        if mode.startswith('w'):
            self._check_dir(local_file_name)
        return open(local_file_name, mode)

    def exists(self, key):
        return os.path.isfile(self.map_key(key))

    def delete(self, key):
        try:
            os.unlink(self.map_key(key))
        except (IOError, OSError):
            pass

    def get_url(self, key):
        return '/download/%s' % key


