# $Id$

import os
import pwd
import logging
import re
import subprocess
from file_based_storage import FileBasedStorage


class SSHStorage(FileBasedStorage):
    """Remote storage using ssh/scp.

    Ensure that a key is installed on the remote server, as this
    storage handler does not support password-based authentication.
    """

    def __init__(self, addr):
        addr_info = self._parse_remote_addr(addr)
        self.username = addr_info["user"]
        self.host = addr_info["host"]
        self.port = int(addr_info["port"])
        self.path = addr_info["path"]

    def _parse_remote_addr(self, addr):
	current_user = pwd.getpwuid(os.getuid())[0]
        out = {
            "user": current_user,
            "host": "localhost",
            "password": None,
            "port": "22",
            "path": "."
            }
        m = re.search(r'^((\w+)(:([^@]+))?@)?([-a-z0-9.]+)(:[0-9]+)?(:(.*))?$', addr)
        if m:
            out["user"] = m.group(2)
            out["password"] = m.group(4)
            out["host"] = m.group(5)
            out["port"] = m.group(6) or "22"
            out["path"] = m.group(8)
        return out

    def rem_exec(self, cmd):
        logging.info('Executing "%s"' % (' '.join(cmd),))
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        out = p.communicate()[0]
        status = p.wait()
        return out, status

    def map_key_to_path(self, key):
        return '%s/%s' % (self.path, self._hash_key(key))

    def map_key(self, key):
        remote_path = '%s:%s/%s' % (self.host, self.path, 
                                    self._hash_key(key))
        if self.username:
            remote_path = '%s@%s' % (self.username, remote_path)
        return remote_path

    def get_url(self, key, base_url):
        if not base_url[-1] == '/':
            base_url += '/'
        return base_url + self.map_key_to_path(key)

    def put(self, key, input_file_name):
        # Create the dir for the remote file first, then scp it.
        cmd = ['ssh', '-o', 'BatchMode=yes', 
               '-n', '-p', str(self.port), 
               '%s@%s' % (self.username, self.host),
               "mkdir -p '%s'" % os.path.dirname(self.map_key_to_path(key))]
        self.rem_exec(cmd)
        cmd = ['scp', '-q', '-B', '-P', str(self.port),
               input_file_name, 
               self.map_key(key)]
        out, status = self.rem_exec(cmd)
        if status != 0:
            raise Exception("Error in scp, output=%s" % out)

    def get(self, key):
        cmd = ['ssh', '-o', 'BatchMode=yes', 
               '-n', '-p', str(self.port),
               '%s%s%s' % (self.username or '', 
                           self.username and '@', self.host),
               'cat',
               '%s/%s' % (self.path, self.key)]
        out, status = self.rem_exec(cmd)
        return out

    def delete(self, key):
        cmd = ['ssh', '-o', 'BatchMode=yes', 
               '-n', '-p', str(self.port),
               'rm', '-f', '%s/%s' % (self.path, self.key)]
        self.rem_exec(cmd)
