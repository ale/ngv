
import os
import unittest
from ngv.storage.local_storage import LocalStorage


class TestLocalStorage(unittest.TestCase):
    def setUp(self):
        # use a local directory as the root
        test_dir = './test_local_storage-%d' % os.getpid()
        os.mkdir(test_dir)
        self.ls = LocalStorage(test_dir)

        # create a 1Mb test file
        self.test_file = './test_local_storage-file-%d' % os.getpid()
        fd = open(self.test_file, 'w')
        fd.seek(999999)
        fd.write('\n')
        fd.close()

    def tearDown(self):
        os.system("/bin/rm -fr '%s'" % self.ls.base_path)
        os.remove(self.test_file)

    def test_storage_put(self):
        for i in range(10):
            self.ls.put('key_%d' % i, self.test_file)
        for i in range(10):
            self.assert_(self.ls.exists('key_%d' % i))
            self.assertEqual(os.path.getsize(self.ls.map_key('key_%d' % i)),
                             1000000)

    def test_storage_consistency(self):
        test_phrase = 'the quick brown fox'
        fd = self.ls.open('phrase', 'w')
        fd.write(test_phrase)
        fd.close()
        self.assertEqual(test_phrase,
                         self.ls.get('phrase'))

    def test_delete(self):
        self.ls.put('key_del', self.test_file)
        self.assert_(self.ls.exists('key_del'))
        self.ls.delete('key_del')
        self.assert_(not self.ls.exists('key_del'))
        # should not raise an exception
        self.ls.delete('key_del')


if __name__ == '__main__':
    unittest.main()
