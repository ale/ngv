
import os
import unittest
from ngv.storage.local_cache import LocalCache


CACHE_SIZE = '10M'

class TestLocalCache(unittest.TestCase):
    def setUp(self):
        # use a local directory as the root
        test_dir = '/tmp/test_local_cache-%d' % os.getpid()
        os.mkdir(test_dir)
        self.lc = LocalCache(test_dir, CACHE_SIZE)

        # create a 1Mb test file
        self.test_file = './test_local_cache-file-%d' % os.getpid()
        fd = open(self.test_file, 'w')
        fd.seek(999999)
        fd.write('\n')
        fd.close()

    def tearDown(self):
        os.system("/bin/rm -fr '%s'" % self.lc.base_path)
        os.remove(self.test_file)

    def test_cache_add(self):
        for i in range(12):
            self.lc.put('key_%d' % i, self.test_file)
        self.assert_(self.lc.exists('key_11'), 
                     "FIFO does not contain newest element")
        self.assert_(not self.lc.exists('key_0'),
                     "FIFO did not remove oldest element")

    def test_cache_delete(self):
        for i in range(10):
            self.lc.put('key_%d' % i, self.test_file)
        self.assert_(self.lc.exists('key_2'))
        self.lc.delete('key_2')
        self.assert_(not self.lc.exists('key_2'),
                     "cache did not remove the entry")

    def test_cache_open_and_update(self):
        fd = self.lc.open('key_add', 'w')
        fd.write('test phrase')
        fd.close()
        self.lc.update_cache('key_add')
        self.assertEqual('test phrase', self.lc.get('key_add'))
        self.lc.delete('key_add')
        self.assert_(not self.lc.exists('key_add'))


if __name__ == '__main__':
    unittest.main()
