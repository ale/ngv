#!/usr/bin/env python

from setuptools import setup, find_packages


setup(
    name = "ngv",
    version = '0.1',
    description = 'NGV base package',
    long_description = 'General-purpose code for NGV',
    classifiers = filter(None, map(str.strip, """
Intended Audience :: Developers
License :: OSI Approved :: MIT License
Programming Language :: Python
Topic :: Software Development :: Libraries :: Python Modules
""".splitlines())),
    author = "Ale",
    author_email = "ale@incal.net",
    url = "http://ngvision.org",
    license = "GNU GPL",

    packages = find_packages(exclude=['ez_setup']),
    platforms = ['any'],
    install_requires = [ 'sqlalchemy', 'simplejson', 'httplib2' ],
    zip_safe = True,

    entry_points = {
        'console_scripts': [
            'ngv-jobserver = ngv.jobserver.jobserver:entry_point',
            'ngv-uploadserver = ngv.uploadserver.uploadserver:entry_point',
            'ngv-incoming-agent = ngv.workflow.scripts.incoming:entry_point',
            'ngv-processing-agent = ngv.workflow.scripts.processing:entry_point',
            'ngv-add-job = ngv.workflow.scripts.add_job:entry_point',
            ],
        },

)

