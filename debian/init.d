#!/bin/sh
#
# Init script to start/stop NGV daemons.
#
# $Id: $

test -e /etc/default/ngv && . /etc/default/ngv

NGV_OPTIONS="--debug"


iterate_procs() {
  func="$1"
  for agent in ${NGV_AGENTS}
  do
    ${func} ngv-${agent}-agent
  done
  for daemon in ${NGV_DAEMONS}
  do
    ${func} ngv-${daemon}
  done
}

check_proc() {
  proc="$1"
  pid_file=/var/run/${proc}.pid
  if [ -e ${pid_file} ]; then
    return 0
  fi
  return 1
}

start_proc() {
  proc="$1"
  pid_file=/var/run/${proc}.pid
  if check_proc ${proc} ; then
    echo "${proc} already running."
  else
    echo "starting ${proc}..."
    ${proc} --pidfile=${pid_file} ${NGV_OPTIONS}
  fi
}

stop_proc() {
  proc="$1"
  pid_file=/var/run/${proc}.pid
  if check_proc ${proc} ; then
    pid=`cat ${pid_file}`
    echo "stopping ${proc} (${pid})..."
    kill -TERM ${pid}
  else
    echo "${proc} not running."
  fi
}
  


case "$1" in
start)
	iterate_procs start_proc
	;;
stop)
	iterate_procs stop_proc
	;;
restart)
	"$0" stop
	sleep 3
	"$0" start
	;;
*)
	echo "Usage: $0 {start|stop|restart}"
	exit 1
esac

exit 0

