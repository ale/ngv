#!/bin/sh
base_dir=`dirname $0`
if [ -z "$base_dir" ]; then
  base_dir=.
fi
export PYTHONPATH=${base_dir}:${base_dir}/ngv_frontend
echo "PYTHONPATH: '$PYTHONPATH'"
cd ngv_frontend
python setup.py build
python setup.py nosetests
